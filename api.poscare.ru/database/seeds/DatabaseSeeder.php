<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Branch;
use App\Models\City;
use App\Models\Service;
use App\PartnerSupply;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $arr = [
		    'add_service' => 1,
		    'fast_answer' => 1,
		    'new_branch' => 1,
		    'new_image' => 1,
		    'options' => 1,
		    'chosen' => 1,
		    'register' => 1,
		    'service_verified' => 1,
	    ];
	    foreach ( $arr as $key => $value ) {
		    $checkRating = \App\RatingEvent::where( 'event', $key )->first();
		    if ( $checkRating ) {
			    $ratingEvent = $checkRating;
		    } else {
			    $ratingEvent = new \App\RatingEvent();
		    }
		    $ratingEvent->event = $key;
		    $ratingEvent->score = $value;
		    $ratingEvent->save();
	    }
    }
}
