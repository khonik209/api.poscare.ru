<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTenderDelivery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('tenders', function (Blueprint $table) {
		    $table->dropColumn('delivery_id');
		    $table->boolean('delivery')->default(0);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('tenders', function (Blueprint $table) {
		    $table->dropColumn('delivery');
		    $table->boolean('delivery_id')->default(0);

	    });
    }
}
