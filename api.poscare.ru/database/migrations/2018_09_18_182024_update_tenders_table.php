<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenders', function (Blueprint $table) {
            $table->boolean('delivery_id')->default(0);
            $table->boolean('is_sell')->default(0);
        });
        Schema::table('product_tender', function (Blueprint $table) {
            $table->decimal('price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_tender', function (Blueprint $table) {
            $table->dropColumn('price');
        });
        Schema::table('tenders', function (Blueprint $table) {
            $table->dropColumn('delivery_id');
            $table->dropColumn('is_sell');
        });
    }
}
