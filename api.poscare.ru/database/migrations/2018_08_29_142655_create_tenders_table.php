<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamp('terms')->nullable();
            $table->integer('status')->default(0);
            $table->integer('is_accepted')->default(0)->comment('0-new,1-accepted,2-rejected');
            $table->text('description')->nullable();
            $table->boolean('is_private')->default(0);
            $table->integer('branch_id')->unsigned()->nullable()->nullable('winner of tender');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('tender_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tender_id')->unsigned()->nullable();
            $table->foreign('tender_id')->references('id')->on('tenders')->onDelete('cascade');
            $table->integer('branch_id')->unsigned()->nullable();
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->decimal('price')->nullable();
            $table->integer('time_value')->nullable()->comment('integer');
            $table->integer('time_key')->nullable()->comment('hour/day/month/etc');
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tender_responses');
        Schema::dropIfExists('tenders');
    }
}
