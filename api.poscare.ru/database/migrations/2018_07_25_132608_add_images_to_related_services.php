<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagesToRelatedServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('related_services', function (Blueprint $table) {
            $table->string('image')->nullable();
            $table->enum('type', ['product', 'service'])->default('service');
            $table->decimal('average_price')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('related_services', function (Blueprint $table) {
            $table->dropColumn('image');
            $table->dropColumn('type');
        });
    }
}
