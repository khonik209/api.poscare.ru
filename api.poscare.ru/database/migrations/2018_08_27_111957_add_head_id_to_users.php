<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHeadIdToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->nullable();
            $table->foreign('head_id')->references('id')->on('users')->onDelete('set null');
            $table->boolean('head_accepted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_head_id_foreign');
            $table->dropColumn('head_id');
            $table->dropColumn('head_accepted');
        });
    }
}
