<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTenderTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'product_tender', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->integer( 'product_id' )->unsigned()->nullable();
			$table->foreign( 'product_id' )->references( 'id' )->on( 'products' )->onDelete( 'cascade' );
			$table->integer( 'tender_id' )->unsigned()->nullable();
			$table->foreign( 'tender_id' )->references( 'id' )->on( 'tenders' )->onDelete( 'cascade' );
			$table->integer( 'quantity' )->default( 0 );
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'product_tender' );
	}
}
