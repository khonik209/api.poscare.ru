<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVariantsToRelatedServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('related_services', function (Blueprint $table) {
		    $table->text('variants')->nullable();
		    $table->text('notes')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('related_services', function (Blueprint $table) {
		    $table->dropColumn('variants');
		    $table->dropColumn('notes');
	    });
    }
}
