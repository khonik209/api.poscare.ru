<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_user', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('reward_id')->unsigned()->nullable();
	        $table->foreign('reward_id')->references('id')->on('rewards')->onDelete('cascade');
	        $table->integer('user_id')->unsigned()->nullable();
	        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
	        $table->boolean('force')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_user');
    }
}
