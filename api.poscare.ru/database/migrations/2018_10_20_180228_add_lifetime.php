<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLifetime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('bids', function (Blueprint $table) {
		    $table->timestamp('last_refresh')->nullable();
		    $table->integer('refresh_count')->nullable();
	    });
	    Schema::table('orders', function (Blueprint $table) {
		    $table->timestamp('last_refresh')->nullable();
		    $table->integer('refresh_count')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('bids', function (Blueprint $table) {
		    $table->dropColumn('last_refresh');
		    $table->dropColumn('refresh_count');
	    });
	    Schema::table('orders', function (Blueprint $table) {
		    $table->dropColumn('last_refresh');
		    $table->dropColumn('refresh_count');
	    });
    }
}
