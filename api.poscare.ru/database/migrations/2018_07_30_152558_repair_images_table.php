<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RepairImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->integer('is_accepted')->default(2)->change();
        });
        Schema::table('bids', function (Blueprint $table) {
            $table->integer('is_accepted')->default(2)->change();
        });
        Schema::table('reviews', function (Blueprint $table) {
            $table->integer('is_accepted')->default(2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->boolean('is_accepted')->default(0)->change();
        });
        Schema::table('bids', function (Blueprint $table) {
            $table->boolean('is_accepted')->default(0)->change();
        });
        Schema::table('reviews', function (Blueprint $table) {
            $table->boolean('is_accepted')->default(0)->change();
        });
    }
}
