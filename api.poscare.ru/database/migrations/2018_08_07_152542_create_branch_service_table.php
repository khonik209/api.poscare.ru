<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('services', function (Blueprint $table) {
		    $table->integer('quantity')->nullable()->comment('Срок оказание');
		    $table->integer('quantity_type')->nullable()->comment('Единицы срока оказания (часы/дни/месяцы)');
	    });
        Schema::create('branch_service', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('branch_id')->unsigned()->nullable();
	        $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
	        $table->integer('service_id')->unsigned()->nullable();
	        $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
	        $table->decimal('price')->nullable();
	        $table->integer('quantity')->nullable()->comment('Срок оказание');
	        $table->integer('quantity_type')->nullable()->comment('Единицы срока оказания (часы/дни/месяцы)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('services', function (Blueprint $table) {
		    $table->dropColumn('terms');
	    });
        Schema::dropIfExists('branch_service');
    }
}
