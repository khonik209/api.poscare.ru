<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('center_lat')->nullable();
            $table->string('center_lon')->nullable();
            $table->string('time_zone')->nullable();
            $table->string('utc_offset')->nullable();
            $table->timestamps();
        });

        Schema::create('phones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone')->nullable();
            $table->string('salt')->nullable();
            $table->string('auth_token')->nullable();
            $table->string('device_token')->nullable();
            $table->integer('app_type')->default(0);
            $table->boolean('is_accepted')->default(0);
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('phone_id')->unsigned()->nullable();
            $table->foreign('phone_id')->references('id')->on('phones')->onDelete('set null');
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamp('birthday')->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null');
            $table->string('lat')->nullable();
            $table->string('lon')->nullable();
            $table->integer('user_type')->nullable()->default(1);
            $table->integer('orgform')->nullable()->defalt(1);
            $table->boolean('is_service')->default(0);
            $table->boolean('is_shop')->default(0);
            $table->text('want2be')->nullable();
            $table->string('role')->default('user');
            $table->integer('rating')->default(0);
            $table->timestamps();
        });
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
        Schema::create('alarms', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('object', ['bid', 'order', 'news', 'sales'])->nullable();
            $table->integer('object_id')->nullable();
            $table->string('title')->nullable();
            $table->text('text')->nullable();
            $table->timestamps();
        });
        Schema::create('alarm_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alarm_id')->unsigned()->nullable();
            $table->foreign('alarm_id')->references('id')->on('alarms')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->enum('status', ['new', 'is_read', 'deleted'])->default('new');
            $table->timestamps();
        });
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('order')->nullable();
            $table->timestamps();
        });
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
            $table->string('name')->nullable();
            $table->integer('average_price')->nullable();
            $table->text('description')->nullable();
            $table->integer('order')->nullable();
            $table->boolean('is_actual')->default(0);
            $table->timestamps();
        });
        Schema::create('bids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('set null');
            $table->timestamp('terms')->nullable();
            $table->integer('status')->default(0);
            $table->boolean('is_accepted')->default(0);
            $table->text('description')->nullable();
            $table->boolean('is_buyer_complete')->default(0);
            $table->boolean('is_partner_complete')->default(0);
            $table->boolean('is_review')->default(0);
            $table->boolean('is_private')->default(0);
            $table->timestamps();
        });
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->integer('type')->default(0);
            $table->boolean('verification')->default(0);
            $table->integer('order')->nullable();
            $table->timestamps();

        });
        Schema::create('bid_service', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bid_id')->unsigned()->nullable();
            $table->foreign('bid_id')->references('id')->on('bids')->onDelete('cascade');
            $table->integer('service_id')->unsigned()->nullable();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null');
            $table->integer('type')->default(1);
            $table->string('name')->nullable();
            $table->time('open_hours_from')->nullable();
            $table->time('open_hours_to')->nullable();
            $table->string('address')->nullable();
            $table->string('address_lat')->nullable();
            $table->string('address_lon')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
        Schema::create('bid_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bid_id')->unsigned()->nullable();
            $table->foreign('bid_id')->references('id')->on('bids')->onDelete('cascade');
            $table->integer('branch_id')->unsigned()->nullable();
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->integer('price')->nullable();
            $table->integer('quantity')->default(0);
            $table->integer('quantity_type')->default(0);
            $table->text('comment')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('branch_id')->unsigned()->nullable();
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->integer('starts')->default(0);
            $table->text('description')->nullable();
            $table->integer('type')->default(0);
            $table->boolean('is_accepted')->default(0);
            $table->timestamps();
        });
        Schema::create('bid_review', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bid_id')->unsigned()->nullable();
            $table->foreign('bid_id')->references('id')->on('bids')->onDelete('cascade');
            $table->integer('review_id')->unsigned()->nullable();
            $table->foreign('review_id')->references('id')->on('reviews')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('href')->nullable();
            $table->boolean('is_accepted')->default(0);
            $table->timestamps();
        });
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
        Schema::create('image_news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('news_id')->unsigned()->nullable();
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->integer('image_id')->unsigned()->nullable();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('payment_id')->unsigned()->nullable();
            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('set null');
            $table->integer('delivery_id')->unsigned()->nullable();
            $table->foreign('delivery_id')->references('id')->on('deliveries')->onDelete('set null');
            $table->timestamp('terms')->nullable();
            $table->integer('status')->default(0);
            $table->integer('type')->default(0);
            $table->integer('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null');
            $table->boolean('is_accepted')->default(0);
            $table->text('conditions')->nullable();
            $table->boolean('is_buyer_complete')->default(0);
            $table->boolean('is_partner_complete')->default(0);
            $table->boolean('is_review')->default(0);
            $table->boolean('is_private')->default(0);
            $table->timestamps();
        });

        Schema::create('order_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->integer('quantity')->default(0);
            $table->timestamps();
        });

        Schema::create('order_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->integer('branch_id')->unsigned()->nullable();
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->integer('price')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('quantity_type')->default(0);
            $table->text('comment')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
        Schema::create('order_review', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->integer('review_id')->unsigned()->nullable();
            $table->foreign('review_id')->references('id')->on('reviews')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('related_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
        });
        Schema::create('order_related_service', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->integer('related_service_id')->unsigned()->nullable();
            $table->foreign('related_service_id')->references('id')->on('related_services')->onDelete('cascade');
            $table->integer('order_product_id')->unsigned()->nullable();
            $table->foreign('order_product_id')->references('id')->on('order_product')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('service_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('service_id')->unsigned()->nullable();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->timestamps();

        });
        Schema::create('characters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('unit')->nullable();
            $table->text('description')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
        });

        Schema::create('character_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('character_id')->unsigned()->nullable();
            $table->foreign('character_id')->references('id')->on('characters')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('image_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('image_id')->unsigned()->nullable();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('product_related_service', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('related_service_id')->unsigned()->nullable();
            $table->foreign('related_service_id')->references('id')->on('related_services')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('product_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('rewards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price')->default(0);
            $table->text('text')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('branch_image', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branch_id')->unsigned()->nullable();
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->integer('image_id')->unsigned()->nullable();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('user_opts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('short_name')->nullable();
            $table->string('full_name')->nullable();
            $table->string('inn')->nullable();
            $table->text('description')->nullable();
            $table->time('open_hours_from')->nullable();
            $table->time('open_hours_to')->nullable();
            $table->string('address')->nullable();
            $table->string('address_lat')->nullable();
            $table->string('address_lon')->nullable();
            $table->string('site')->nullable();
            $table->integer('image_id')->unsigned()->nullable();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('developer_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_type')->default(1);
            $table->string('value')->nullable();
            $table->timestamps();
        });


        Schema::create('image_sale', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('image_id')->unsigned()->nullable();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            $table->integer('sale_id')->unsigned()->nullable();
            $table->foreign('sale_id')->references('id')->on('sales')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('city_sale', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->integer('sale_id')->unsigned()->nullable();
            $table->foreign('sale_id')->references('id')->on('sales')->onDelete('cascade');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_sale');
        Schema::dropIfExists('image_sale');
        Schema::dropIfExists('developer_tokens');
        Schema::dropIfExists('user_opts');
        Schema::dropIfExists('branch_image');
        Schema::dropIfExists('sales');
        Schema::dropIfExists('rewards');
        Schema::dropIfExists('product_user');
        Schema::dropIfExists('product_related_service');
        Schema::dropIfExists('image_product');
        Schema::dropIfExists('character_product');
        Schema::dropIfExists('characters');
        Schema::dropIfExists('service_user');
        Schema::dropIfExists('order_related_service');
        Schema::dropIfExists('related_services');
        Schema::dropIfExists('order_review');
        Schema::dropIfExists('order_responses');
        Schema::dropIfExists('order_product');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('payments');
        Schema::dropIfExists('deliveries');
        Schema::dropIfExists('image_news');
        Schema::dropIfExists('news');
        Schema::dropIfExists('images');
        Schema::dropIfExists('bid_review');
        Schema::dropIfExists('reviews');
        Schema::dropIfExists('bid_responses');
        Schema::dropIfExists('branches');
        Schema::dropIfExists('bid_service');
        Schema::dropIfExists('services');
        Schema::dropIfExists('bids');
        Schema::dropIfExists('products');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('alarm_user');
        Schema::dropIfExists('alarms');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('users');
        Schema::dropIfExists('phones');
        Schema::dropIfExists('cities');
    }
}
