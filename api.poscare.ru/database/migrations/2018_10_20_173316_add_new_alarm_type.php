<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewAlarmType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    \Illuminate\Support\Facades\DB::statement("ALTER TABLE `alarms` CHANGE `object` `object` ENUM('bid','order','news','sales','tender') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    \Illuminate\Support\Facades\DB::statement("ALTER TABLE `alarms` CHANGE `object` `object` ENUM('bid','order','news','sales') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL");

    }
}
