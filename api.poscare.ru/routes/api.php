<?php

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Input;
use \Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware( [ 'auth.api.dev' ] )->prefix( 'v1' )->group( function () {

	Route::prefix( 'geo' )->group( function () {

		Route::get( 'cities', 'Geo\CitiesController@getCities' );

		Route::get( 'nearestCity', 'Geo\CitiesController@nearestCity' );

		Route::get( 'distance', 'Geo\GeoController@getDistance' );

		Route::get( 'addressCoords', 'Geo\GeoController@getAddressCoords' );
	} );

	Route::prefix( 'user' )->group( function () {

		Route::post( 'sendSmsCode', 'User\SMSController@sendSmsCode' );

		Route::post( 'checkSmsBalance', 'User\SMSController@checkSmsBalance' );

		Route::post( 'authBySms', 'User\SMSController@authBySms' );

		Route::middleware( [ 'auth.api.user' ] )->group( function () {

			Route::post( 'checkAuthToken', 'User\RegistrationController@checkAuthToken' );
			Route::post( 'regUser', 'User\RegistrationController@regUser' );
			Route::post( 'info', 'User\UserDataController@getInfo' );
			Route::post( 'changetype', 'User\UserDataController@changetype' );
			Route::post( 'edit', 'User\UserDataController@editInfo' );

			Route::group( [ 'prefix' => 'head' ], function () {
				Route::get( 'my_users', 'User\HeadController@userList' );
				Route::post( 'new_user', 'User\HeadController@newUser' );
				Route::post( 'delete_user', 'User\HeadController@deleteUser' );
				Route::post( 'accept', 'User\HeadController@acceptHead' );
				Route::post( 'deny', 'User\HeadController@denyHead' );
			} );
			Route::group( [ 'prefix' => 'notifications' ], function () {
				Route::post( 'list', 'AlarmController@getNotificationList' );
				Route::post( 'getOne', 'AlarmController@getNotification' );
				Route::post( 'read', 'AlarmController@readNotification' );
				Route::post( 'delete', 'AlarmController@deleteNotification' );
				Route::post( 'newCount', 'AlarmController@newNotificationCount' );
			} );

		} );


		Route::prefix( 'partner' )->group( function () {


			Route::prefix( 'branches' )->group( function () {
				Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
					Route::post( 'new', 'User\PartnerBranchesController@newBranch' );
					Route::post( 'edit', 'User\PartnerBranchesController@editBranch' );
					Route::post( 'delete', 'User\PartnerBranchesController@deleteBranch' );
					Route::post( 'list', 'User\PartnerBranchesController@getBranchesList' );
					Route::post( 'getOne', 'User\PartnerBranchesController@getBranch' );
				} );

				Route::prefix( 'photos' )->group( function () {

					Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
						Route::post( 'add', 'User\PartnerBranchesController@addBranchPhotos' );
						Route::post( 'delete', 'User\PartnerBranchesController@deleteBranchPhoto' );
					} );

				} );

				Route::get( 'contacts', 'User\PartnerBranchesController@getBranchContacts' );


			} );

			Route::prefix( 'logo' )->group( function () {

				Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
					Route::post( 'add', 'User\UserOptDataController@addUserOptLogo' );
				} );

			} );

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
				Route::post( 'addServices', 'PartnerController@addServices' );


				Route::prefix( 'product' )->group( function () {
					Route::post( 'categories', 'PartnerController@partnerProductCategories' );
					Route::post( 'list', 'PartnerController@partnerProductList' );
					Route::post( 'add', 'PartnerController@addPartnerProduct' );
					Route::post( 'delete', 'PartnerController@deletePartnerProduct' );
				} );
			} );

			Route::prefix( 'services' )->group( function () {

				Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
					Route::post( 'getList', 'User\PartnerBranchesController@getServicesList' );
				} );

			} );
			Route::prefix( 'rewards' )->group( function () {
				Route::post( '/', 'User\UserDataController@getRewards' );
			} );
		} );
	} );

	Route::middleware( [ 'auth.api.user' ] )->prefix( 'images' )->group( function () {

		Route::post( 'upload', 'Image\ImageController@uploadImage' );

		Route::middleware( [ 'auth.api.user.reged' ] )->post( 'delete', 'Image\ImageController@deleteImage' );

	} );


	Route::prefix( 'services' )->group( function () {

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

			Route::post( 'add', 'Service\ServiceController@addService' );
			Route::post( 'delete', 'Service\ServiceController@deleteService' );

		} );


		Route::get( 'list', 'Service\ServiceController@getServicesList' );
		Route::get( 'getOne', 'Service\ServiceController@getService' );

	} );

	Route::prefix( 'products' )->group( function () {

		Route::prefix( 'categories' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

				Route::post( 'add', 'Products\ProductCategoryController@addCategory' );
				Route::post( 'edit', 'Products\ProductCategoryController@editCategory' );
				Route::post( 'delete', 'Products\ProductCategoryController@deleteCategory' );

			} );

			Route::get( 'list', 'Products\ProductCategoryController@getCategoriesList' );
		} );

		Route::prefix( 'characters' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

				Route::post( 'add', 'Products\ProductsCharacterController@addProductsCharacter' );
				Route::post( 'delete', 'Products\ProductsCharacterController@deleteProductsCharacter' );

			} );

			Route::get( 'list', 'Products\ProductsCharacterController@getProductsCharactersList' );

		} );

		Route::prefix( 'images' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
				Route::post( 'add', 'Products\ProductsImageController@addProductPhotos' );
				Route::post( 'delete', 'Products\ProductsImageController@deleteProductPhotos' );
			} );

		} );

		Route::prefix( 'relatedServices' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
				Route::post( 'add', 'Products\ProductsRelatedServicesController@addProductRelatedService' );

				Route::post( 'delete', 'Products\ProductsRelatedServicesController@deleteProductRelatedService' );
			} );

			Route::get( 'list', 'Products\ProductsRelatedServicesController@getProductRelatedServicesList' );

		} );

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

			Route::post( 'add', 'Products\ProductsController@addProduct' );
			Route::post( 'edit', 'Products\ProductsController@editProduct' );
			Route::post( 'delete', 'Products\ProductsController@deleteProduct' );

		} );

		Route::get( 'getOne', 'Products\ProductsController@getProduct' );
		Route::post( 'list', 'Products\ProductsController@getProductsList' );
		Route::post( 'partner/list', 'Products\ProductsController@getProductsToPartner' );

	} );

	Route::prefix( 'characters' )->group( function () {

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

			Route::post( 'add', 'Characters\CharactersController@addCharacter' );
			Route::post( 'edit', 'Characters\CharactersController@editCharacter' );
			Route::post( 'delete', 'Characters\CharactersController@deleteCharacter' );

		} );

		Route::get( 'list', 'Characters\CharactersController@getCharactersList' );
		Route::get( 'getOne', 'Characters\CharactersController@getCharacter' );

	} );

	Route::prefix( 'bids' )->group( function () {

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
			Route::post( 'add', 'Bids\BidsController@addBid' );
			Route::post( 'repeat', 'Bids\BidsController@repeatBid' );
			Route::post( 'refresh', 'Bids\BidsController@refreshBid' );
			Route::post( 'cancel', 'Bids\BidsController@cancelBid' );
			Route::post( 'complete', 'Bids\BidsController@bidComplete' );
		} );

		Route::prefix( 'partner' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
				Route::post( 'list', 'Bids\BidsController@getPartnerBidsList' );
				Route::post( 'getOne', 'Bids\BidsController@getPartnerBid' );
				Route::post( 'complete', 'Bids\BidsController@bidPartnerComplete' );
			} );

		} );

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->prefix( 'user' )->group( function () {

			Route::post( 'list', 'Bids\UserBidsController@getUserBidsList' );
			Route::post( 'getOne', 'Bids\UserBidsController@getBid' );

		} );

		Route::prefix( 'services' )->group( function () {

			Route::get( 'list', 'Bids\BidsController@getBidsServices' );

		} );

		Route::prefix( 'response' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

				Route::post( 'add', 'Bids\BidsResponseController@addResponse' );
				Route::post( 'edit', 'Bids\BidsResponseController@editResponse' );
				Route::post( 'list', 'Bids\BidsResponseController@getBidResponsesList' );
				Route::post( 'select', 'Bids\BidsResponseController@selectBidResponse' );
				Route::post( 'getOne', 'Bids\BidsResponseController@getBidResponse' );
				Route::post( 'refuse', 'Bids\BidsResponseController@refuseBidResponse' );
				Route::post( 'delete', 'Bids\BidsResponseController@deleteBidResponse' );
				Route::post( 'fix', 'Bids\BidsResponseController@fixBidResponse' );
				Route::post( 'unfix', 'Bids\BidsResponseController@unfixBidResponse' );

			} );

			Route::get( 'contacts', 'Bids\BidsResponseController@getBidResponseContacts' );

		} );

	} );

	Route::prefix( 'reviews' )->group( function () {

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

			Route::post( 'add', 'Reviews\ReviewsController@addReview' );
			Route::post( 'list', 'Reviews\ReviewsController@getReviewsList' );
			Route::post( 'getOne', 'Reviews\ReviewsController@getReview' );

		} );

	} );


	Route::prefix( 'relatedServices' )->group( function () {

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

			Route::post( 'add', 'RelatedServices\RelatedServicesController@addRelatedService' );
			Route::post( 'delete', 'RelatedServices\RelatedServicesController@deleteRelatedService' );

		} );

		Route::get( 'getOne', 'RelatedServices\RelatedServicesController@getRelatedService' );

	} );

	Route::get( 'branches/contacts', 'User\PartnerBranchesController@getBranchContacts' );

	Route::prefix( 'sales' )->group( function () {

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

			Route::post( 'add', 'Sales\SalesController@addSale' );
			Route::post( 'edit', 'Sales\SalesController@editSale' );
			Route::post( 'delete', 'Sales\SalesController@deleteSale' );

		} );

		Route::get( 'getOne', 'Sales\SalesController@getSale' );
		Route::get( 'list', 'Sales\SalesController@getSalesList' );

		Route::prefix( 'photos' )->group( function () {
			Route::post( 'add', 'Sales\PhotosController@addSalesPhotos' );
			Route::post( 'delete', 'Sales\PhotosController@deleteSalesPhotos' );
		} );

		Route::prefix( 'cities' )->group( function () {
			Route::post( 'add', 'Sales\CitiesController@addSalesCities' );
			Route::post( 'delete', 'Sales\CitiesController@deleteSalesCities' );
		} );


	} );

	Route::prefix( 'news' )->group( function () {

		Route::prefix( 'photos' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

				Route::post( 'add', 'News\NewsController@addNewsPhotos' );
				Route::post( 'delete', 'News\NewsController@deleteNewsPhotos' );

			} );

		} );

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

			Route::post( 'add', 'News\NewsController@addNews' );
			Route::post( 'edit', 'News\NewsController@editNews' );
			Route::post( 'delete', 'News\NewsController@deleteNews' );

		} );

		Route::get( 'getOne', 'News\NewsController@getOneNews' );
		Route::get( 'list', 'News\NewsController@getNewsList' );

	} );

	Route::prefix( 'businesses' )->group( function () {

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
			Route::post( 'add', 'Businesses\BusinessesController@addBusiness' );
		} );

		Route::get( 'list', 'Businesses\BusinessesController@getBusinessesList' );
		Route::get( 'getOne', 'Businesses\BusinessesController@getBusiness' );

	} );

	Route::prefix( 'taxes' )->group( function () {

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
			Route::post( 'add', 'Taxes\TaxesController@addTax' );
		} );

		Route::get( 'list', 'Taxes\TaxesController@getTaxesList' );
		Route::get( 'getOne', 'Taxes\TaxesController@getTax' );

	} );

	Route::prefix( 'delivery' )->group( function () {
		Route::prefix( 'method' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
				Route::post( 'add', 'Delivery\DeliveryController@addMethod' );
			} );

			Route::get( 'list', 'Delivery\DeliveryController@getMethodsList' );
			Route::get( 'getOne', 'Delivery\DeliveryController@getMethod' );

		} );

	} );

	Route::prefix( 'payments' )->group( function () {
		Route::prefix( 'method' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
				Route::post( 'add', 'Payments\PaymentsController@addMethod' );
			} );

			Route::get( 'list', 'Payments\PaymentsController@getMethodsList' );
			Route::get( 'getOne', 'Payments\PaymentsController@getMethod' );

		} );

	} );

	Route::prefix( 'orders' )->group( function () {
		Route::prefix( 'products' )->group( function () {
			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
				Route::post( 'add', 'Orders\OrdersController@addOrderProduct' );
				Route::post( 'delete', 'Orders\OrdersController@deleteOrderProduct' );
			} );

			Route::get( 'list', 'Orders\OrdersController@getOrderProductsList' );


			Route::prefix( 'relatedServices' )->group( function () {
				Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
					Route::post( 'sync', 'Orders\OrdersRelatedServicesController@syncRelatedServices' );
				} );

				Route::get( 'list', 'Orders\OrdersRelatedServicesController@getProductsRelatedServices' );

			} );
		} );

		Route::prefix( 'user' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

				Route::post( 'list', 'Orders\OrdersUserController@getUserOrdersList' );

			} );

		} );

		Route::prefix( 'partner' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

				Route::post( 'list', 'Orders\OrdersController@getOrdersList' );
				Route::post( 'getOne', 'Orders\OrdersController@getPartnerOrder' );
				Route::post( 'payed', 'Orders\OrdersController@setOrderPayed' );
				Route::post( 'delivered', 'Orders\OrdersController@setOrderDelivered' );
				Route::post( 'complete', 'Orders\OrdersController@orderPartnerComplete' );
			} );

		} );

		Route::post( 'switch_status', 'Orders\OrdersController@switchStatus' );

		Route::prefix( 'responses' )->group( function () {

			Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {

				Route::post( 'add', 'Orders\OrdersResponsesController@addOrderResponse' );
				Route::post( 'getList', 'Orders\OrdersResponsesController@getListOrderResponse' );
				Route::post( 'getOne', 'Orders\OrdersResponsesController@getOrderResponse' );
				Route::post( 'choose', 'Orders\OrdersResponsesController@chooseOrderResponse' );
				Route::post( 'refuse', 'Orders\OrdersResponsesController@refuseOrderResponse' );
				Route::post( 'delete', 'Orders\OrdersResponsesController@deleteOrderResponse' );
			} );

			Route::get( 'contacts', 'Orders\OrdersResponsesController@orderResponseContacts' );

		} );

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
			Route::post( 'cancel', 'Orders\OrdersController@cancelOrder' );
			Route::post( 'form', 'Orders\OrdersController@makeOrderFormed' );
			Route::post( 'complete', 'Orders\OrdersController@orderComplete' );
			Route::get( 'getOne', 'Orders\OrdersController@getOrder' );
			Route::get( 'getOne/{count}', 'Orders\OrdersController@getOrder' )->where( 'count', 'count' );
			Route::post( 'getOne/conditions', 'Orders\OrdersController@getOrderConditions' );
			Route::post( 'repeat', 'Orders\OrdersController@repeatOrder' );
			Route::post( 'refresh', 'Orders\OrdersController@refreshOrder' );
		} );

	} );

	Route::prefix( 'tenders' )->group( function () {

		Route::middleware( [ 'auth.api.user', 'auth.api.user.reged' ] )->group( function () {
			Route::get( 'list', 'Tenders\TenderController@list' );
			Route::get( 'show', 'Tenders\TenderController@show' );
			Route::get( 'show/products/list', 'Tenders\TenderController@tenderProducts' );
			Route::post( 'create', 'Tenders\TenderController@create' );
			Route::post( 'edit', 'Tenders\TenderController@edit' );
			Route::post( 'delete', 'Tenders\TenderController@delete' );
			Route::post( 'complete', 'Tenders\TenderController@complete' );
			Route::post( 'cancel', 'Tenders\TenderController@cancel' );

			Route::prefix( 'response' )->group( function () {
				Route::get( 'list', 'Tenders\TenderResponseController@list' );
				Route::get( 'show', 'Tenders\TenderResponseController@show' );
				Route::post( 'create', 'Tenders\TenderResponseController@create' );
				Route::post( 'edit', 'Tenders\TenderResponseController@edit' );
				Route::post( 'delete', 'Tenders\TenderResponseController@delete' );
			} );
		} );
		Route::get( 'products/list', 'Tenders\TenderController@productList' );
	} );

	Route::get( 'activities', 'Controller@activities' );
} );

Route::prefix( 'yandex' )->group( function () {
	Route::post( 'create', 'YandexController@create' );
	Route::post( 'check', 'YandexController@check' );
	Route::post( 'aviso', 'YandexController@aviso' );
	Route::post( 'info', 'YandexController@info' );
} );

Route::get( 'give_me_routes', function () {
	$routeCollection = \Illuminate\Support\Facades\Route::getRoutes();
	echo "<table style='width:100%'>";
	echo "<tr>";
	echo "<td width='10%'><h4>HTTP Method</h4></td>";
	echo "<td width='10%'><h4>Route</h4></td>";
	echo "</tr>";
	foreach ( $routeCollection as $value ) {
		$url = $value->uri();
		$exp = explode( '/', $url );

		unset( $exp[0] );
		unset( $exp[1] );

		echo "<tr>";
		echo "<td>" . $value->methods()[0] . "</td>";
		echo "<td>/" . implode( '/', $exp ) . "</td>";
		echo "</tr>";
	}
	echo "</table>";
} );

Route::get( 'admin/order/accept', 'Admin\AdminController@acceptOrder' );
Route::get( 'admin/bid/accept', 'Admin\AdminController@acceptBid' );

//Метод для проверки значений

Route::post( 'v1/dev/query/check', function ( Request $request ) {

	$all_params = Input::all() ?? "Нет параметров";

	return response()->json( [

		'response' => [
			'ALL params' => $all_params,
			'headers' => $request->header(),
			'POST params' => $_POST,
			'GET PARAMS' => $_GET,
			'FILES' => $_FILES,
		]

	], 200, [], JSON_UNESCAPED_UNICODE );

} );

Route::post( 'v1/dev/user/delete', function ( Request $request ) {

	$auth_token = $request->input( 'auth_token' );

	$ph_slt = \App\Models\PhoneSalt::where( 'auth_token', $auth_token )->where( 'is_accepted', 1 )->first();

	$user = $ph_slt->user()->first();

	DB::transaction( function () use ( $user, $ph_slt ) {

		$user->delete();
		$ph_slt->delete();

	} );


	return response()->json( [

		'response' => 1

	], 200, [], JSON_UNESCAPED_UNICODE );

} );
