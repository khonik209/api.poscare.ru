<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'Admin\HomeController@index')->name('home');

    Route::prefix('user')->group(function () {
        /* HomeController. returns views */
        Route::get('/', 'Admin\HomeController@getUsers');
        Route::get('/get/{id}', 'Admin\HomeController@getUser');
        Route::get('/get/{user_id}/branch/new', 'Admin\HomeController@userBranchCreate');
        Route::get('/get/branch/{branch_id}/edit', 'Admin\HomeController@userBranchEdit');
        Route::get('/new', 'Admin\HomeController@newUser');
        Route::post('/new', 'Admin\HomeController@saveNewUser');
        /* AdminController. returns json */
        Route::post('/list', 'Admin\AdminController@userList');
        Route::post('/save', 'Admin\AdminController@saveUser');
        Route::post('/delete', 'Admin\AdminController@deleteUser');
        Route::post('/opt/create', 'Admin\AdminController@userOptCreate');
        Route::post('/opt/edit', 'Admin\AdminController@userOptEdit');
        Route::post('/branch/edit', 'Admin\AdminController@branchEdit');
        Route::post('/branch/create', 'Admin\AdminController@branchCreate');
        Route::post('/branch/image/add', 'Admin\AdminController@branchImageAdd');
        Route::post('/branch/image/delete', 'Admin\AdminController@branchImageDelete');
        Route::post('/branch/{branch_id}/delete', 'Admin\AdminController@userBranchDelete');
        Route::post('/service/verify', 'Admin\AdminController@userServiceVerify');


        Route::post('/rewards/force', 'Admin\RewardController@force');

    });
    Route::prefix('lists')->group(function () {
        // Route::get('/', 'Admin\AdminController@getLists');
        Route::get('/{vue_capture?}', function () {
            return view('lists');
        })->where('vue-capture', '[\/w\.-]*');
        Route::post('/categories', 'Admin\AdminController@getCategories');
        Route::post('/categories/order', 'Admin\AdminController@orderCategory');
        Route::post('/category/delete', 'Admin\AdminController@deleteCategory');
        Route::post('/category/create', 'Admin\AdminController@createCategory');
        Route::post('/category/edit', 'Admin\AdminController@editCategory');

        Route::post('/products', 'Admin\AdminController@getProducts');
        Route::post('/products/import', 'Admin\AdminController@importProducts');
        Route::get('/products/export', 'Admin\AdminController@exportProducts');
        Route::post('/products/order', 'Admin\AdminController@orderProducts');
        Route::post('/product/delete', 'Admin\AdminController@deleteProduct');
        Route::get('/product/create', 'Admin\AdminController@newProductPage');
        Route::post('/product/create', 'Admin\AdminController@newProduct');
        Route::get('/product/edit/{id}', 'Admin\AdminController@editProductPage');
        Route::post('/product/edit', 'Admin\AdminController@editProduct');

        Route::post('/services', 'Admin\AdminController@getServices');
        Route::post('/services/order', 'Admin\AdminController@orderServices');
        Route::get('/service/create', 'Admin\AdminController@createServicePage');
        Route::post('/service/create', 'Admin\AdminController@createService');
        Route::get('/service/edit/{id}', 'Admin\AdminController@editServicePage');
        Route::post('/service/edit', 'Admin\AdminController@editService');
        Route::post('/service/delete', 'Admin\AdminController@deleteService');

        Route::post('/characters', 'Admin\AdminController@getCharacters');
        Route::post('/characters/order', 'Admin\AdminController@orderCharacters');
        Route::post('/character/delete', 'Admin\AdminController@deleteCharacter');
        Route::post('/character/edit', 'Admin\AdminController@editCharacter');
        Route::post('/character/create', 'Admin\AdminController@createCharacter');

        Route::post('/related_services', 'Admin\AdminController@getRelatedServices');
        Route::post('/related_services/order', 'Admin\AdminController@orderRelatedServices');
        Route::post('/related_service/create', 'Admin\AdminController@createRelatedService');
        Route::post('/related_service/delete', 'Admin\AdminController@deleteRelatedService');
        Route::post('/related_service/edit', 'Admin\AdminController@editRelatedService');

        Route::post('/deliveries', 'Admin\AdminController@getDeliveries');
        Route::post('/deliveries/order', 'Admin\AdminController@orderDeliveries');
        Route::post('/delivery/delete', 'Admin\AdminController@deleteDelivery');
        Route::post('/delivery/edit', 'Admin\AdminController@editDelivery');
        Route::post('/delivery/create', 'Admin\AdminController@createDelivery');

        Route::post('/payments', 'Admin\AdminController@getPayments');
        Route::post('/payments/order', 'Admin\AdminController@orderPayments');
        Route::post('/payment/delete', 'Admin\AdminController@deletePayment');
        Route::post('/payment/edit', 'Admin\AdminController@editPayment');
        Route::post('/payment/create', 'Admin\AdminController@createPayment');

        Route::post('/rewards', 'Admin\RewardController@getRewards');
        Route::post('/reward/delete', 'Admin\RewardController@deleteReward');
        Route::get('/reward/edit/{id}', 'Admin\RewardController@editRewardPage');
        Route::post('/reward/edit', 'Admin\RewardController@editReward');
        Route::get('/reward/create', 'Admin\RewardController@createRewardPage');
        Route::post('/reward/create', 'Admin\RewardController@createReward');
    });
    Route::prefix('moderation')->group(function () {
        Route::get('/', 'Admin\AdminController@getModeration');

        Route::post('/bids', 'Admin\AdminController@bidsToModerate');
        Route::post('/bid/accept', 'Admin\AdminController@acceptBid');
        Route::post('/bid/reject', 'Admin\AdminController@rejectBid');
        Route::get('/bid/offer/{id}', 'Admin\AdminController@bidOfferPage');
        Route::post('/bid/offer', 'Admin\AdminController@bidOffer');

        Route::post('/orders', 'Admin\AdminController@ordersToModerate');
        Route::post('/order/accept', 'Admin\AdminController@acceptOrder');
        Route::post('/order/reject', 'Admin\AdminController@rejectOrder');
        Route::get('/order/offer/{id}', 'Admin\AdminController@orderOfferPage');
        Route::post('/order/offer', 'Admin\AdminController@orderOffer');

        Route::post('/reviews', 'Admin\AdminController@reviewsToModerate');
        Route::post('/review/accept', 'Admin\AdminController@acceptReview');
        Route::post('/review/reject', 'Admin\AdminController@rejectReview');

        Route::post('/images', 'Admin\AdminController@imagesToModerate');
        Route::post('/image/accept', 'Admin\AdminController@acceptImage');
        Route::post('/image/reject', 'Admin\AdminController@rejectImage');

	    Route::post('/tenders', 'Admin\TenderController@tendersToModerate');
	    Route::post('/tender/accept', 'Admin\TenderController@acceptTender');
	    Route::post('/tender/reject', 'Admin\TenderController@rejectTender');
    });
    Route::prefix('bids')->group(function () {
        Route::get('/', 'Admin\AdminController@getBidsPage');
        Route::post('/list', 'Admin\AdminController@getBids');
        Route::get('/new', 'Admin\AdminController@newBid');

        Route::get('/getOne/{id}', 'Admin\AdminController@getBid');
        Route::post('/getOne/edit', 'Admin\AdminController@editBid');
    });
    Route::prefix('orders')->group(function () {
        Route::get('/', 'Admin\AdminController@getOrdersPage');
        Route::post('/list', 'Admin\AdminController@getOrders');
        Route::get('/new', 'Admin\AdminController@newOrder');

        Route::get('/getOne/{id}', 'Admin\AdminController@getOrder');
        Route::post('/getOne/edit', 'Admin\AdminController@editOrder');
    });

    Route::prefix('tenders')->group(function () {
        Route::get('/', 'Admin\TenderController@list');
        Route::get('create', 'Admin\TenderController@create');
        Route::post('store', 'Admin\TenderController@store');
        Route::get('/{id}', 'Admin\TenderController@show');
        Route::get('{id}/edit', 'Admin\TenderController@edit');
        Route::post('update', 'Admin\TenderController@update');
        Route::post('delete', 'Admin\TenderController@delete');

        Route::prefix('response')->group(function () {
            //Route::get('list', 'Admin\TenderResponseController@list');
            //Route::get('show', 'Admin\TenderResponseController@show');
            //Route::post('create', 'Admin\TenderResponseController@create');
            //Route::post('edit', 'Admin\TenderResponseController@edit');
            Route::post('delete', 'Admin\TenderController@deleteResponse');
        });
    });

    Route::prefix('news')->group(function () {
        Route::get('/', 'Admin\AdminController@getNewsPage');
        Route::get('/get/{id}', 'Admin\AdminController@getNews');
        Route::get('/create', 'Admin\AdminController@createNewsPage');
        Route::post('/create', 'Admin\AdminController@createNews');
        Route::post('/edit', 'Admin\AdminController@editNews');
        Route::post('/delete', 'Admin\AdminController@deleteNews');
    });
    Route::prefix('sales')->group(function () {
        Route::get('/', 'Admin\AdminController@getSalesPage');
        Route::get('/get/{id}', 'Admin\AdminController@getSale');
        Route::get('/create', 'Admin\AdminController@createSalePage');
        Route::post('/create', 'Admin\AdminController@createSale');
        Route::post('/edit', 'Admin\AdminController@editSale');
        Route::post('/delete', 'Admin\AdminController@deleteSale');
    });
    Route::resource('settings', 'Admin\SettingsController');
});

Route::any('/getcities', 'Admin\AdminController@getCities');
Route::any('/getusers', 'Admin\AdminController@getUsers');