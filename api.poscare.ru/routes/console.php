<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command( 'inspire', function () {
	$this->comment( Inspiring::quote() );
} )->describe( 'Display an inspiring quote' );


Artisan::command( 'seed_notifications', function () {

	$users = \App\Models\User::all();
	$news  = \App\Models\News::first();
	$order = \App\Models\Order::first();
	$bid   = \App\Models\Bid::first();
	$sale  = \App\Models\Sale::first();

	$alarm            = new \App\Models\Alarm;
	$alarm->title     = "Новый заказ";
	$alarm->text      = "Поступил новый заказ " . \Carbon\Carbon::now();
	$alarm->object    = "bid";
	$alarm->object_id = $bid->id;
	$alarm->save();

	$users = \App\Models\User::get( [ 'id' ] );

	$alarm->users()->attach( $users );

} )->describe( 'seed db for test notifications' );

Artisan::command( 'test_alarm', function () {
	$alarm = \App\Models\Alarm::first();
	print $alarm->id;
} );

Artisan::command( 'test_push', function () {
	$alarm     = \App\Models\Alarm::find( 24 );
	$receivers = [ 1, 2 ];
	dispatch( new \App\Jobs\SendPushNotification( $alarm, $receivers ) );
} );

Artisan::command( 'sync_cities', function () {
	$old = DB::connection( 'old' )->table( 'cities' )->get( [ 'name', 'center_lat', 'center_lon', 'time_zone', 'utc_offset' ] )->toJson();
	DB::table( 'cities' )->insert( json_decode( $old, true ) );
} );
Artisan::command( 'sync_developer_tokens', function () {
	$old = DB::connection( 'old' )->table( 'developer_tokens' )->get( [ 'app_type', 'value' ] )->toJson();
	DB::table( 'developer_tokens' )->insert( json_decode( $old, true ) );
} );

Artisan::command( 'sync_products', function () {
	$old = DB::connection( 'old' )->table( 'products' )->get( [ 'name', 'average_price', 'description' ] )->toJson();
	DB::table( 'products' )->insert( json_decode( $old, true ) );
} );
/*Artisan::command('sync_products', function () {
    $old = DB::connection('old')->table('products')->get(['name', 'average_price', 'description'])->toJson();
    DB::table('products')->insert(json_decode($old, true));
});*/
Artisan::command( 'sync_related_services', function () {
	$old = DB::connection( 'old' )->table( 'related_services' )->get( [ 'name', 'description' ] )->toJson();
	DB::table( 'related_services' )->insert( json_decode( $old, true ) );
} );
Artisan::command( 'sync_services', function () {
	$old = DB::connection( 'old' )->table( 'services' )->get( [ 'name', 'description', 'type' ] )->toJson();
	DB::table( 'services' )->insert( json_decode( $old, true ) );
} );
Artisan::command( 'sync_users', function () {
	$old = DB::connection( 'old' )->table( 'users' )->get( [ 'email', 'password', 'name', 'surname', 'lat', 'lon', 'user_type', 'orgform', 'is_service', 'is_shop', 'role', 'city_id', 'phone_id' ] );
	$old->map( function ( $v, $k ) {
		$phone = collect( DB::connection( 'old' )->table( 'phone_salt' )->where( 'id', $v->phone_id )->first() )->only( [ 'phone', 'salt', 'is_accepted', 'auth_token', 'device_token', 'app_type' ] );

		DB::table( 'phones' )->insert( json_decode( $phone, true ) );
		$findPhone = collect( DB::connection( 'old' )->table( 'phone_salt' )->where( 'id', $v->phone_id )->first() )->only( [ 'phone' ] );
		if ( count( $findPhone ) > 0 ) {
			$checkPhone = DB::table( 'phones' )->where( 'phone', $findPhone )->first() ? DB::table( 'phones' )->where( 'phone', $findPhone )->first()->id : null;
		} else {
			$checkPhone = null;
		}
		$v->phone_id = $checkPhone;
		$findMosoow  = DB::table( 'cities' )->where( 'name', 'Москва' )->first()->id;
		$v->city_id  = $findMosoow;
		return $v;
	} );
	DB::table( 'users' )->insert( json_decode( $old, true ) );
} );
Artisan::command( 'sync_branches', function () {
	$old = DB::connection( 'old' )->table( 'users_branches' )->get( [
		'user_id',
		'city_id',
		'type',
		'name',
		'open_hours_from',
		'open_hours_to',
		'address',
		'address_lat',
		'address_lon',
		'phone',
		'email',
		'description'
	] );
	$old->map( function ( $v, $k ) {
		$findUser = collect( DB::connection( 'old' )->table( 'users' )->where( 'id', $v->user_id )->first() )->only( [ 'email' ] );
		$findCity = collect( DB::connection( 'old' )->table( 'cities' )->where( 'id', $v->city_id )->first() )->only( [ 'name' ] );
		if ( count( $findUser ) > 0 ) {
			$checkUser = DB::table( 'users' )->where( 'email', $findUser )->first() ? DB::table( 'users' )->where( 'email', $findUser )->first()->id : null;
		} else {
			$checkUser = null;
		}
		$checkCity = DB::table( 'cities' )->where( 'name', $findCity )->first()->id;

		$v->user_id = $checkUser;
		$v->city_id = $checkCity;

		return $v;
	} );
	DB::table( 'branches' )->insert( json_decode( $old, true ) );
} );

Artisan::command( 'sync_product_image', function () {
	$old = DB::connection( 'old' )->table( 'products_img' )->get( [ 'product_id', 'image_id' ] );
	$old->map( function ( $v, $k ) {
		$old_product_id = $v->product_id;
		print 'old product id: ' . $old_product_id . "\n";
		$old_image_id = $v->image_id;
		print 'old image id: ' . $old_image_id . "\n";
		$old_product_name = DB::connection( 'old' )->table( 'products' )->where( 'id', $old_product_id )->first()->name;
		print 'old product name: ' . $old_product_name . "\n";
		$new_product_id = DB::table( 'products' )->where( 'name', $old_product_name )->first()->id;
		print 'new product id: ' . $new_product_id . "\n";
		$old_image = collect( DB::connection( 'old' )->table( 'images' )->where( 'id', $old_image_id )->first() )->only( [ 'href', 'is_accepted' ] );

		DB::table( 'images' )->insert( json_decode( $old_image, true ) );
		print 'old image: ' . ( $old_image->first() ) . "\n";
		$new_image_id = DB::table( 'images' )->where( 'href', $old_image->first() )->first()->id;
		print 'new image id: ' . $new_image_id . "\n";
		$v->product_id = $new_product_id;
		$v->image_id   = $new_image_id;
		return $v;
	} );
	//var_dump($old);
	DB::table( 'image_product' )->insert( json_decode( $old, true ) );
} );
Artisan::command( 'branch_service_sync', function () {
	$branches = \App\Models\Branch::where( 'user_id', '=', 15 )->get();
	$services = \App\Models\Service::get( [ 'id', 'average_price' ] );
	foreach ( $branches as $branch ) {
		foreach ( $services as $service ) {
			$branch->services()->attach( $service->id, [ 'price' => $service->average_price, 'quantity' => 3, 'quantity_type' => 2 ] );
			print 'id: ' . $service->id . "\n";
			print 'price: ' . $service->average_price . "\n";
		}
	}
	//var_dump( $services );
} );
Artisan::command( 'email_send', function () {
	$user = \App\Models\User::where( 'email', 'noname-pw@mail.ru' )->first();
	\Illuminate\Support\Facades\Mail::to( $user )->send( new \App\Mail\EmailNotification( 'MY TITLE', 'MY TEXT' ) );
} );

Artisan::command( 'update_last_refresh', function () {
	$bids = \App\Models\Bid::whereNull( 'refresh_count' )->orWhereNull( 'last_refresh' )->get();
	foreach ($bids as $bid)
	{
		$bid->refresh_count = 1;
		$bid->last_refresh = \Carbon\Carbon::createFromTimestamp($bid->updated_at);
		$bid->save();
	}
	$orders = \App\Models\Order::whereNull( 'refresh_count' )->orWhereNull( 'last_refresh' )->get();
	foreach ($orders as $order)
	{
		$order->refresh_count = 1;
		$order->last_refresh = \Carbon\Carbon::createFromTimestamp($order->updated_at);
		$order->save();
	}
} );

Artisan::command( 'remove_last_statuses', function () {
    $bids = \App\Models\Bid::where('status',6)->get();
    foreach ($bids as $bid)
    {
        $bid->status = 5;
        $bid->save();
    }

    $orders = \App\Models\Order::where('status',7)->get();
    foreach ($orders as $order)
    {
        $order->status = 6;
        $order->save();
    }
} )->describe( '' );