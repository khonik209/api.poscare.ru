<?php

return [
    'apn' => [
        'certificate' => __DIR__ . '/iosCertificates/ck.pem',
        'passPhrase' => 'Ugu4943439Ziskp', //Optional
         'passFile' => __DIR__ . '/iosCertificates/entrust_root_certification_authority.pem', //Optional
        'dry_run' => false
    ],
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => false,
      'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'My_ApiKey',
  ],

];