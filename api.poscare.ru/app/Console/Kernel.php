<?php

namespace App\Console;

use App\Models\Bid;
use App\Models\Order;
use App\Traits\AlarmTrait;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel {
	use AlarmTrait;
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [//
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	protected function schedule( Schedule $schedule ) {
		//$schedule->command('inspire')->hourly();

		/**
		 * Уведомление о скором закрытии заявки или заказа (за час)
		 */

		$schedule->call( function () {

			/*
			 *  create = 1
			 *  no no = 2
			 *	alert = 3
			 *	close = 4
			 *
			 *
			 */

			$orders = Order::where( 'refresh_count', '>', 0 )->where( 'last_refresh', ">", Carbon::now()->subHours( 3 ) )->where( 'last_refresh', "<", Carbon::now()->subHours( 2 ) )->get();
			foreach ( $orders as $order ) {
				$receivers = [ $order->user_id ];
				$this->createAlarm( 'order', $order, 'Ваш заказ скоро будет закрыт', 'Ваш заказ скоро будет закрыт', $receivers );
			}

			$bids = Bid::where( 'refresh_count', '>', 0 )->where( 'last_refresh', ">", Carbon::now()->subHours( 3 ) )->where( 'last_refresh', "<", Carbon::now()->subHours( 2 ) )->get();
			foreach ( $bids as $bid ) {
				$receivers = [ $bid->user_id ];
				$this->createAlarm( 'bid', $bid, 'Ваша заявка скоро будет закрыта', 'Ваша заявка скоро будет закрыта', $receivers );
			}

		} )->everyTenMinutes();

		/**
		 * Аннулирование заказа/заявки
		 */

		$schedule->call( function () {
			$orders    = Order::where( 'status', 2 )->where( 'refresh_count', '>', 0 )->where( 'last_refresh', ">", Carbon::now()->subHours( 4 ) )->where( 'last_refresh', "<", Carbon::now()->subHours( 3 ) )->get();
			$order_ids = $orders->pluck( 'id' );
			DB::table( 'orders' )->whereIn( 'id', $order_ids )->update( [ 'status' => 6 ] );
			foreach ( $orders as $order ) {
				$receivers = [ $order->user_id ];
				$this->createAlarm( 'order', $order, 'Ваш заказ был аннулирован', 'Ваш заказ был аннулирован', $receivers );
			}

			$bids = Bid::where( 'status', 1 )->where( 'refresh_count', '>', 0 )->where( 'last_refresh', ">", Carbon::now()->subHours( 4 ) )->where( 'last_refresh', "<", Carbon::now()->subHours( 3 ) )->get();
			$bid_ids = $bids->pluck( 'id' );
			DB::table( 'bids' )->whereIn( 'id', $bid_ids )->update( [ 'status' => 5 ] );
			foreach ( $bids as $bid ) {
				$receivers = [ $bid->user_id ];
				$this->createAlarm( 'bid', $bid, 'Ваша заявка была аннулирована', 'Ваша заявка была аннулирована', $receivers );
			}

		} )->everyTenMinutes();
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands() {
		$this->load( __DIR__ . '/Commands' );

		require base_path( 'routes/console.php' );
	}
}
