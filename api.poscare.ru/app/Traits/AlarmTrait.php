<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 24.04.2018
 * Time: 11:26
 */

namespace App\Traits;


use App\Jobs\PushNotification;
use App\Jobs\SMSNotification;
use App\Mail\EmailNotification;
use App\Models\Alarm;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

trait AlarmTrait
{
    /**
     * @param string $type - Тип уведомления (news|sale|order|bid)
     * @param object $object - Связанная модель
     * @param string $title - Заголовок
     * @param string $text - Текст сообщения
     * @param array $receivers - Массив ID получателей уведомления
     * @return object
     */
    protected function createAlarm($type, $object, $title, $text, $receivers)
    {
        $alarm = new Alarm;
        $alarm->object = $type;
        $alarm->object_id = $object->id;
        $alarm->title = $title;
        $alarm->text = $text;
        $alarm->save();

        $alarm->users()->attach($receivers);


        /* Отправляем PUSH */
        $this->dispatch(new PushNotification($alarm, $receivers));


        /* Отправляем email */
        //$users = User::whereIn('id', $receivers)->get();
        //Mail::to($users)->send(new EmailNotification($title,$text));

        /* Отправляем SMS */
        //$this->dispatch(new SMSNotification($alarm, $receivers));

        return $alarm;
    }
}