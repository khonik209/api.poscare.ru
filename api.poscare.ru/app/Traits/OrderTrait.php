<?php

namespace App\Traits;

use App\Classes\ApiError;
use App\Models\Order;
use App\Models\OrderCondition;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

trait OrderTrait
{

    public function saveOrder($user, $orders_id = null, $prod_id, $quant, $lifetime = null)
    {

        //Если не передан orders_id
        if (!isset($orders_id)) {

            //Сначала нужно понять есть ли неоф. заказ
            $not_formed_order_bld = $user->orders()->where('status', 0);

            if ($not_formed_order_bld->exists()) {
                //Существует ,добавляем только товар

                $order = $not_formed_order_bld->first();


            } else {
                //Не существует ,создаём новый
                $resp = $this->makeNewOrder($user->id, null, 0, $lifetime);

                if ($resp instanceof ApiError) {
                    return $resp;
                }

                $order = $resp;
                unset($resp);
            }

        } else {

            //проверка прав пользователя на данный заказ
            $usr_order_bld = $user->orders()->where('id', $orders_id);

            if (!$usr_order_bld->exists()) {
                $err = new ApiError(308);
                return $err;
            }

            $order = $usr_order_bld->first();

        }

        //Добавляем товар
        $resp = $this->addProductToOrder($prod_id, $quant, $order);

        if ($resp instanceof ApiError) {
            return $resp;
        }

        return $order;

    }

    public function makeNewOrder($user_id, $conditions, $type, $lifetime = null)
    {

        $city_id = User::find($user_id)->city_id;
        //Не существует ,создаём новый
        $order = new Order;

        $order->user_id = $user_id;
        $order->status = 0;
        $order->is_accepted = 2;
        $order->type = $type;
        $order->city_id = $city_id;
        $order->conditions = $conditions;

        try {
            $order->save();
        } catch (QueryException $ex) {
            $err = new ApiError(310);
            return $err;
        }

        return $order;
    }

    public function addProductToOrder($prod_id, $quant, $order)
    {

        try {
            DB::transaction(function () use ($prod_id, &$quant, $order) {

                $is_upd = false;

                $resp = $this->isProductInOrder($order, $prod_id);

                if ($resp !== false) {
                    $prev_quant = $resp;
                    unset($resp);
                    $quant += $prev_quant;
                    $order->products()->updateExistingPivot($prod_id, ['quantity' => $quant]);
                    $is_upd = true;
                }

                if (!$is_upd) {
                    $order->products()->attach($prod_id, ['quantity' => $quant]);
                }
            });
        } catch (QueryException $ex) {
            $err = new ApiError(310);
            return $err;
        }

        return true;
    }

    /*
     * Возвращает false или количество товара
     * */
    public function isProductInOrder($order, $prod_id)
    {

        $ord_prods = $order->products();

        $piv_bld = $ord_prods->wherePivot('product_id', $prod_id);

        if ($piv_bld->exists()) {
            $piv = $piv_bld->first();
            $prev_quant = $piv->pivot->quantity;

            return $prev_quant;
        }

        return false;

    }

    /**
     * Прикрепляем услугу к чеку
     *
     * @param $order
     * @param $rel_serv_ids
     * @param $prod_id
     *
     * @return ApiError|array
     */
    public function addRelatedServiceToOrder($order, $rel_serv_ids, $prod_id, $type)
    {

        $ord_itm = $order->items()->where('product_id', $prod_id)->first();

        if (!$ord_itm) {
            $err = new ApiError(341, null, 'Нет товара в заказе', 'Нет товара в заказе');
            return $err;
        }

        /* Готовим массив к синхронизации */
        $ids_to_delete = $ord_itm->relatedServices->where('type', $type)->pluck('id'); // То, что нам нужно открепить.
        $ord_itm->relatedServices()->detach($ids_to_delete);
        $syncData = [];

        // $rel_serv_ids - массив вида [ [service_id=>variant_id],[service_id=>variant_id] ]
        if (json_decode($rel_serv_ids) > 0) {
            foreach (json_decode($rel_serv_ids) as $value) {
                $syncData [$value->service] = ['order_id' => $order->id, 'variant' => $value->variant===""?null:$value->variant];
            }
        }

        $ord_itm->relatedServices()->attach($syncData);

        $services = [];
        $product = Product::find($prod_id);
        $related_products_count = 0;
        $related_services_count = 0;
        $price = 0;
        foreach ($product->relatedServices as $service) {
            $selected = 0;

            if ($service->type == "service") {
                if ($ord_itm->relatedServices()->get()->pluck('id')->contains($service->id)) {
                    $selected = 1;
                    $related_services_count++;
                    $price = $price + $service->average_price;
                }
                $services[] = [
                    'id' => $service->id,
                    'name' => $service->name,
                    'description' => $service->description,
                    'selected' => $selected,
                    'average_price' => $service->average_price,
                    'image' => $service->image,
                ];
            } else {
                if ($ord_itm->relatedServices()->get()->pluck('id')->contains($service->id)) {
                    $selected = 1;
                    $related_products_count++;
                    $price = $price + $service->average_price;
                }
                $products[] = [
                    'id' => $service->id,
                    'name' => $service->name,
                    'description' => $service->description,
                    'selected' => $selected,
                    'average_price' => $service->average_price,
                    'image' => $service->image,
                ];
            }
        }
        return ['services' => $services, 'products' => $products, 'related_services_count' => $related_services_count, 'related_products_count' => $related_products_count, 'price' => $price];
    }


    /*
     * Возвращает false ,если не находит , модель OrderItem, если находит
     * */
    public function isOrdersProdHasRelServ($prod_id, $rel_serv_id, $order_id)
    {

        $order = Order::find($order_id);

        if (!isset($order)) {
            $err = new ApiError(411, null, "Нет заказа", 'Нет заказа с указанным id');
            return $err;
        }

        $item = $order->items()->where('product_id', $prod_id)->first();

        if (!$item) {
            $err = new ApiError(412, null, "Нет такого продукта в заказе", 'Нет продукта с указанным id в данном заказе');
            return $err;
        }

        $rel_service_bld = $item->relatedServices()->where('related_services_id', $rel_serv_id);

        if (!$rel_service_bld->exists()) {
            return false;
        }

        return $item;

    }

    public function deleteOrderItemsRelatedServices($item, array $rel_serv_ids)
    {

        try {
            DB::transaction(function () use ($item, $rel_serv_ids) {

                foreach ($rel_serv_ids as $rel_serv_id) {
                    $item->relatedServices()->detach($rel_serv_id);
                }

            });
        } catch (QueryException $ex) {
            $err = new ApiError(310);
            return $err;
        }

        return true;

    }

    public function getProductRelServList($product_id, $order_id)
    {

        $order = Order::find($order_id);

        $items = $order->items()->where('product_id', $product_id)->first();

        $rel_servcs_arr = [];

        $rel_servcs = $items->relatedServices;

        if (!$rel_servcs->isEmpty()) {

            foreach ($rel_servcs as $rel_servc) {

                $rel_servc_in_arr = [

                    'id' => $rel_servc->id,
                    'name' => $rel_servc->name,
                    'description' => $rel_servc->description,
                    'average_price' => $rel_servc->average_price,
                ];

                array_push($rel_servcs_arr, $rel_servc_in_arr);

            }

        }

        return $rel_servcs_arr;

    }

    /*
     * Возвращает кол-во товара ,которое осталось или 0 ,если товар был удалён
     * */
    public function changeProductQuantity($operator, $quant, $order_id, $prod_id)
    {

        $ord_itm_bld = OrderItem::where('order_id', $order_id)->where('product_id', $prod_id);

        if (!$ord_itm_bld->exists()) {
            return new ApiError(451, null, 'Товар не найден', 'Товар не найден');
        }

        $order_item = $ord_itm_bld->first();

        $prev_quant = $order_item->quantity;

        if (gettype($prev_quant) !== 'integer' || $prev_quant === 0) {
            return new ApiError(452, null, 'Ошибка сервера', 'Неверное количество в базе данных');
        }

        switch ($operator) {

            case '+':
                $new_quant = $prev_quant + $quant;
                break;
            case '-':
                $new_quant = $prev_quant - $quant;
                break;

            default:
                return new ApiError(453, null, 'Ошибка сервера', 'Передан неправильный оператор');

        }

        /* Подготавливаем ответ, собираем услуги товара */
        $services = [];
        $product = Product::find($prod_id);
        $count = 0;

        $services = collect($services);
        /* // */
        // todo При удалении товара - удалять и сопутствующие услуги?!
        if ($new_quant <= 0) {
            foreach ($product->relatedServices as $service) {
                $selected = 0;
                if ($order_item->relatedServices()->get()->pluck('id')->contains($service->id)) {
                    $selected = 1;
                    $count++;
                }
                $services[] = [
                    'id' => $service->id,
                    'name' => $service->name,
                    'description' => $service->description,
                    'selected' => 0,
                ];
            }

            //Тут удаляем товар
            $resp = $this->deleteProduct($order_id, $prod_id);

            if ($resp instanceof ApiError) {
                return $resp;
            }

            return [
                'orders_id' => $order_id,
                'quantity_in_unformed_order' => 0,
                'related_services_count' => 0,
                "related_services" => $services
            ];
        }

        foreach ($product->relatedServices as $service) {
            $selected = 0;
            if ($order_item->relatedServices()->get()->pluck('id')->contains($service->id)) {
                $selected = 1;
                $count++;
            }
            $services[] = [
                'id' => $service->id,
                'name' => $service->name,
                'description' => $service->description,
                'selected' => $selected,
                'image' => $service->image
            ];
        }

        $order_item->quantity = $new_quant;

        try {
            $order_item->save();
        } catch (QueryException $ex) {
            return new ApiError(310);
        }

        //return ['orders_id' => $order_item->orders_id, 'product_quantity' => $order_item->quantity];

        return [
            'orders_id' => $order_id,
            'quantity_in_unformed_order' => $new_quant,
            'related_services_count' => $services->where('selected', '=', 1)->count(),
            "related_services" => $services
        ];

    }

    /**
     * Удаление товара
     *
     * @param $order_id
     * @param $prod_id
     *
     * @return ApiError|array|bool
     */
    public function deleteProduct($order_id, $prod_id)
    {

        $ord_itm_bld = OrderItem::where('order_id', $order_id)->where('product_id', $prod_id);

        if (!$ord_itm_bld->exists()) {
            return new ApiError(451, null, 'Товар не найден', 'Товар не найден');
        }

        $resp = $this->getProductRelServList($prod_id, $order_id);

        if ($resp instanceof ApiError) {
            return $resp;
        }

        $prod_rel_services = $resp;

        unset($resp);

        $rel_serv_ids = [];

        foreach ($prod_rel_services as $prod_rel_service) {
            array_push($rel_serv_ids, $prod_rel_service['id']);
        }

        $item = $ord_itm_bld->first();


        try {

            if (isset($rel_serv_ids)) {

                $resp = $this->deleteOrderItemsRelatedServices($item, $rel_serv_ids);

                if ($resp instanceof ApiError) {
                    return $resp;
                }

                unset($resp);
            }

            $order = Order::find($order_id);

            $order->items()->find($item->id)->delete();
        } catch (QueryException $ex) {
            return new ApiError(310);
        }

        /* Подготавливаем ответ, собираем услуги товара */
        $services = [];
        $product = Product::find($prod_id);
        foreach ($product->relatedServices as $service) {
            $services[] = [
                'id' => $service->id,
                'name' => $service->name,
                'description' => $service->description,
                'selected' => 0,
                'image' => $service->image
            ];
        }
        $services = collect($services);
        /* // */

        return [
            'orders_id' => $order_id,
            'quantity_in_unformed_order' => 0,
            'related_services_count' => $services->where('selected', '=', 1)->count(),
            "related_services" => $services
        ];

    }

    public function formOrder($paym_id, $delivery_id, $terms, $order_id, $lifetime = null)
    {

        $order = Order::find($order_id);

        $order->payment_id = $paym_id;
        $order->delivery_id = $delivery_id;
        $order->terms = date("Y-m-d H:i:s", $terms);
        $order->status = 1;

        try {
            $order->save();
        } catch (QueryException $ex) {
            return new ApiError(310);
        }

        return true;
    }

    public function formOrderWithoutProduct($conditions, $user_id, $lifetime = null)
    {
        $resp = $this->makeNewOrder($user_id, $conditions, 1, $lifetime);

        if ($resp instanceof ApiError) {
            return $resp;
        }

        $order = $resp;

        unset($resp);

        return $order->id;

    }

}