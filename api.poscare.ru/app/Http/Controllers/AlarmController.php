<?php

namespace App\Http\Controllers;

use App\Classes\ApiError;
use App\Models\Bid;
use App\Models\News;
use App\Models\Order;
use App\Models\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AlarmController extends Controller
{
    /**
     * Посмотреть все уведомления
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotificationList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'string|in:new,is_read,deleted',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }
        $user = $this->getCurrentUser($request->input('auth_token'));


        if ($request->has('status') && $request->status != "") {
            $alarms = $user->alarms()->wherePivot('status', '=', $request->status)->get()->map(function ($value, $key) {
                $response = $value->only(['id', 'object', 'object_id', 'title', 'text', 'created_at']);
                $response['status'] = $value->pivot->status;
                return $response;
            });
        } else {
            $alarms = $user->alarms()->get()->map(function ($value, $key) {
                $response = $value->only(['id', 'object', 'object_id', 'title', 'text', 'created_at']);
                $response['status'] = $value->pivot->status;
                return $response;
            });
        }


        return response()->json([

            'response' => $alarms,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Посмотреть конкретные уведомления
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotification(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'notification_id' => 'required|integer|exists:alarms,id',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $user = $this->getCurrentUser($request->input('auth_token'));
        $alarm = $user->alarms()->find($request->notification_id);
        if (!$alarm) {
            abort(404);
        }
        $status = $alarm->pivot->status;

        if ($status == "new") {
            $user->alarms()->updateExistingPivot($alarm->id, ['status' => 'is_read']);
            $status = "is_read";
        }
        $mybranches = $user->branches->keyBy('id')->keys();
        $objectType = $alarm->object;
        if ($objectType == "news") {
            $news = News::find($alarm->object_id);
            if ($news) {
                $object = $news;
            }
        } elseif ($objectType == "bid") {

            $bid = Bid::where('id', $alarm->object_id)->first();
            if ($bid) {
                if ($user->user_type == 2) {
                    $bidResponses = $bid->bidResponses->whereIn('branch_id', $mybranches);
                    $bid->didIAnswer = ($bidResponses->count() > 0) ? 1 : 0;
                    foreach ($bidResponses as $bid_response) {
                        if ($bid_response->status == 2) { // А если я отвечал и мне отказали
                            $bid->status = 5; // Он для меня архивный
                        }
                    }
                }
                $object = $bid->only(['type','id','terms','status','created_at','description','didIAnswer']);
                $service = $bid->services()->first();
                $object['name'] = $service->name;
            }

        } elseif ($objectType == "order") {
            $order = Order::where('id', $alarm->object_id)->with(['items'])->first();
            if ($order) {

                $order->product_qty = count($order->items);
                if ($user->user_type == 2) { // Для партнера
                    $orderResponses = $order->orderResponses->whereIn('branch_id', $mybranches);
                    $order->didIAnswer = $orderResponses->count() > 0 ? 1 : 0;
                    foreach ($orderResponses as $order_response) {
                        if ($order_response->status == 2) { // А если я отвечал и мне отказали
                            $order->status = 6; // Он для меня архивный
                        }
                    }
                }
                $object = $order->only(['id','terms','status','type','created_at','conditions','product_qty','didIAnswer']);
            }


        } elseif ($objectType == "sales") {
            $sales = Sale::find($alarm->object_id);
            if ($sales) {
                $object = $sales;
            }
        }
        $alarm = $alarm->only(['id', 'title', 'text', 'created_at']);
        $alarm['status'] = $status;
        if (isset($object)) {
            $object['type'] = $objectType;
            $alarm['object'] = $object;
        }

        return response()->json([

            'response' => $alarm,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Прочитать уведомление
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function readNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'notification_id' => 'required|integer|exists:alarms,id',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $user = $this->getCurrentUser($request->input('auth_token'));
        $alarm = $user->alarms()->find($request->notification_id);
        if (!$alarm) {
            abort(404);
        }
        $status = $alarm->pivot->status;

        if ($status == "new") {
            $user->alarms()->updateExistingPivot($alarm->id, ['status' => 'is_read']);
        }

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Удалить уведомление
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteNotification(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'notification_id' => 'required|integer|exists:alarms,id',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $user = $this->getCurrentUser($request->input('auth_token'));
        $alarm = $user->alarms->find($request->notification_id);
        if (!$alarm) {
            abort(404);
        }
        $user->alarms()->updateExistingPivot($alarm->id, ['status' => 'deleted']);

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Возвращает кол-во непрочитанных уведомлений
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newNotificationCount(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));
        return response()->json([

            'response' => ['count' => $user->alarms()->wherePivot('status', 'new')->count()],

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }
}
