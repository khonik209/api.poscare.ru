<?php

namespace App\Http\Controllers\Orders;

use App\Classes\ApiError;
use App\Models\Order;
use App\Traits\OrderTrait;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrdersRelatedServicesController extends Controller {
	use OrderTrait;


	public function __construct() {
		//print 21;
	}

	/**
	 * Синхронизация услуг в корзине
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function syncRelatedServices( Request $request ) {

		$user = $this->getCurrentUserModel( $request->input( 'auth_token' ) );
		//Проверяем ,является ли покупателем
		if ( $user->user_type !== 1 ) {
			$err = new ApiError( 308 );
			return $err->json();
		}

		$validator = Validator::make( $request->all(), [
			'product_id' => 'required|integer|exists:products,id',
			'type' => 'required',
			'orders_id' => [
				'integer',
				Rule::exists( 'orders', 'id' )->where( function ( $query ) use ( $user ) {
					$query->where( 'user_id', $user->id );
				} ),
			],
			// 'related_services_ids' => 'required|integer|exists:products_related_services,related_services_id', todo Сделать валидатор
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		/* Валидацию прошли, начинаем бизнес-логику */

		$prod_id = $request->input( 'product_id' );

		//Проверяем ,есть ли заказ - корзина
		$not_formed_order_bld = $user->orders()->where( 'status', 0 )->first();

		if ( $not_formed_order_bld ) {

			$order = $not_formed_order_bld;

		} else {
			//Нет заказа ,создаём новый
			$resp = $this->makeNewOrder( $user->id, null, 0 );

			if ( $resp instanceof ApiError ) {
				return $resp->json();
			}

			$order = $resp;

			unset( $resp );
		}


		$resp = $this->isProductInOrder( $order, $prod_id );

		if ( $resp === false ) {
			unset( $resp );

			$resp = $this->addProductToOrder( $prod_id, 1, $order );

			if ( $resp instanceof ApiError ) {
				return $resp->json();
			}

			unset( $resp );

		}

		$rel_serv_ids = $request->input( 'related_services_ids' ); // может быть несколько id

		$productRelatedServices = collect( $this->addRelatedServiceToOrder( $order, $rel_serv_ids, $prod_id, $request->type ) ); // Возвращает услуги с пометкой, прикреплено или нет (selected 0|1)
		$response = [
			'orders_id' => $order->id,
			'quantity_in_unformed_order' => $order->items->where( 'product_id', $prod_id )->first()->quantity,
			'related_services_count' => $productRelatedServices['related_services_count'],
			'related_products_count' => $productRelatedServices['related_products_count'],
			"related_services" => $productRelatedServices['services'],
			"related_products" => $productRelatedServices['products'],
			"related_price"=>$productRelatedServices['price']
		];


		if ( $productRelatedServices instanceof ApiError ) {
			return $order->id->json();
		}

		return response()->json( [
			'response' => $response
		], 200, [], JSON_UNESCAPED_UNICODE );

	}

	public function getProductsRelatedServices( Request $request ) {

		$validator = Validator::make( $request->all(), [
			'orders_id' => [
				'required',
				'integer',
				'exists:orders,id'
			],
			'product_id' => [
				'required',
				'integer',
				Rule::exists( 'order_product', 'product_id' )->where( function ( $query ) use ( $request ) {
						$query->where( 'orders_id', $request->input( 'orders_id' ) );
					} ),
			],
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		$prod_id  = $request->input( 'product_id' );
		$order_id = $request->input( 'orders_id' );

		$resp = $this->getProductRelServList( $prod_id, $order_id );

		if ( $resp instanceof ApiError ) {
			return $resp->json();
		}

		return response()->json( [

			'response' => $resp,

		], 200, [], JSON_UNESCAPED_UNICODE );

	}

}
