<?php

namespace App\Http\Controllers\Orders;

use App\Classes\ApiError;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderResponse;
use App\Models\User;
use App\Traits\AlarmTrait;
use App\Traits\OrderResponseTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrdersResponsesController extends Controller
{

    use OrderResponseTrait;
    use AlarmTrait;
    public function addOrderResponse(Request $request)
    {

        $user = $this->getCurrentUserModel($request->input('auth_token'));

        if ($user->user_type !== 2) {
            $err = new ApiError(308);
            return $err->json();
        }

        $validator = Validator::make($request->all(), [
            'orders_id' => [
                'required',
                'integer',
                'exists:orders,id',
            ],
            'users_branches_id' => [
                'required', 'integer',
                Rule::exists('branches', 'id')
                    ->where(function ($query) use ($user) {
                        $query->where('user_id', $user->id);
                    }),
            ],
            'quantity' => 'required|integer',
            'quantity_type' => 'required|integer|between:1,3',
            'comment' => 'required|max:255',
            'price' => 'required|numeric'
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());

            return $err->json();

        }

        $order_id = $request->input('orders_id');
        $usr_br_id = $request->input('users_branches_id');
        $quant = $request->input('quantity');
        $quant_type = $request->input('quantity_type');
        $comment = $request->input('comment');
        $price = $request->input('price');

        $order = Order::find($order_id);

        $resp = $order->addResponse($usr_br_id, $quant, $quant_type, $comment, $price);
        if ($resp instanceof ApiError)
            return $resp->json();

        unset($resp);
	    if(Carbon::createFromTimestamp($order->created_at)->diffInMinutes(Carbon::now())<=15)
	    {
		    $user->myRating('fast_answer');
	    }
        $receivers = [$order->user_id];
        $this->createAlarm('order', $order, 'Поступило предложение на заказ', 'Поступило предложение на заказ', $receivers);


        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function getListOrderResponse(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orders_id' => [
                'required',
                'integer',
                'exists:orders,id',
            ]
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());

            return $err->json();

        }
        $user = $this->getCurrentUser($request->input('auth_token'));
        $mybranches = $user->branches->keyBy('id')->keys();
        if ($user->user_type == 2) {
            // Выборка для партнера
            $list = OrderResponse::where('order_id', $request->orders_id)->whereIn('branch_id', $mybranches)->where('status', '!=', 2)->selectRaw('id, branch_id, price, quantity, quantity_type, comment')->with(['branch.reviews', 'branch.user.userOpt'])->get();
        } else {
            // Выборка для покупателя
            $list = OrderResponse::where('order_id', $request->orders_id)->where('status', '!=', 2)->selectRaw('id, branch_id, price, quantity, quantity_type, comment')->with(['branch.reviews', 'branch.user.userOpt'])->get();
        }
        $response = $list->map(function ($item, $key) {
            return [
                'id' => $item['id'],
                'users_branches_id' => $item['branch_id'],
                'users_branches_name' => $item['branch']['name'],
                'price' => $item['price'],
                'quantity' => $item['quantity'],
                'quantity_type' => $item['quantity_type'],
                'comment' => $item['comment'],
                'reviews_count' => $item['branch']['reviews']->where('type', '=', 1)->count(),
                'reviews_rating' => round($item['branch']['reviews']->where('type', '=', 1)->avg('stars')),
                'logo_id' => $item['branch']['user']['userOpt']['image_id'],
                'logo_href' => $item['branch']['user']['userOpt']['logo']['href'],
                'address_lat' => $item['branch']['address_lat'],
                'address_lon' => $item['branch']['address_lon'],
                'address' => $item['branch']['address'],
                'rating'=>$item['branch']['user']['rating']
            ];
        });
        $sort_by = $request->sort_by?:'rating';

        return response()->json([

            'response' => collect($response)->sortByDesc($sort_by)->values(),

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function getOrderResponse(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_response_id' => [
                'required',
                'integer',
                'exists:order_responses,id',
            ]
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());

            return $err->json();

        }
        $ordResp = OrderResponse::where('id', $request->order_response_id)->with(['branch.reviews', 'branch.user.userOpt'])->first();
        $user = User::with('rewards')->find($ordResp->branch->user_id);

        $rewardsCount = $user->rewards->count();

        $response = [
            'id' => $ordResp['id'],
            'users_branches_id' => $ordResp['branch_id'],
            'users_branches_name' => $ordResp['branch']['name'],
            'price' => $ordResp['price'],
            'quantity' => $ordResp['quantity'],
            'quantity_type' => $ordResp['quantity_type'],
            'comment' => $ordResp['comment'],
            'reviews_count' => $ordResp['branch']['reviews']->where('type', '=', 1)->count(),
            'reviews_rating' => round($ordResp['branch']['reviews']->where('type', '=', 1)->avg('stars')),
            'logo_id' => $ordResp['branch']['user']['userOpt']['image_id'],
            'logo_href' => $ordResp['branch']['user']['userOpt']['logo']['href'],
            'address_lat' => $ordResp['branch']['address_lat'],
            'address_lon' => $ordResp['branch']['address_lon'],
            'address' => $ordResp['branch']['address'],
            'description' => $ordResp['branch']['description'],
            'open_hours_from' => $ordResp['branch']['open_hours_from'] ? date('H:i', strtotime($ordResp['branch']['open_hours_from'])) : null,
            'open_hours_to' => $ordResp['branch']['open_hours_to'] ? date('H:i', strtotime($ordResp['branch']['open_hours_to'])) : null,
            'city_id' => $ordResp['branch']['city_id'],
            'users_branches_user_id' => $ordResp['branch']['user_id'],
            'user_branch_images' => $ordResp['branch']['images']->map(function ($item, $key) {
                return ['id' => $item['id'], 'href' => $item['href']];
            }),
            'status' => $ordResp['status'],
            'company_description' => $ordResp['branch']['description'],
            'rating'=>$ordResp['branch']['user']['rating'],
	        'reward_count'=>$rewardsCount
        ];
        return response()->json([

            'response' => $response,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function chooseOrderResponse(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_response_id' => [
                'required',
                'integer',
                'exists:order_responses,id',
            ]
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());
            return $err->json();
        }

        $user = $this->getCurrentUserModel($request->input('auth_token'));
        $acceptedResponse = OrderResponse::find($request->order_response_id);

        if ($acceptedResponse->order->user_id != $user->id) {
            $err = new ApiError(308,
                NULL,
                NULL,
                'Заказ не принадлежит пользователю');
            return $err->json();
        }

        $order = $acceptedResponse->order;
        $order->status = 3;
        $order->save();

        $allResponses = $order->orderResponses->map(function ($response, $key) use ($acceptedResponse) {
            if ($response->id == $acceptedResponse->id) {
                $response->status = 1;
                $response->branch->user->myRating('chosen');
                $receivers = [$response->branch->user_id];
                $this->createAlarm('order', $response->order, 'Вас выбрали исполнителем', 'Вас выбрали исполнителем', $receivers);
            } else {
                $response->status = 2;
                $receivers = [$response->branch->user_id];
                $this->createAlarm('order', $response->order, 'Вам отказали', 'Вам отказали', $receivers);
            }
            $response->save();
        });

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function refuseOrderResponse(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_response_id' => [
                'required',
                'integer',
                'exists:order_responses,id',
            ]
        ]);

        $user = $this->getCurrentUserModel($request->input('auth_token'));

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());
            return $err->json();
        } else {
            $order_response = OrderResponse::find($request->order_response_id);
            if ($order_response->order->user_id != $user->id) {
                $err = new ApiError(308,
                    NULL,
                    NULL,
                    'Заказ не принадлежит пользователю');
                return $err->json();
            }
        }

        if ($order_response->status == 1) {
            $order = $order_response->order;
            $checkNewResponses = $order->orderResponses()->with('branch', 'branch.user')->where('id', '!=', $order_response->id)->get();
            if ($checkNewResponses->count() > 0) {
                $nextResp = $checkNewResponses->sortByDesc(function ($r, $k) {
                    if (!$r->branch || !$r->branch->user) {
                        return 0;
                    }
                    return $r->branch->user->rating;
                })->first();
                if ($nextResp) {
                    $nextResp->status = 1;
                    $nextResp->save();
                    $nextResp->branch->user->myRating('chosen');
                    $receivers = [$nextResp->branch->user_id];
                    $this->createAlarm('order', $nextResp->order, 'Вас выбрали исполнителем', 'Вас выбрали исполнителем', $receivers);
                }

            }
        }

        $order_response->status = 2;
        $order_response->save();

        $receivers = [$order_response->branch->user_id];
        $this->createAlarm('order', $order_response->order, 'Вам отказали', 'Вам отказали', $receivers);


        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function orderResponseContacts(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_response_id' => 'required|integer|exists:order_responses,id',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());

            return $err->json();

        }

        $bid_resp = OrderResponse::find($request->input('order_response_id'));

        $branch = $bid_resp->branch()->first();

        $resp = [];

        $resp['address'] = $branch->address;

        $user = $branch->user()->first();


        $resp['email'] = $user->email;

        $resp['phone'] = $user->phoneSalt->phone;

        return response()->json([

            'response' => $resp,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function deleteOrderResponse(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'response_id' => 'required|integer|exists:order_responses,id',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());

            return $err->json();

        }

        $user = $this->getCurrentUserModel($request->input('auth_token'));
        $bid_resp_id = $request->input('response_id');

        $bid_resp = OrderResponse::find($bid_resp_id);

        $branch_id = $bid_resp->branch->id;

        $bid_resp_bld = $user->branches()
            ->find($branch_id)
            ->OrderResponses()
            ->where('id', $bid_resp_id);

        $bid_resp = $bid_resp_bld->first();

        if (!$bid_resp_bld->exists()) {

            $err = new ApiError(308);

            return $err->json();

        }

        if ($bid_resp->status === 1) {

            $bid_status = $bid_resp->order->status;

            if ($bid_status === 2 || $bid_status === 3) {

                $err = new ApiError(341,
                    NULL,
                    'Вы не можете удалить предложение на данном этапе',
                    'Нельзя удалить предложение при данном статусе order_status = '
                    . $bid_status . ' ,будучи исполнителем');
                return $err->json();

            }

        }

        $is_deleted = $bid_resp->delete();

        if (!$is_deleted) {
            $err = new ApiError(310);
            return $err->json();
        }

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }
}
