<?php

namespace App\Http\Controllers\Orders;

use App\Classes\ApiError;
use App\Models\Branch;
use App\Models\Order;
use App\Models\Product;
use App\Traits\AlarmTrait;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Traits\OrderTrait;
use App\Models\OrderResponse;

class OrdersController extends Controller
{
    use OrderTrait;
    use AlarmTrait;

    public function addOrderProduct(Request $request)
    {

        $user = $this->getCurrentUserModel($request->input('auth_token'));

        //Проверяем ,является ли покупателем

        if ($user->user_type !== 1) {
            $err = new ApiError(308);
            return $err->json();
        }

        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer|exists:products,id',
            'orders_id' => [
                'integer',
                Rule::exists('orders', 'id')->where(function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                }),
            ],
            'quantity' => 'required|integer',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $ord_id = $request->input('orders_id');
        $prod_id = $request->input('product_id');
        $quant = $request->input('quantity');

        $response = $this->saveOrder($user, $ord_id, $prod_id, $quant, $request->lifetime);

        if ($response instanceof ApiError) {
            return $response->json();
        }

        return response()->json([

            'response' => [
                'orders_id' => $response->id,
                'quantity_in_unformed_order' => $response->items->where('product_id', $prod_id)->first()->quantity
            ],

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Возвращает список товаров в корзине
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderProductsList(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'orders_id' => [
                'required',
                'integer',
                'exists:orders,id',
            ]
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $ord_id = $request->input('orders_id');

        $order = Order::with([
            'products' => function ($query) {
                $query->with(['images', 'relatedServices']);
            }
        ])->find($ord_id);

        $response = [];
        // todo: отверактироть,слишком медленный скрипт. Избавиться от циклов
        foreach ($order->items as $orderItem) {

            $product = $orderItem->product;

            $related_products = [];
            $related_services = [];
            $services_count = 0;
            $products_count = 0;
            $related_price = 0;
            foreach ($product->relatedServices as $service) {
                $selected = 0;

                // Посмотрим варианты
                $variants = [];
                if (count($service->variants) > 0) {
                    foreach ($service->variants as $key => $var) {
                        $variants[$key] = [
                            'variant_id' => $key,
                            'image' => $service->image,
                            'name' => $var->name . " " . $var->value,
                            'average_price' => $service->average_price,
                            'selected' => 0,
                            'parent_id' => $service->id
                        ];
                    }
                }

                if ($service->type == "service") {
                    if ($orderItem->relatedServices()->orderBy('related_services.order')->get()->pluck('id')->contains($service->id)) {
                        $selected = 1;
                        $services_count++;
                        $related_price = $related_price + $service->average_price;
                    }
                    $related_services[] = [
                        'id' => $service->id,
                        'name' => $service->name,
                        'description' => $service->description,
                        'selected' => $selected,
                        'image' => $service->image,
                        'average_price' => $service->average_price,
                        'is_variants' => count($variants) > 0 ? 1 : 0,
                        'variants' => $variants,
                    ];
                } else {
                    if ($orderItem->relatedServices()->orderBy('related_services.order')->get()->pluck('id')->contains($service->id)) {
                        $selected = 1;
                        $products_count++;
                        $related_price = $related_price + $service->average_price;
                    }
                    $related_products[] = [
                        'id' => $service->id,
                        'name' => $service->name,
                        'description' => $service->description,
                        'selected' => $selected,
                        'image' => $service->image,
                        'average_price' => $service->average_price,
                        'is_variants' => count($variants) > 0 ? 1 : 0,
                        'variants' => $variants,
                    ];
                }
            }

            $response[] = [
                'id' => $product->id,
                'quantity_in_unformed_order' => $orderItem->quantity,
                'name' => $product->name,
                'description' => $product->description,
                'average_price' => $product->average_price,
                'category_id' => $product->category_id,
                'category_name' => $product->category->name,
                'images' => $product->images->map(function ($i, $k) {
                    return [
                        'img_id' => $i->id,
                        'img_href' => $i->href
                    ];
                }),
                'related_services' => $related_services,
                'related_services_count' => $services_count,
                'related_products' => $related_products,
                'related_products_count' => $products_count,
                'related_price' => $related_price
            ];
        }

        return response()->json([

            'response' => $response,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }


    public function getOrdersList(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));
        if ($user->user_type != 2) {
            $err = new ApiError(308, null, 'Данный метод недоступен для покупателя', null);
            return $err->json();
        }
        $validator = Validator::make($request->all(), [
            'orders_status' => Rule::in([0, 2, 3, 4, 5, 6, '']),
            'didIAnswer' => 'boolean'
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }

        /* Прошли валидацию, начинаем бизнес-логику */
        $mycities = $user->branches->keyBy('city_id')->keys(); // id городов, в которых расположены филиалы партнера
        $mybranches = $user->branches->keyBy('id')->keys(); // id филиалов партнера


        if ($request->orders_status == 2 && !$request->didIAnswer) {
            $newOrders = Order::where('is_private', '=', 0)->where('is_accepted', '=', 1)->where('status', '=', 2)->whereIn('city_id', $mycities);

            $response = $newOrders->with([
                'items',
                'orderResponses' => function ($query) use ($mybranches) {
                    $query->whereIn('branch_id', $mybranches);
                }
            ])->get([
                'id',
                'terms',
                'created_at',
                'updated_at',
                'status',
                'is_accepted',
                'payment_id',
                'delivery_id',
                'user_id',
                'is_review',
                'type'
            ]);

        } elseif ($request->orders_status == 2 && $request->didIAnswer == 1) {
            $candidateOrders = Order::where('is_private', '=', 0)->where('is_accepted', '=', 1)->where('status', '=', 2)->whereHas('orderResponses', function ($query) use ($mybranches) {
                $query->whereIn('branch_id', $mybranches)->where('status', '!=', 2);
            });

            $response = $candidateOrders->with([
                'items',
                'orderResponses' => function ($query) use ($mybranches) {
                    $query->whereIn('branch_id', $mybranches);
                }
            ])->get([
                'id',
                'terms',
                'created_at',
                'updated_at',
                'status',
                'is_accepted',
                'payment_id',
                'delivery_id',
                'user_id',
                'is_review',
                'type'
            ]);

        } elseif ($request->orders_status == 3) {
            $waitingForPaymentOrders = Order::where('is_private', '=', 0)->where('is_accepted', '=', 1)->where('status', '=', 3)->whereHas('orderResponses', function ($query) use ($mybranches) {
                $query->whereIn('branch_id', $mybranches)->where('status', '=', 1);
            });

            $response = $waitingForPaymentOrders->with([
                'items',
                'orderResponses' => function ($query) use ($mybranches) {
                    $query->whereIn('branch_id', $mybranches);
                }
            ])->get([
                'id',
                'terms',
                'created_at',
                'updated_at',
                'status',
                'is_accepted',
                'payment_id',
                'delivery_id',
                'user_id',
                'is_review',
                'type'
            ]);

        } elseif ($request->orders_status == 4) {
            $inJobOrders = Order::where('is_private', '=', 0)->where('is_accepted', '=', 1)->where('status', '=', 4)->whereHas('orderResponses', function ($query) use ($mybranches) {
                $query->whereIn('branch_id', $mybranches)->where('status', '=', 1);
            });

            $response = $inJobOrders->with([
                'items',
                'orderResponses' => function ($query) use ($mybranches) {
                    $query->whereIn('branch_id', $mybranches);
                }
            ])->get([
                'id',
                'terms',
                'created_at',
                'updated_at',
                'status',
                'is_accepted',
                'payment_id',
                'delivery_id',
                'user_id',
                'is_review',
                'type'
            ]);
        } elseif ($request->orders_status == 5) {
            $finishedOrders = Order::where('is_private', '=', 0)->where('is_accepted', '=', 1)->where('status', '=', 5)->whereHas('orderResponses', function ($query) use ($mybranches) {
                $query->whereIn('branch_id', $mybranches)->where('status', '=', 1);
            });

            $response = $finishedOrders->with([
                'items',
                'orderResponses' => function ($query) use ($mybranches) {
                    $query->whereIn('branch_id', $mybranches);
                }
            ])->get([
                'id',
                'terms',
                'created_at',
                'updated_at',
                'status',
                'is_accepted',
                'payment_id',
                'delivery_id',
                'user_id',
                'is_review',
                'type'
            ]);
        } elseif ($request->orders_status == 6) {
            $archiveOrders = Order::where('is_private', '=', 0)->where('is_accepted', '=', 1)->where('status', '=', 6)->whereHas('orderResponses', function ($query) use ($mybranches) {
                $query->whereIn('branch_id', $mybranches);
            });

            $canceledOrders = Order::where('is_private', '=', 0)->where('is_accepted', '=', 1)->whereHas('orderResponses', function ($query) use ($mybranches) {
                $query->whereIn('branch_id', $mybranches)->where('status', 2);
            });

            $response_1 = $archiveOrders->with([
                'items',
                'orderResponses' => function ($query) use ($mybranches) {
                    $query->whereIn('branch_id', $mybranches);
                }
            ])->get([
                'id',
                'terms',
                'created_at',
                'updated_at',
                'status',
                'is_accepted',
                'payment_id',
                'delivery_id',
                'user_id',
                'is_review',
                'type'
            ]);

            $response_2 = $canceledOrders->with([
                'items',
                'orderResponses' => function ($query) use ($mybranches) {
                    $query->whereIn('branch_id', $mybranches);
                }
            ])->get([
                'id',
                'terms',
                'created_at',
                'updated_at',
                'status',
                'is_accepted',
                'payment_id',
                'delivery_id',
                'user_id',
                'is_review',
                'type'
            ]);

            $response = $response_1->merge($response_2);

        } else {
            $newOrders = Order::where('is_private', '=', 0)->where('is_accepted', 1)->whereIn('city_id', $mycities)->where('status', '=', 2);

            $allOrders = Order::where('is_private', '=', 0)->where('is_accepted', 1)->whereHas('orderResponses', function ($query) use ($mybranches) {
                $query->whereIn('branch_id', $mybranches); // Возвращаем заказы с статусом 5, на которые есть ответ от филиала партнера со статусом 1
            });

            $response_1 = $newOrders->with([
                'items',
                'orderResponses' => function ($query) use ($mybranches) {
                    $query->whereIn('branch_id', $mybranches);
                }
            ])->get([
                'id',
                'terms',
                'created_at',
                'updated_at',
                'status',
                'is_accepted',
                'payment_id',
                'delivery_id',
                'user_id',
                'is_review',
                'type'
            ]);

            $response_2 = $allOrders->with([
                'items',
                'orderResponses' => function ($query) use ($mybranches) {
                    $query->whereIn('branch_id', $mybranches);
                }
            ])->get([
                'id',
                'terms',
                'created_at',
                'updated_at',
                'status',
                'is_accepted',
                'payment_id',
                'delivery_id',
                'user_id',
                'is_review',
                'type'
            ]);


            $response = $response_1->merge($response_2);
        }

        $response = $response->sortByDesc('created_at')->toArray();
        foreach ($response as $i => &$order) {
            $order['didIAnswer'] = 0;
            $order['order_responses_count'] = count($order['order_responses']);
            if (count($order['order_responses']) > 0) {
                $order['didIAnswer'] = 1;
                foreach ($order['order_responses'] as $order_response) {
                    if ($order_response['status'] == 2) { // А если я отвечал и мне отказали
                        if (!$request->orders_status || $request->orders_status == 6) {
                            $order['status'] = 6; // Он для меня архивный
                        } else {
                            unset($response[$i]);
                            continue;
                        }
                    }
                }
            }

            if ($request->has('didIAnswer')) {
                if ($order['didIAnswer'] != $request->didIAnswer) {
                    // array_splice($response, $i);
                    unset($response[$i]);
                    continue;
                }
            }

            $order['products_quantity'] = count($order['items']);
            unset($order['items']);
            unset($order['order_responses']);
        }


        return response()->json([

            'response' => array_values($response),

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Удалить товар из корзины
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteOrderProduct(Request $request)
    {

        $user = $this->getCurrentUserModel($request->input('auth_token'));

        //Проверяем ,является ли покупателем

        if ($user->user_type !== 1) {
            $err = new ApiError(308);
            return $err->json();
        }

        $validator = Validator::make($request->all(), [
            'orders_id' => [
                'required',
                'integer',
                Rule::exists('orders', 'id')->where(function ($query) use ($user) {
                    $query->where(['user_id' => $user->id, 'status' => 0]);
                }),
            ],
            'product_id' => [
                'required',
                'integer',
                Rule::exists('order_product', 'product_id')->where(function ($query) use ($request) {
                    $query->where('order_id', $request->input('orders_id'));
                }),
            ],
            'quantity' => 'integer|filled',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $order_id = $request->input('orders_id');
        $prod_id = $request->input('product_id');

        if ($request->has('quantity')) {
            $quant = $request->input('quantity');

            $resp = $this->changeProductQuantity('-', $quant, $order_id, $prod_id);

        } else {
            $resp = $this->deleteProduct($order_id, $prod_id);
        }

        if ($resp instanceof ApiError) {
            return $resp->json();
        }

        // unset($resp);

        return response()->json([

            'response' => $resp,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function cancelOrder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'orders_id' => 'required|integer'
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $order_id = $request->input('orders_id');

        $user = $this->getCurrentUserModel($request->input('auth_token'));

        $myBranches = $user->branches->pluck('id');

        $order = Order::where('id', $order_id)->where(function ($q) use ($user, $myBranches) {
            $q->where('user_id', $user->id)->orWhereHas('orderResponses', function ($query) use ($myBranches) {
                $query->whereIn('branch_id', $myBranches);
            });
        })->first();

        if (!$order) {
            $err = new ApiError(341, null, 'Заказ не найден', 'нет такого заказа id - ' . $order_id);
            return $err->json();
        }

        $order->status = 6;

        try {
            $order->save();
        } catch (QueryException $ex) {
            $err = new ApiError(310);
            return $err->json();
        }

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function getOrder(Request $request, $count = null)
    {
        $user = $this->getCurrentUserModel($request->input('auth_token'));

        $validator = Validator::make($request->all(), [
            'orders_id' => [
                'required',
                'integer',
                Rule::exists('orders', 'id')->where(function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                }),
            ],
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $order_id = $request->input('orders_id');

        $order = Order::with(['paymentMethod', 'delivery', 'items'])->find($order_id);

        if ($count) {
            $response = [
                'products_quantity' => ($order->type == 0) ? $order->items->count() : 0,
            ];
        } else {
            $response = [
                'id' => $order->id,
                'status' => $order->status,
                'payment_id' => $order->payment_id,
                'payment_name' => $order->paymentMethod['name'],
                'delivery_id' => $order->delivery_id,
                'delivery_name' => $order->delivery['name'],
                'terms' => isset($order->terms) ? ($order->terms) : null,
                'products_quantity' => ($order->type == 0) ? $order->items->count() : 0,
                // 'user_id' => $order->user_id,
                'is_review' => $order->is_review,
                'type' => $order->type,
                'created_at' => isset($order->created_at) ? ($order->created_at) : null,
                'updated_at' => isset($order->updated_at) ? ($order->updated_at) : null,
                'conditions' => json_decode($order->conditions, true)
            ];
        }

        return response()->json([

            'response' => $response,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Оформить заказ
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeOrderFormed(Request $request)
    {
        $user = $this->getCurrentUserModel($request->input('auth_token'));

        //Проверяем ,является ли покупателем

        if ($user->user_type !== 1) {
            $err = new ApiError(308);
            return $err->json();
        }

        $validator = Validator::make($request->all(), [
            'payment_id' => [
                'required',
                'integer',
                'exists:payment_methods,id',
            ],
            'delivery_id' => 'required|integer|exists:deliveries,id',

            'terms' => [
                'required',
                'integer',
                'min:' . time(),
            ],

            'type' => 'required|integer|between:0,1',

            'orders_id' => [
                'required_if:type,0',
                'integer',
                Rule::exists('orders', 'id')->where(function ($query) use ($user) {
                    $query->where('user_id', $user->id)->where('status', 0)->orWhere('status', 6);
                }),
            ],
            'conditions' => 'required_if:type,1|json',
            'buyer_dop_cred' => 'json|max:1000'
        ]);
        $validator->after(function ($validator) use ($request) {
            if ($request->has('buyer_dop_cred')) {
                $buyer_dop_cred = json_decode($request->buyer_dop_cred, true);
                $avail_fields = ['fname', 'lname', 'phone', 'address', 'email'];
                foreach ($buyer_dop_cred as $key => $value) {
                    if (!in_array($key, $avail_fields)) {
                        $validator->errors()->add($key, 'Недопустимое поле - ' . $key);
                    }
                }
            }
        });

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        /* Прошли валидацию - начинаем бизнес-логику */
        $paym_id = $request->input('payment_id');
        $delivery_id = $request->input('delivery_id');
        $terms = $request->input('terms');
        $type = $request->input('type');

        //Оформляем заказ без товара
        if ($type == '0') {

            $order_id = $request->input('orders_id');
        } else {
            $conditions = $request->conditions ? $request->conditions : null;

            $resp = $this->formOrderWithoutProduct($conditions, $user->id);

            if ($resp instanceof ApiError) {
                return $resp->json();
            }

            $order_id = $resp;

            unset($resp);

        }

        $resp = $this->formOrder($paym_id, $delivery_id, $terms, $order_id, $request->lifetime);

        if ($resp instanceof ApiError) {
            return $resp->json();
        }


        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function getOrderConditions(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orders_id' => [
                'required',
                'integer',
            ],
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $response = Order::find($request->orders_id)->only('conditions');

        return response()->json([

            'response' => json_decode($response['conditions'], true),

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Вывод конкретного заказа партнера
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPartnerOrder(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));
        $validator = Validator::make($request->all(), [
            'orders_id' => 'required|integer|exists:orders,id',
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }
        $mybranches = $user->branches->keyBy('id')->keys();
        $order = Order::where('id', $request->orders_id)->with([
            'paymentMethod',
            'delivery',
            'items',
            'orderResponses' => function ($query) use ($mybranches) {
                $query->whereIn('branch_id', $mybranches);
            }
        ])->first();

        if ($order->status == 0 || ($order->status > 2 && $order->orderResponses->where('status', 1)->count() == 0)) {
            $err = new ApiError(308, null, 'Вы не имеете доступа к этой заявке', null);
            return $err->json();
        }

        $response = [
            'id' => $order->id,
            'status' => $order->status,
            'payment_id' => $order->payment_id,
            'payment_name' => $order->paymentMethod['name'],
            'delivery_id' => $order->delivery_id,
            'delivery_name' => $order->delivery['name'],
            'terms' => isset($order->terms) ? ($order->terms) : null,
            'products_quantity' => ($order->type == 0) ? $order->items->count() : 0,
            'is_review' => $order->is_review,
            'type' => $order->type,
            'created_at' => isset($order->created_at) ? ($order->created_at) : null,
            'updated_at' => isset($order->updated_at) ? ($order->updated_at) : null,
            'conditions' => json_decode($order->conditions, true),
            'didIAnswer' => ($order->orderResponses->count() > 0) ? 1 : 0,
            'order_responses_count' => $order->orderResponses->count()
        ];

        $orderResponse = $order->orderResponses->first();
        if ($orderResponse) {
            $response['chosen_response_id'] = $orderResponse->id;
            if ($orderResponse->status == 2) {
                $response['status'] = 6;
            }
        }

        if ($order->user) {
            $founder = [
                'name' => $order->user->name . ' ' . $order->user->surname,
                'phone' => $order->user->phoneSalt ? $order->user->phoneSalt->phone : '',
                'email' => $order->user->email,
                'city' => $order->user->city ? $order->user->city->name : '',
            ];
            $response['founder'] = $founder;
        }

        return response()->json([

            'response' => $response,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function setOrderPayed(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));

        $validator = Validator::make($request->all(), [
            'orders_id' => [
                'bail',
                'required',
                'integer',
                Rule::exists('orders', 'id')->where(function ($query) {
                    $query->where('status', 2);
                })
            ],
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }
        $mybranches = $user->branches->keyBy('id')->keys();
        $order = Order::where('id', $request->orders_id)->with([
            'orderResponses' => function ($query) use ($mybranches) {
                $query->whereIn('branch_id', $mybranches);
            }
        ])->first();
        if ($order->orderResponses->where('status', 1)->count() == 0) {
            $err = new ApiError(308, null, 'Вы не имеете доступа к этой заявке', null);
            return $err->json();
        }
        $order->status = 3;
        $order->save();

        $receivers = [$order->user_id];
        $this->createAlarm('order', $order, 'Оплата поступила', 'Оплата поступила', $receivers);

        return response()->json(['response' => 1], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function setOrderDelivered(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));

        $validator = Validator::make($request->all(), [
            'orders_id' => [
                'bail',
                'required',
                'integer',
                Rule::exists('orders', 'id')->where(function ($query) {
                    $query->where('status', 3);
                })
            ],
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }
        $mybranches = $user->branches->keyBy('id')->keys();
        $order = Order::where('id', $request->orders_id)->with([
            'orderResponses' => function ($query) use ($mybranches) {
                $query->whereIn('branch_id', $mybranches);
            }
        ])->first();
        if ($order->orderResponses->where('status', 1)->count() == 0) {
            $err = new ApiError(308, null, 'Вы не имеете доступа к этой заявке', null);
            return $err->json();
        }
        $order->status = 4;
        $order->save();

        $receivers = [$order->user_id];
        $this->createAlarm('order', $order, 'Доставлено', 'Доставлено', $receivers);

        return response()->json(['response' => 1], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function orderComplete(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));
        $validator = Validator::make($request->all(), [
            'orders_id' => [
                'required',
                'integer',
                Rule::exists('orders', 'id')->where(function ($query) use ($user) {
                    $query->where(['user_id' => $user->id, 'is_buyer_complete' => 0, 'status' => 3]);
                }),
            ],
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }
        $order = Order::where('id', $request->orders_id)->first();
        $order->is_buyer_complete = 1;
        if ($order->is_partner_complete == 1) {
            $order->status = 4;
        }
        $order->save();

        $orderResponse = $order->orderResponses->where('status', '=', 1)->first();
        if (!$orderResponse) {
            return response()->json([

                'response' => ['order_status' => $order->status, 'warning' => "Уведомление не создалось. BidResponse не найден."],

            ], 200, [], JSON_UNESCAPED_UNICODE);
        }

        $receivers = [$orderResponse->branch->user->id];
        $this->createAlarm('order', $order, 'Заказ был завершен покупателем', 'Заказ был завершен покупателем', $receivers);
        if ($order->is_partner_complete == 1 && $order->is_complete == 1) {
            $receivers = [$order->user_id];
            $this->createAlarm('order', $order, 'Вам необходимо оставить отзыв партнеру', 'Вам необходимо оставить отзыв партнеру', $receivers);
        }

        return response()->json([

            'response' => ['order_status' => $order->status],

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function orderPartnerComplete(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));

        $validator = Validator::make($request->all(), [
            'orders_id' => [
                'bail',
                'required',
                'integer',
                Rule::exists('orders', 'id')->where(function ($query) {
                    $query->where(['is_partner_complete' => 0, 'status' => 3]);
                }),
            ],
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }

        $mybranches = $user->branches()->get()->keyBy('id')->keys()->toArray();

        $orderresponses = OrderResponse::where(['order_id' => $request->orders_id, 'status' => 1])->whereIn('branch_id', $mybranches)->get();

        if ($orderresponses->isEmpty()) {
            $err = new ApiError(308, null, null, ['error' => 'Вы не являетесь исполнителем по этой заявке']);
            return $err->json();
        }

        $order = Order::where('id', $request->orders_id)->first();
        $order->is_partner_complete = 1;
        if ($order->is_buyer_complete == 1) {
            $order->status = 4;
        }
        $order->save();

        $receivers = [$order->user_id];
        $this->createAlarm('order', $order, 'Заказ был завершен партнером', 'Заказ был завершен партнером', $receivers);

        if ($order->is_partner_complete == 1 && $order->is_complete == 1) {
            $this->createAlarm('order', $order, 'Вам необходимо оставить отзыв партнеру', 'Вам необходимо оставить отзыв партнеру', $receivers);
        }

        return response()->json([

            'response' => ['order_status' => $order->status],

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Смена статуса
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function switchStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orders_id' => 'required|integer',
            'status' => 'required|integer'
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }
        $order = Order::find($request->orders_id);
        if ($order) {
            $order->status = $request->status;
            if ($request->status == 5) {
                $order->is_review = 1;
            }
            $order->save();
        }

        $receivers = [$order->user_id];
        $this->createAlarm('order', $order, 'у вашего заказа сменился статус', 'у вашего заказа сменился статус', $receivers);
        if ($order->status == 5) {
            $this->createAlarm('order', $order, 'Вам необходимо оставить отзыв партнеру', 'Вам необходимо оставить отзыв партнеру', $receivers);
        }

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function refreshOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|integer|exists:orders,id',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        //Проверка прав
        $user = $this->getCurrentUser($request->input('auth_token'));

        $order = $user->orders()->where('id', $request->input('order_id'))->where(function ($query) {
            $query->where('status', 2)->orWhere('status', 3)->orWhere('status', 4);
        })->first();

        if (!$order) {
            $err = new ApiError(308);
            return $err->json();
        }

        $order->status = 1;
        $order->save();

        $order->orderResponses()->delete();

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function repeatOrder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'order_id' => 'required|integer|exists:orders,id',
            'terms' => 'required|integer',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        //Проверка прав
        $user = $this->getCurrentUser($request->input('auth_token'));

        if ($user->user_type !== 1) {
            $err = new ApiError(308, null, "Только покупатель может выполнить это действие");
            return $err->json();
        }

        $order = $user->orders()->where('id', $request->input('order_id'))->where(function ($query) {
            $query->where('status', 2)->orWhere('status', 3)->orWhere('status', 4);
        })->first();

        if (!$order) {
            $err = new ApiError(308);
            return $err->json();
        }


        if ($order->is_accepted === 0 && ($order->status === 2 || $order->status === 3)) {
            $err = new ApiError(343, null, 'Заказ не принят модератором', 'Заказ не принят модератором');
            return $err->json();
        }

        $newOrder = new Order;

        $terms = $request->input('terms');

        if ($terms <= time()) {
            $err = new ApiError(341, null, 'срок заявки должен быть позже текущего времени', 'срок заявки должен быть позже текущего времени');
            return $err->json();
        }

        $newOrder->conditions = $order->conditions;
        $newOrder->user_id = $order->user_id;
        $newOrder->delivery_id = $order->delivery_id;
        $newOrder->status = $order->status;
        $newOrder->type = $order->type;


        $newOrder->is_accepted = $order->is_accepted === 1 ? 1 : 2;


        $newOrder->terms = date("Y-m-d H:i:s", $terms);

        $newOrder->save();

        $founder = $newOrder->user;
        $city_id = $founder->city_id;
        $receivers = Branch::where('city_id', $city_id)->get()->pluck('user_id')->unique();
        $this->createAlarm('order', $newOrder, 'Был размещен повторный заказ', 'Был размещен повторный заказ', $receivers);

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }
}
