<?php

namespace App\Http\Controllers;

use App\Classes\ApiError;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use YandexCheckout\Client;

class YandexController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sum' => 'required|integer'
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $user = $this->getCurrentUserModel($request->input('auth_token'));
        $payment = new Payment;
        $payment->user_id = $user->id;
        $payment->sum = $request->sum;
        $payment->save();

        $client = new Client();
        $client->setAuth(config('yandex.id'), config('yandex.id'));
        $ya_payment = $client->createPayment(
            [
                'amount' => [
                    'value' => $payment->sum,
                    'currency' => 'RUB',
                ],
                'confirmation' => [
                    'type' => 'redirect',
                    'return_url' => url('/api/yandex/success'),
                ],
                'description' => 'Заказ №' . $payment->id,
                'capture' => true,
                'metadata' => json_encode([
                    'payment_id' => $payment->id,
                    'user_id' => $user->id
                ])
            ],
            uniqid('', true)
        );
        $payment->invoice_id = $ya_payment->id;
        $payment->save();
        return response()->json(
            [
                'payment' => $payment,
                'yandex' => $ya_payment
            ]
        );

    }

    public function check(Request $request)
    {
        $check = $request->all();

        $payment = Payment::find($check->metadata->payment_id);
        if (!$payment) {
            Log::error('Payment Aviso not found: ' . json_encode($request->all()));
            return response()->json([
                'response' => 'Payment not found'
            ], 404);
        }

        if ($payment->invoice_id != $check->id) {
            Log::error('Payment and YandexPayment Ids dont confirm: ' . json_encode($request->all()));
            return response()->json([
                'response' => 'Payment and YandexPayment Ids dont confirm: '
            ], 500);
        }

        $payment->check = 1;
        $payment->log_check = json_encode($check);
        $payment->save();

        $client = new Client();
        $client->setAuth(config('yandex.id'), config('yandex.id'));
        $client->capturePayment(
            array(
                'amount' => $payment->sum,
            ),
            $payment->id,
            uniqid('', true)
        );

        return response()->json([], 200);
    }

    public function aviso(Request $request)
    {
        /*

        {
  "id": "227cf565-000f-5000-8000-1c9d1c6000fb",
  "status": "succeeded",
  "paid": true,
  "amount": {
    "value": "2.00",
    "currency": "RUB"
  },
  "authorization_details": {
    "rrn": "10000000000",
    "auth_code": "000000"
  },
  "captured_at": "2018-05-03T10:17:31.487Z",
  "created_at": "2018-05-03T10:17:09.337Z",
  "metadata": {},
  "payment_method": {
    "type": "bank_card",
    "id": "227cf565-000f-5000-8000-1c9d1c6000fb",
    "saved": false,
    "card": {
      "first6": "411111",
      "last4": "1111",
      "expiry_month": "01",
      "expiry_year": "2020",
      "card_type": "Visa"
    },
    "title": "Bank card *1111"
  },
  "receipt_registration": "pending",
  "recipient": {
    "account_id": "67192",
    "gateway_id": "352780"
  },
  "refunded_amount": {
    "value": "2.00",
    "currency": "RUB"
  }
}
         */

        $aviso = $request->all();

        $payment = Payment::find($aviso->metadata->payment_id);
        if (!$payment) {
            Log::error('Payment Aviso not found: ' . json_encode($request->all()));
            return response()->json([
                'response' => 'Payment not found'
            ], 404);
        }

        if ($payment->invoice_id != $aviso->id) {
            Log::error('Payment and YandexPayment Ids dont confirm: ' . json_encode($request->all()));
            return response()->json([
                'response' => 'Payment and YandexPayment Ids dont confirm: '
            ], 500);
        }

        $payment->check = 1;
        $payment->aviso = 1;
        $payment->log_aviso = json_encode($aviso);
        $payment->save();

        return response()->json([1], 200);
    }

    public function info(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sum' => 'required|integer'
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

    }
}
