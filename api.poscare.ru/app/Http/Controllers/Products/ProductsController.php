<?php

namespace App\Http\Controllers\Products;

use App\Classes\ApiError;
use App\Models\Product;
use App\Models\Order;
use App\Models\ProductCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\PartnerSupply;

class ProductsController extends Controller
{
    public function addProduct(Request $request)
    {

        $validator = Validator::make($request->all(), [
            //	'product_id' => 'required|integer|exists:products,id',
            'name' => 'required|unique:products,name',
            'description' => 'required|max:250',
            'category_id' => 'required|integer|exists:products_category,id',
            'average_price' => 'required|numeric',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $cat_id = $request->input('category_id');

        $prod_cat = ProductCategory::where('id', $cat_id);

        if (!$prod_cat->exists()) {

            $err = new ApiError(341, null, 'Выберите категорию товара', 'Категория товара не найдена');
            return $err->json();

        }

        $prod = new Product;

        $prod->name = $request->input('name');
        $prod->description = $request->input('description');
        $prod->category_id = $request->input('category_id');
        $prod->average_price = $request->input('average_price');

        try {

            $prod->save();

        } catch (QueryException $ex) {
            $err = new ApiError(310);
            return $err->json();
        }

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Выводит все товары
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductsList(Request $request)
    {

        $products_bld = Product::with(['relatedServices'])->where('is_actual', 1)->orderBy('order', 'asc');

        if ($request->exists('category_id') && $request->filled('category_id')) {
            $products_bld = $products_bld->where('category_id', $request->input('category_id'));
        }

        if (!$products_bld->exists()) {
            $err = new ApiError(341, null, 'Товары не найдены', 'Товары не найдены');
            return $err->json();
        }

        $products = $products_bld->cursor();

        $products_arr = [];

        if ($request->has('auth_token')) {
            $user = $this->getCurrentUserModel($request->input('auth_token'));
            if (!$user) {
                $err = new ApiError(309);
                return $err->json();
            }
            $unformed_order = Order::where(['status' => 0, 'user_id' => $user->id])->with('products.relatedServices')->first();
            if ($unformed_order) {
                $selected_products = $unformed_order->products->pluck('id')->toArray();
            }
            //	$myproducts = $selected_products->products->toArray();

        }
        foreach ($products as $product) {
            $products_in_arr = [];

            //Получаем изображения
            $img_arr = [];

            $imgs = $product->images;

            if (isset($imgs)) {

                foreach ($imgs as $img) {

                    $img_in_arr = [];

                    $img_in_arr['img_id'] = $img->id;

                    $img_in_arr['img_href'] = $img->href;

                    array_push($img_arr, $img_in_arr);

                }

                $products_in_arr['images'] = $img_arr;

            }

            $products_in_arr['name'] = $product->name;

            $products_in_arr['id'] = $product->id;

            $products_in_arr['description'] = $product->description;

            $products_in_arr['category_id'] = $product->category_id;

            $products_in_arr['average_price'] = $product->average_price;

            $products_in_arr['quantity_in_unformed_order'] = 0;

            $related_services_count = 0;
            $related_products_count = 0;
            $common_price = 0;
            if (!isset($user)) {
                $products_in_arr['related_services'] = $product->relatedServices;
            } else {
                $related_products = [];
                $related_services = [];
                $related = collect();
                if (isset($unformed_order) && $unformed_order->products->count() > 0) {

                    /* Если товар в корзине */
                    $orderProduct = $unformed_order->items()->where('product_id', $product->id)->first();

                    if ($orderProduct) {
                        $products_in_arr['quantity_in_unformed_order'] = $orderProduct->quantity;
                        /* Если выбрали - смотрим, че там по услугам */
                        $related = $orderProduct->relatedServices;
                    }
                }
                foreach ($product->relatedServices as $service) {

                    $selected = 0;

                    if (in_array($service->id, $related->pluck('id')->toArray())) {
                        /* Если к товару есть выбранные услуги */
                        $selected = 1;
                        $common_price = $common_price + $service->average_price;
                        if ($service->type == "service") {
                            $related_services_count++;
                        } else {
                            $related_products_count++;
                        }
                    }

                    $variants = [];
                    if (count($service->variants) > 0) {
                        foreach ($service->variants as $key => $var) {
                            $variants[$key] = [
                                'variant_id' => $key,
                                'image' => $service->image,
                                'name' => $var->name . " " . $var->value,
                                'average_price' => $service->average_price,
                                'selected' => 0,
                                'parent_id' => $service->id
                            ];
                        }
                    }

                    if ($service->type == "product") {
                        $related_products[] = [
                            'id' => $service->id,
                            'name' => $service->name,
                            'description' => $service->description,
                            'selected' => $selected,
                            'average_price' => $service->average_price,
                            'image' => $service->image,
                            'notes' => $service->notes,
                            'is_variants' => count($variants) > 0 ? 1 : 0,
                            'variants' => $variants,
                        ];
                    } else {
                        $related_services[] = [
                            'id' => $service->id,
                            'name' => $service->name,
                            'description' => $service->description,
                            'selected' => $selected,
                            'average_price' => $service->average_price,
                            'image' => $service->image,
                            'notes' => $service->notes,
                            'is_variants' => count($variants) > 0 ? 1 : 0,
                            'variants' => $variants,
                        ];
                    }
                }

                $products_in_arr['related_services'] = $related_services;
                $products_in_arr['related_products'] = $related_products;
                // = array_key_exists( $product->id, $selected_products ) ? $selected_products[ $product->id ]['quantity'] : 0;
                if (isset($selected_products) && in_array($product->id, $selected_products)) {
                    $products_in_arr['selected'] = 1;

                } else {
                    $products_in_arr['selected'] = 0;
                }

            }

            $products_in_arr['related_price'] = $common_price;
            $products_in_arr['related_services_count'] = $related_services_count;
            $products_in_arr['related_products_count'] = $related_products_count;
            array_push($products_arr, $products_in_arr);

        }
        return response()->json([

            'response' => $products_arr,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function editProduct(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer|exists:products,id',

            'name' => 'required_without_all:description,category_id,average_price|
            unique:products,name',

            'description' => 'required_without_all:name,category_id,average_price|max:250',

            'category_id' => 'required_without_all:description,name,average_price|
            integer|exists:products_category,id',

            'average_price' => 'required_without_all:description,category_id,name|numeric',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $product = Product::find($request->input('product_id'));

        if ($request->has('name')) {
            $product->name = $request->input('name');
        }

        if ($request->has('description')) {
            $product->description = $request->input('description');
        }

        if ($request->has('category_id')) {
            $product->category_id = $request->input('category_id');
        }

        if ($request->has('average_price')) {
            $product->average_price = $request->input('average_price');
        }

        try {
            $product->save();
        } catch (QueryException $ex) {
            $err = new ApiError(310);
            return $err->json();
        }

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function deleteProduct(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer|exists:products,id',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $product = Product::find($request->input('product_id'));

        $product->delete();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function getProduct(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer|exists:products,id',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $product = Product::find($request->input('product_id'));

        $resp = [
            'id' => $product->id,
            'name' => $product->name,
            'description' => $product->description,
            'category_id' => $product->category_id,
            'average_price' => $product->average_price,
        ];

        //Получаем изображения
        $img_arr = [];

        $imgs = $product->images;

        if (isset($imgs)) {

            foreach ($imgs as $img) {

                $img_in_arr = [];

                $img_in_arr['img_id'] = $img->id;

                $img_in_arr['img_href'] = $img->href;

                array_push($img_arr, $img_in_arr);

            }

            $resp['images'] = $img_arr;

        }

        return response()->json([

            'response' => $resp,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Выводим товары партнера, отмечаем те, что у него есть
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductsToPartner(Request $request)
    {
        $products_bld = Product::with(['relatedServices'])->orderBy('order', 'asc');

        if ($request->exists('category_id') && $request->filled('category_id')) {

            $products_bld = $products_bld->where('category_id', $request->input('category_id'));

        }

        if (!$products_bld->exists()) {
            $err = new ApiError(341, null, 'Товары не найдены', 'Товары не найдены');
            return $err->json();
        }

        $products = $products_bld->cursor();

        $user = $this->getCurrentUserModel($request->input('auth_token'));
        if (!$user) {
            $err = new ApiError(309);
            return $err->json();
        }
        $products_arr = [];
        foreach ($products as $product) {
            $products_in_arr = [];

            //Получаем изображения
            $img_arr = [];

            $imgs = $product->images;

            if (isset($imgs)) {

                foreach ($imgs as $img) {

                    $img_in_arr = [];

                    $img_in_arr['img_id'] = $img->id;

                    $img_in_arr['img_href'] = $img->href;

                    array_push($img_arr, $img_in_arr);

                }

                $products_in_arr['images'] = $img_arr;

            }

            $products_in_arr['name'] = $product->name;

            $products_in_arr['id'] = $product->id;

            $products_in_arr['description'] = $product->description;

            $products_in_arr['category_id'] = $product->category_id;

            $products_in_arr['average_price'] = $product->average_price;

            $products_in_arr['quantity_in_unformed_order'] = 0;

            $count = 0;

            $products_in_arr['related_services'] = $product->relatedServices;

            //$products_in_arr['quantity_in_unformed_order'] = array_key_exists($product->id, $selected_products) ? $selected_products[$product->id]['quantity'] : 0;

            if ($user->products->find($product->id)) {
                $products_in_arr['selected'] = 1;
            } else {
                $products_in_arr['selected'] = 0;
            }


            $products_in_arr['related_services_count'] = $count;
            array_push($products_arr, $products_in_arr);

        }
        return response()->json([

            'response' => $products_arr,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }
}
