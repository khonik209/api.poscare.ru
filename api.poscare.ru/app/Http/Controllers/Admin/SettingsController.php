<?php

namespace App\Http\Controllers\Admin;

use App\RatingEvent;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$settings = Setting::first();
		$ratings  = RatingEvent::all();
		return view( 'settings.settings', compact( 'settings', 'ratings' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

		$settings = Setting::find( 1 );

		$botSettings = [];
		if ( ! $settings ) {
			$settings = new Setting();
		} else {
			$botSettings = $settings->bot_settings;
		}
		$botSettings['bid_answer']   = $request->bid_answer;
		$botSettings['order_answer'] = $request->order_answer;
		$botSettings['bot_timeout']  = explode( ':', $request->bot_timeout )[0] * 60 + explode( ':', $request->bot_timeout )[1];
		$settings->bot_settings      = $botSettings;

		$settings->save();

		$ratings = $request->ratings;
		foreach ( $ratings as $key => $value ) {
			$rating = RatingEvent::find( $key );
			if ( $rating ) {
				$rating->score = $value;
				$rating->save();
			}
		}

		return back();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {


		$settings = Setting::first();
		if ( $request->settings == 'bot' ) {
			$botSettings                 = [];
			$botSettings['bid_answer']   = $request->bid_answer;
			$botSettings['order_answer'] = $request->order_answer;
			$botSettings['bot_timeout']  = explode( ':', $request->bot_timeout )[0] * 60 + explode( ':', $request->bot_timeout )[1];
			$settings->bot_settings      = $botSettings;
		}
		$settings->save();

		return back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
