<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Reward;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RewardController extends Controller {
	public function getRewards() {
		return Reward::orderBy( 'price', 'asc' )->get();
	}

	public function createRewardPage() {
		return view( 'rewards.newReward' );
	}

	public function createReward( Request $request ) {

		$reward        = new Reward();
		$reward->text  = $request->text;
		$reward->price = $request->price;
		// IMG
		if ( $request->file( 'image' ) ) {
			/* Если обновляем логотип */
			$file            = $request->file( 'image' );
			$validator_image = Validator::make( $request->all(), [
				'image' => 'image|mimes:jpg,jpeg,png',
			] );
			if ( $validator_image->fails() ) {
				return back();
			}

			$path          = $file->storePublicly( 'public' );
			$reward->image = Storage::url( $path );

		}
		$reward->save();

		return redirect( '/lists#/rewards' );
	}

	public function editRewardPage( $id ) {
		$reward = Reward::findOrFail( $id );
		return view( 'rewards.editReward', compact( 'reward' ) );
	}

	public function editReward( Request $request ) {
		$reward        = Reward::findOrFail( $request->reward_id );
		$reward->text  = $request->text;
		$reward->price = $request->price;
		// IMG
		if ( $request->file( 'image' ) ) {
			/* Если обновляем логотип */
			$file            = $request->file( 'image' );
			$validator_image = Validator::make( $request->all(), [
				'image' => 'image|mimes:jpg,jpeg,png',
			] );
			if ( $validator_image->fails() ) {
				return back();
			}

			$oldImage = $reward->image;
			if ( $oldImage ) {
				$result = Storage::delete( '/public/' . explode( '/', $oldImage )[2] );
			}
			$path          = $file->storePublicly( 'public' );
			$reward->image = Storage::url( $path );

		}
		$reward->save();

		return redirect( '/lists#/rewards' );
	}

	public function deleteReward( Request $request ) {
		$reward = Reward::findOrFail( $request->reward_id );
		$reward->delete();

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function force( Request $request ) {
		$user         = User::findOrFail( $request->id );
		$reward_ids   = $request->force_rewards;
		$user_rewards = $user->rewards()->wherePivot( 'force', '=', 1 )->get()->pluck( 'id' );
		$user->rewards()->detach( $user_rewards );
		if ( ! in_array( 0, $reward_ids ) ) {
			$user->rewards()->attach( $reward_ids, [ 'force' => 1 ] );
		}
		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}
}
