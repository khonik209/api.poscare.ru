<?php

namespace App\Http\Controllers\Admin;

use App\Classes\ApiError;
use App\Http\Controllers\Controller;
use App\Jobs\OrderResponse;
use App\Models\Bid;
use App\Models\BidResponse;
use App\Models\BidReview;
use App\Models\Branch;
use App\Models\Character;
use App\Models\City;
use App\Models\Delivery;
use App\Models\Image;
use App\Models\News;
use App\Models\Order;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\RelatedService;
use App\Models\Review;
use App\Models\Sale;
use App\Models\Service;
use App\Models\User;
use App\Models\UserOpt;
use App\Setting;
use App\Traits\AlarmTrait;
use App\Traits\GeoTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{

    use AlarmTrait;
    use GeoTrait;

    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Возвращаем всех пользователей
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function userList(Request $request)
    {

        if ($request->role == 'ex') {
            $users = User::where('role', '=', 'user');
        } else {
            $users = User::where('role', '=', 'admin')->orWhere('role', '=', 'manager');
        }

        if ($request->type) {
            $type = $request->type;
            if ($type == 'buyers') {
                $users = $users->where('user_type', 1);
            } elseif ($type == 'partners') {
                $users = $users->where('user_type', 2);
            }
        }

        if ($request->searchQuery && $request->searchQuery != '') {
            $query = $request->searchQuery;
            $users = $users->where('name', 'like', "%$query%")->orWhere('surname', 'like', "%$query%")->orWhere('email', 'like', "%$query%")->orWhereHas('phoneSalt', function ($q) use ($query) {
                $q->where('phone', 'like', "%$query%");
            });
        }

        return $users->with('city', 'branches', 'phoneSalt')->simplePaginate(25);
    }

    /**
     * Сохраняем пользователя
     *
     * @param Request $request
     *
     * @return int
     */
    public function saveUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'surname' => 'required'
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $user = User::findOrFail($request->id);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->user_type = $request->user_type;
        $user->orgform = $request->orgform;
        $user->role = $request->role;
        $user->city_id = $request->city_id;
        $user->rating = $request->rating;
        $user->save();
        $user->myRating('none', $request->rating);
        if ($request->phone_salt) {
            $phoneSalt = $user->phoneSalt;
            $phoneSalt->phone = $request->phone_salt['phone'];
            $phoneSalt->save();
        }
        if ($request->user_opt && $user->orgform == 1) {

            $resp = $this->getApiAddressCoords($request->user_opt['address']);

            if ($resp instanceof ApiError) {
                return $resp->json();
            }

            $userOpt = $user->userOpt;
            $userOpt->short_name = $request->user_opt['short_name'];
            $userOpt->full_name = $request->user_opt['full_name'];
            $userOpt->inn = $request->user_opt['inn'];
            $userOpt->description = $request->user_opt['description'];
            $userOpt->address = $request->user_opt['address'];
            $userOpt->address_lat = $resp['lat'];
            $userOpt->address_lon = $resp['lon'];
            $userOpt->open_hours_from = $request->user_opt['open_hours_from'];
            $userOpt->open_hours_to = $request->user_opt['open_hours_to'];
            $userOpt->site = $request->user_opt['site'];
            $userOpt->save();
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function deleteUser(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        if ($user->bids->count() > 0) {
            foreach ($user->bids as $bid) {
                $bid->services()->detach();
                $bid->delete();
            }
        }
        if ($user->orders->count() > 0) {
            foreach ($user->orders as $order) {
                $order->products()->detach();
                $order->delete;
            }
        }

        if ($user->reviews->count() > 0) {
            foreach ($user->reviews as $review) {
                $review->delete();
            }
        }

        if ($user->branches->count() > 0) {
            foreach ($user->branches as $branch) {
                foreach ($branch->reviews as $b_r) {
                    $bidsReview = BidReview::where('review_id', $b_r->id)->get();
                    foreach ($bidsReview as $value) {
                        $value->delete();
                    }
                    $b_r->delete();
                }
                $branch->images()->detach();
                $branch->delete();
            }
        }
        if ($user->images->count() > 0) {
            foreach ($user->images as $image) {
                $image->delete();
            }
        }
        if ($user->userOpt) {
            $opt = $user->userOpt;
            $opt->delete();
        }
        $user->delete();
        return redirect('/user');
    }

    public function userOptCreate(Request $request)
    {

        $user = User::findOrFail($request->user_id);
        if ($user->orgform != 2) {
            $err = new ApiError(0, null, 'Создание невозможно', null);

            return $err->json();
        }
        $resp = $this->getApiAddressCoords($request->address);

        if ($resp instanceof ApiError) {
            return $resp->json();
        }

        $userOpt = new UserOpt;
        if ($request->file('logo')) {
            /* Если обновляем логотип */

            $file = $request->file('logo');

            $validator_image = Validator::make($request->all(), [
                'logo' => 'image|mimes:jpg,jpeg,png',
            ]);

            if ($validator_image->fails()) {

                $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                return $err->json();

            }

            $path = $file->storePublicly('public');
            // #Шаг 2. Сохраняем новый файл.
            $logo = new Image;

            $logo->href = Storage::url($path);
            $logo->user_id = $user->id;
            $logo->is_accepted = 1;
            $logo->save();

            // Шаг 3. Обновляем id файла.
            $userOpt->image_id = $logo->id;

        }
        $userOpt->user_id = $user->id;
        $userOpt->short_name = $request->short_name;
        $userOpt->full_name = $request->full_name;
        $userOpt->inn = $request->inn;
        $userOpt->description = $request->description;
        $userOpt->address = $request->address;
        $userOpt->address_lat = $resp['lat'];
        $userOpt->address_lon = $resp['lon'];
        $userOpt->open_hours_from = $request->open_hours_from;
        $userOpt->open_hours_to = $request->open_hours_to;
        $userOpt->site = $request->site;
        $userOpt->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function userOptEdit(Request $request)
    {
        $resp = $this->getApiAddressCoords($request->address);

        if ($resp instanceof ApiError) {
            return $resp->json();
        }

        $userOpt = UserOpt::findOrFail($request->id);

        if ($request->file('logo')) {
            /* Если обновляем логотип */

            $file = $request->file('logo');

            $validator_image = Validator::make($request->all(), [
                'logo' => 'image|mimes:jpg,jpeg,png',
            ]);

            if ($validator_image->fails()) {

                $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                return $err->json();

            }

            $path = $file->storePublicly('public');
            // #Шаг 1. Удаляем старый файл.
            $oldLogo = $userOpt->logo;
            if ($oldLogo) {
                Storage::delete($oldLogo->href);
            }
            // #Шаг 2. Сохраняем новый файл.
            $logo = new Image;

            $logo->href = Storage::url($path);
            $logo->user_id = $userOpt->user->id;
            $logo->is_accepted = 1;
            $logo->save();

            // Шаг 3. Обновляем id файла.
            $userOpt->image_id = $logo->id;

        }

        $userOpt->short_name = $request->short_name;
        $userOpt->full_name = $request->full_name;
        $userOpt->inn = $request->inn;
        $userOpt->description = $request->description;
        $userOpt->address = $request->address;
        $userOpt->address_lat = $resp['lat'];
        $userOpt->address_lon = $resp['lon'];
        $userOpt->open_hours_from = $request->open_hours_from;
        $userOpt->open_hours_to = $request->open_hours_to;
        $userOpt->site = $request->site;
        $userOpt->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Удалить филиал
     *
     * @param $branch_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userBranchDelete($branch_id)
    {
        $branch = Branch::findOrFail($branch_id);

        $orderResponses = $branch->orderResponses;
        foreach ($orderResponses as $orderResponse) {
            $orderResponse->delete();
        }

        $bidResponses = $branch->bidResponses;
        foreach ($bidResponses as $bidResponse) {
            $bidResponse->delete();
        }

        if ($branch->reviews) {
            foreach ($branch->reviews as $review) {
                //    $review->bids()->detach();
                $bidsReview = BidReview::where('review_id', $review->id)->get();
                foreach ($bidsReview as $value) {
                    $value->delete();
                }
                $review->delete();
            }
        }
        $branch->images()->detach();

        $branch->delete();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function branchEdit(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'address' => 'required|string',
            //'open_hours_from' => 'required|time',
            //'open_hours_to' => 'required|time',
            'email' => 'required|email',
            //'phone' => 'required|phone',
            'description' => 'string',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $branch = Branch::findOrFail($request->id);
        $branch->name = $request->name;
        $branch->address = $request->address;
        $branch->open_hours_from = $request->open_hours_from;
        $branch->open_hours_to = $request->open_hours_to;
        $branch->email = $request->email;
        $branch->phone = $request->phone;
        $branch->description = $request->description;
        $branch->city_id = $request->city_id;
        $branch->save();

        foreach ($request->services as $service) {

            $branch->services()->updateExistingPivot($service['id'], [
                'quantity' => $service['pivot']['quantity'],
                'quantity_type' => $service['pivot']['quantity_type'],
                'price' => $service['pivot']['price']
            ]);
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function branchImageAdd(Request $request)
    {
        $branch = Branch::findOrFail($request->branch_id);
        if ($branch->images->count() >= 5) {
            $err = new ApiError(341, null, "Можно загрузить не более 5 изображений", "Можно загрузить не более 5 изображений");
            return $err->json();
        }
        if ($request->file('image')) {
            /* Если обновляем логотип */

            $file = $request->file('image');

            $validator_image = Validator::make($request->all(), [
                'logo' => 'image|mimes:jpg,jpeg,png',
            ]);

            if ($validator_image->fails()) {

                $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                return $err->json();

            }

            $path = $file->storePublicly('public');

            $image = new Image;

            $image->href = Storage::url($path);
            $image->user_id = $branch->user->id;
            $image->is_accepted = 1;
            $image->save();

            $branch->images()->attach($image->id);
            return response()->json([

                'response' => [
                    'result' => 1,
                    'image' => $image
                ],

            ], 200, [], JSON_UNESCAPED_UNICODE);
        }
        return response()->json([

            'response' => 0,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function branchImageDelete(Request $request)
    {
        $image = Image::findOrFail($request->id);
        Storage::delete($image->href);
        $image->branches()->detach();
        //$image->delete();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function branchCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'address' => 'required|string',
            //'open_hours_from' => 'required|time',
            //'open_hours_to' => 'required|time',
            'email' => 'required|email',
            //'phone' => 'required|phone',
            'description' => 'required|string',
        ]);

        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $resp = $this->getApiAddressCoords($request->address);

        if ($resp instanceof ApiError) {
            return $resp->json();
        }

        $branch = new Branch;
        $branch->type = 1; //todo РАЗОБРАТЬСЯ С ТИПОМ
        $branch->user_id = $request->user_id;
        $branch->name = $request->name;
        $branch->address = $request->address;
        $branch->open_hours_from = $request->open_hours_from;
        $branch->open_hours_to = $request->open_hours_to;
        $branch->email = $request->email;
        $branch->phone = $request->phone;
        $branch->description = $request->description;
        $branch->city_id = $request->city_id;
        $branch->address_lat = $resp['lat'];
        $branch->address_lon = $resp['lon'];
        $branch->save();
        return response()->json([

            'response' => [
                'result' => 1,
                'branch_id' => $branch->id
            ],

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function userServiceVerify(Request $request)
    {

        $user = User::find($request->user['id']);
        $currentPivot = $user->services()->find($request->service['id'])->pivot->verified;
        if ($currentPivot) {
            $user->services()->updateExistingPivot($request->service['id'], ['verified' => 0]);
        } else {
            $user->services()->updateExistingPivot($request->service['id'], ['verified' => 1]);
            $user->myRating('service_verified');
        }
        return response()->json([

            'response' => !$currentPivot

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Возвращаем все категории товаров
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCategories()
    {
        return ProductCategory::orderBy('order', 'asc')->get();
    }

    public function orderCategory(Request $request)
    {
        $categories = $request->categories;
        foreach ($categories as $category) {

            $updCategory = ProductCategory::find($category['id']);
            if ($updCategory) {
                $updCategory->order = $category['order'];
                $updCategory->save();
            }
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }


    /**
     * Создать новую категорию
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCategory(Request $request)
    {
        $category = new ProductCategory;
        $category->name = $request->name;
        $category->save();
        return response()->json([

            'response' => [
                'code' => 1,
                'category' => $category
            ],

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Редактируем категории
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editCategory(Request $request)
    {
        $category = ProductCategory::findOrFail($request->id);
        $category->name = $request->name;
        $category->order = $request->order;
        $category->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Удаление категории
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCategory(Request $request)
    {
        $category = ProductCategory::findOrFail($request->id);
        $category->delete();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Возвращает все товары
     *
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getProducts(Request $request)
    {
        return Product::with('category', 'characters', 'relatedServices', 'images')->orderBy('order', 'asc')->simplePaginate(10);
    }

    public function importProducts(Request $request)
    {
        //   dd($request->all());
        if (!$request->hasFile('file')) {
            print "file not found";
        }

        $file = $request->file('file');
        $handle = fopen($file, "r");
        $header = true;

        $i = 0;
        $productsInFile = [];
        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            //dd($csvLine);
            $i++;
            if ($i == 1) {
                continue;
            }

            // dd($csvLine);
            $id = isset($csvLine[0]) ? $csvLine[0] : 0;
            if ($id) {
                array_push($productsInFile, $csvLine[0]); // remmember that product in list
            }
            $product = Product::find($id);
            if (!$product) {
                $product = new Product;
            }
            try {
                $product->category_id = $csvLine[1];
                $product->name = $csvLine[2];
                $product->average_price = $csvLine[3];
                $product->description = $csvLine[4];
                $product->is_actual = isset($csvLine[5]) ? $csvLine[5] : 0;
                $product->save();
            } catch (\Exception $e) {
                print "Ошибка разделителя строки (значение не найдено): \n";
                dd($csvLine);
            }
        }
        $prToDelete = Product::whereNotIn('id',$productsInFile)->delete();
        return back();
    }

    public function exportProducts()
    {
        $headers = [
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0'
            , 'Content-type' => 'text/csv'
            , 'Content-Disposition' => 'attachment; filename=pos_products.csv'
            , 'Expires' => '0'
            , 'Pragma' => 'public'
        ];

        $list = Product::orderBy('id', 'asc')->get(['id', 'category_id', 'name', 'average_price', 'description', 'is_actual'])->toArray();

        # add headers for each column in the CSV download
        array_unshift($list, array_keys($list[0]));

        $callback = function () use ($list) {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row, ";");
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers); //use Illuminate\Support\Facades\Response;
    }

    public function orderProducts(Request $request)
    {
        $products = $request->products;
        foreach ($products as $product) {

            $updProduct = Product::find($product['id']);
            if ($updProduct) {
                $updProduct->order = $product['order'];
                $updProduct->save();
            }
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Страница создания нового товара
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function newProductPage()
    {
        $categories = ProductCategory::all();
        return view('newProduct', compact('categories'));
    }

    /**
     * Сохранение нового товара
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function newProduct(Request $request)
    {
        $product = new Product;
        $product->category_id = $request->category;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->average_price = $request->average_price;
        if ($request->is_actual) {
            $product->is_actual = 1;
        }
        $product->save();
        if (count($request->images) > 0) {
            $ids = [];
            $i = 0;
            foreach ($request->images as $file) {

                if ($i >= 5) {
                    break;
                }
                $i++;
                $validator_image = Validator::make($request->all(), [
                    'logo' => 'image|mimes:jpg,jpeg,png',
                ]);

                if ($validator_image->fails()) {

                    $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                    return $err->json();

                }

                $path = $file->storePublicly('public');

                $image = new Image;

                $image->href = Storage::url($path);
                //$image->owner_id = $branch->user->id;
                $image->is_accepted = 1;
                $image->save();
                $ids[] = $image->id;
            }
            $product->images()->attach($ids);
        }
        return redirect('/lists#/products');
    }

    /**
     * Страница редактирования товара
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editProductPage($id)
    {
        $product = Product::findOrFail($id);
        $categories = ProductCategory::all();
        $relatedServices = RelatedService::all();

        $products = Product::where('id', '!=', $id)->where(function ($q) use ($relatedServices) {
            $ids = $relatedServices->pluck('product_id')->unique()->filter()->values();
            $q->whereNotIn('id', $ids);
        })->get();
        $characters = Character::all();
        //dd($characters);
        return view('editProduct', compact('product', 'categories', 'relatedServices', 'characters', 'products'));
    }

    /**
     * Сохранить товар
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editProduct(Request $request)
    {

        $product = Product::findOrFail($request->id);
        $product->category_id = $request->category;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->average_price = $request->average_price;
        if ($request->is_actual) {
            $product->is_actual = 1;
        } else {
            $product->is_actual = 0;
        }
        $product->save();

        $toSync = [];

        if (count($request->characters) > 0) {
            foreach ($request->characters as $key => $char_id) {
                $toSync[$char_id] = ['quantity' => $request->values[$key]];
            }
            $product->characters()->sync($toSync);
        }

        $related_ids = $request->services;
        if ($request->related_products && count($request->related_products) > 0) {
            foreach ($request->related_products as $related_product_id) {
                $checkProduct = Product::find($related_product_id);
                if ($checkProduct) {
                    $new_related_product = new RelatedService();
                    $new_related_product->type = "product";
                    $new_related_product->name = $checkProduct->name;
                    $new_related_product->description = $checkProduct->description;
                    $new_related_product->average_price = $checkProduct->average_price;
                    $new_related_product->product_id = $checkProduct->id;
                    $checkImage = $checkProduct->images->first();
                    if ($checkImage) {
                        $new_related_product->image = $checkImage->href;
                    }

                    $new_related_product->save();
                    $related_ids[] = $new_related_product->id;
                }
            }
        }

        $product->relatedServices()->sync($related_ids);

        if (count($request->to_delete_images) > 0) {
            $product->images()->detach($request->to_delete_images);
        }
        if (count($request->images) > 0) {
            $ids = [];
            $i = 0;
            foreach ($request->images as $file) {

                if ($product->images->count() + $i >= 5) {
                    break;
                }
                $i++;
                $validator_image = Validator::make($request->all(), [
                    'logo' => 'image|mimes:jpg,jpeg,png',
                ]);

                if ($validator_image->fails()) {

                    $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                    return $err->json();

                }

                $path = $file->storePublicly('public');

                $image = new Image;

                $image->href = Storage::url($path);
                //$image->owner_id = $branch->user->id;
                $image->is_accepted = 1;
                $image->save();
                $ids[] = $image->id;
            }
            $product->images()->attach($ids);
        }

        $checkRelatedProduct = RelatedService::where('product_id', $product->id)->first();
        if ($checkRelatedProduct) {
            $checkRelatedProduct->name = $product->name;
            $checkRelatedProduct->description = $product->description;
            $checkRelatedProduct->average_price = $product->average_price;

            $checkImage = $product->images->first();
            if ($checkImage) {
                $checkRelatedProduct->image = $checkImage->href;

            }
            $checkRelatedProduct->save();
        }
        return back();
    }

    /**
     * Удаление товара
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProduct(Request $request)
    {
        $product = Product::findOrFail($request->id);

        if ($product->bids->count() > 0 || $product->orders->count() > 0) {
            $err = new ApiError(999, null, 'Нельзя удалить товар. Он используется в заявках или заказах', null);

            return $err->json();
        }

        $product->delete();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Возвращает все услуги или сервисы
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function getServices(Request $request)
    {
        $services = Service::orderBy('order', 'asc')->get();
        return $services;
    }

    public function orderServices(Request $request)
    {
        $services = $request->services;
        foreach ($services as $service) {

            $updService = Service::find($service['id']);
            if ($updService) {
                $updService->order = $service['order'];
                $updService->save();
            }
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Страница создания услуги
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createServicePage()
    {
        return view('newService');
    }

    /**
     * Сохранить новую услугу
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createService(Request $request)
    {
        $service = new Service;
        $service->name = $request->name;
        $service->description = $request->description;
        $service->type = $request->type;
        $service->average_price = $request->average_price;
        $service->quantity = $request->quantity;
        $service->quantity_type = $request->quantity_type;
        $service->save();

        return redirect('/lists#/services/' . $service->type);
    }

    /**
     * Страница редактирования услуги
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editServicePage($id)
    {
        $service = Service::findOrFail($id);
        return view('editService', compact('service'));
    }

    /**
     * Сохранить услугу
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editService(Request $request)
    {
        $service = Service::findOrFail($request->id);
        $service->name = $request->name;
        $service->description = $request->description;
        $service->type = $request->type;
        $service->average_price = $request->average_price;
        $service->quantity = $request->quantity;
        $service->quantity_type = $request->quantity_type;
        $service->save();
        return redirect('/lists#/services/' . $service->type);
    }

    /**
     * Удаление услуги
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteService(Request $request)
    {
        $service = Service::findOrFail($request->id);
        if ($service->bids->count() > 0) {
            $err = new ApiError(999, null, 'Нельзя удалить сервис (услугу). Он используется в заявках или заказах', null);

            return $err->json();
        }
        $service->delete();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Возвращает все характеристики
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCharacters()
    {
        return Character::orderBy('order', 'asc')->get();
    }

    public function orderCharacters(Request $request)
    {
        $characters = $request->characters;
        foreach ($characters as $character) {

            $updCharacter = Character::find($character['id']);
            if ($updCharacter) {
                $updCharacter->order = $character['order'];
                $updCharacter->save();
            }
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Удаление характеристики
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCharacter(Request $request)
    {
        $character = Character::findOrFail($request->id);
        if ($character->products->count() > 0) {
            $err = new ApiError(999, null, 'Нельзя удалить характеристику. Она используется.', null);

            return $err->json();
        }
        $character->delete();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Редактировать характеристику
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editCharacter(Request $request)
    {
        $character = Character::findOrFail($request->id);
        $character->name = $request->name;
        $character->unit = $request->unit;
        $character->description = $request->description;
        $character->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Создание характеристики
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCharacter(Request $request)
    {
        $character = new Character;
        $character->name = $request->name;
        $character->description = $request->description;
        $character->unit = $request->unit;
        $character->save();
        return response()->json([

            'response' => [
                'code' => 1,
                'character' => $character
            ],

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Создание сопутствующей услуги
     *
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRelatedServices(Request $request)
    {
        $relatedServices = RelatedService::orderBy('order', 'asc')->get()->map(function ($v, $k) {
            if (!$v->variants) {
                $v->variants = [];
            }
            return $v;
        });
        return $relatedServices;
    }

    public function orderRelatedServices(Request $request)
    {
        $services = $request->services;
        foreach ($services as $service) {
            $updService = RelatedService::find($service['id']);
            if ($updService) {
                $updService->order = $service['order'];
                $updService->save();
            }
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Удаление сопутствующей услуги
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteRelatedService(Request $request)
    {
        $relatedService = RelatedService::findOrFail($request->id);
        $relatedService->delete();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function editRelatedService(Request $request)
    {
        $data = json_decode($request->service);

        $relatedService = RelatedService::findOrFail($data->id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $validator_image = Validator::make($request->all(), [
                'image' => 'image|mimes:jpg,jpeg,png',
            ]);

            if ($validator_image->fails()) {

                $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                return $err->json();

            }

            $path = $file->storePublicly('public');
        }
        if (isset($path)) {
            $relatedService->image = Storage::url($path);
        }
        $relatedService->name = $data->name;
        $relatedService->description = $data->description;
        $relatedService->type = $data->type;
        $relatedService->average_price = $data->average_price;
        $relatedService->notes = $data->notes;
        if ($data->variants) {
            $relatedService->variants = $data->variants;
        } else {
            $relatedService->variants = [];
        }
        $relatedService->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Создание сопутствующей услуги
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRelatedService(Request $request)
    {

        $data = json_decode($request->service);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $validator_image = Validator::make($request->all(), [
                'image' => 'image|mimes:jpg,jpeg,png',
            ]);

            if ($validator_image->fails()) {

                $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                return $err->json();

            }
            $path = $file->storePublicly('public');
        }

        $relatedService = new RelatedService;
        if (isset($path)) {
            print $path;
            $relatedService->image = Storage::url($path);
        }

        $relatedService->name = $data->name;
        $relatedService->description = $data->description;
        $relatedService->type = $data->type;
        $relatedService->average_price = $data->average_price;
        if (isset($data->variants)) {
            $relatedService->variants = $data->variants;
        }
        if (isset($data->notes)) {
            $relatedService->notes = $data->notes;
        }
        $relatedService->save();
        return response()->json([

            'response' => $relatedService,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Список способов доставки
     *
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getDeliveries(Request $request)
    {
        return Delivery::orderBy('order', 'asc')->get();
    }

    public function orderDeliveries(Request $request)
    {
        $deliveries = $request->deliveries;
        foreach ($deliveries as $delivery) {
            $updDelivery = Delivery::find($delivery['id']);
            if ($updDelivery) {
                $updDelivery->order = $delivery['order'];
                $updDelivery->save();
            }
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }


    /**
     * Удалить способ доставки
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteDelivery(Request $request)
    {
        $delivery = Delivery::findOrFail($request->id);
        $delivery->delete();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Сохранить способ доставки
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editDelivery(Request $request)
    {
        $delivery = Delivery::findOrFail($request->id);
        $delivery->name = $request->name;
        $delivery->required_address = $request->required_address;
        $delivery->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Создать способ доставки
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createDelivery(Request $request)
    {
        $delivery = new Delivery;
        $delivery->name = $request->name;
        $delivery->required_address = $request->address;
        $delivery->save();
        return response()->json([

            'response' => [
                'code' => 1,
                'delivery' => $delivery,
            ],

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Вернуть все способы платежи
     *
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getPayments(Request $request)
    {
        return PaymentMethod::orderBy('order', 'asc')->get();
    }

    public function orderPayments(Request $request)
    {
        $payments = $request->payments;
        foreach ($payments as $payment) {
            $updPayment = PaymentMethod::find($payment['id']);
            if ($updPayment) {
                $updPayment->order = $payment['order'];
                $updPayment->save();
            }
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Удалить способ платежей
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePayment(Request $request)
    {
        $payment = PaymentMethod::findOrFail($request->id);
        $payment->delete();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Сохранить способ оплаты
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editPayment(Request $request)
    {
        $payment = PaymentMethod::findOrFail($request->id);
        $payment->name = $request->name;
        $payment->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Создать способ оплаты
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPayment(Request $request)
    {
        $payment = new PaymentMethod();
        $payment->name = $request->name;
        $payment->save();
        return response()->json([

            'response' => [
                'code' => 1,
                'payment' => $payment,
            ],

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Странциа модерации
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getModeration()
    {
        return view('moderation');
    }

    /**
     * Список новых заявок
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function bidsToModerate(Request $request)
    {
        $bids = Bid::where('status', 0)->with('user', 'services', 'product', 'user.city')->get();
        return $bids;
    }

    /**
     * утвердить заявку
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptBid(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bid_id' => 'required|integer',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $bid = Bid::findOrFail($request->bid_id);
        if ($bid->status < 1) {
            $bid->status = 1;
            $bid->is_accepted = 1;
            $bid->last_refresh = Carbon::now();
            $bid->refresh_count = 1;
            $bid->save();

            $founder = $bid->user;
            $city_id = $founder->city_id;
            $receivers = Branch::where('city_id', $city_id)->get()->pluck('user_id')->unique();
            $this->createAlarm('bid', $bid, 'Была размещена новая заявка', 'Была размещена новая заявка', $receivers);
            $settings = Setting::first();
            if ($settings) {
                $botSettings = $settings->bot_settings;
            }
            $job = (new \App\Jobs\BidResponse($bid))->delay(Carbon::now()->addSeconds(isset($botSettings) ? $botSettings->bot_timeout : 60));

            dispatch($job);
        }

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * утвердить заявку
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function rejectBid(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bid_id' => 'required|integer',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $bid = Bid::findOrFail($request->bid_id);
        if ($bid->status < 1) {
            $bid->status = 5;
            $bid->is_accepted = 0;
            $bid->save();

            /* $founder = $bid->user;
			 $city_id = $founder->city_id;
			 $receivers = Branch::where('city_id', $city_id)->get()->pluck('user_id')->unique();
			 $this->createAlarm('bid', $bid, 'Была размещена новая заявка', 'Была размещена новая заявка', $receivers);*/
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Страница предложения
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function bidOfferPage($id)
    {
        $user = User::findOrFail(15);
        $bid = Bid::findOrFail($id);
        return view('bidOffer', compact('bid', 'user'));
    }

    /**
     * Сделать предложение
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bidOffer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bid_id' => 'required|integer|exists:bids,id',
            'branch_id' => 'required|integer|exists:branches,id',
            'price' => 'required|numeric',
            'quantity' => 'required|integer',
            'quantity_type' => 'required|integer|between:1,3',
            'comment' => 'required',
        ]);

        if ($validator->fails()) {

            return back()->with('error', 'Введены некорректный данные');

        }

        $users_br_id = $request->input('branch_id');

        $user = User::find(15);

        //Проверяем принадлежность филиала пользователю
        $brch_bld = Branch::where('id', $users_br_id)->where('user_id', $user->id);

        if (!$brch_bld->exists()) {

            return redirect('/moderation')->with('error', 'Филиал не найден');
        }

        $bid_id = $request->input('bid_id');

        $bid = Bid::findOrFail($bid_id);
        if ($bid->status < 1) {
            $bid->status = 1;
            $bid->is_accepted = 1;
            if ($request->is_private) {
                $bid->is_private = 1;
            } else {
                $founder = $bid->user;
                $city_id = $founder->city_id;
                $receivers = Branch::where('city_id', $city_id)->get()->pluck('user_id')->unique();
                $this->createAlarm('bid', $bid, 'Была размещена новая заявка', 'Была размещена новая заявка', $receivers);

            }
            $bid->save();
        }
        //Проверка ,отвечал ли уже текущий пользователь
        $user_branches = $user->branches;

        foreach ($user_branches as $user_branch) {

            $is_responsed = BidResponse::where('branch_id', $user_branch->id)->where('bid_id', $bid_id)->exists();

            if ($is_responsed) {
                return redirect('/moderation')->with('error', 'Вы уже отвечали на данную заявку');
            }

        }


        if (!$user->is_service) {

            return redirect('/moderation')->with('error', 'Нельзя оказывать услугу is_service = 0');
        }

        $bid_response = new BidResponse;
        $bid_response->branch_id = $users_br_id;
        $bid_response->price = $request->input('price');
        $bid_response->bid_id = $bid_id;
        $bid_response->quantity = $request->input('quantity');
        $bid_response->quantity_type = $request->input('quantity_type');
        $bid_response->comment = $request->input('comment');
        $bid_response->save();

        /* Раскидываем уведомления */
        $receivers = [$bid->user_id];
        $this->createAlarm('bid', $bid, 'Поступило предложение на заявку', 'Поступило предложение на заявку', $receivers);

        return redirect('/moderation')->with('success', 'Предложение сделано');
    }

    public function ordersToModerate(Request $request)
    {
        $orders = Order::where('status', '=', 1)->with('user', 'user.city', 'products', 'relatedServices', 'items', 'delivery')->get();

        return $orders;
    }

    /**
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|integer',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }
        $order = Order::findOrFail($request->order_id);
        if ($order->status < 2) {
            $order->status = 2;
            $order->is_accepted = 1;
            $order->last_refresh = Carbon::now();
            $order->refresh_count = 1;
            $order->save();

            $founder = $order->user;
            $city_id = $founder->city_id;
            $receivers = Branch::where('city_id', $city_id)->get()->pluck('user_id')->unique();
            $this->createAlarm('order', $order, 'Был размещен новый заказ', 'Был размещен новый заказ', $receivers);
            $job = (new OrderResponse($order))->delay(Carbon::now()->addSeconds(isset($botSettings) ? $botSettings->bot_timeout : 60));

            dispatch($job);
        }

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * утвердить заявку
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function rejectOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|integer',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299, null, null, $val_err->all());

            return $err->json();

        }

        $order = Order::findOrFail($request->order_id);
        if ($order->status < 2) {
            $order->status = 6;
            $order->is_accepted = 0;
            $order->save();

            /* $founder = $bid->user;
			 $city_id = $founder->city_id;
			 $receivers = Branch::where('city_id', $city_id)->get()->pluck('user_id')->unique();
			 $this->createAlarm('bid', $bid, 'Была размещена новая заявка', 'Была размещена новая заявка', $receivers);*/
        }
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Страница предложения
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function orderOfferPage($id)
    {
        $order = Order::findOrFail($id);
        $user = User::findOrFail(15);
        return view('orderOffer', compact('order', 'user'));
    }

    /**
     * Сделать предложение
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function orderOffer(Request $request)
    {
        $user = User::findOrFail(15);

        if ($user->user_type !== 2) {
            return back()->with('error', 'Некорректный пользователь. user_type=' . $user->user_type);
        }

        $validator = Validator::make($request->all(), [
            'order_id' => [
                'required',
                'integer',
                'exists:orders,id',
            ],
            'branch_id' => [
                'required',
                'integer',
                Rule::exists('branches', 'id')->where(function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                }),
            ],
            'quantity' => 'required|integer',
            'quantity_type' => 'required|integer|between:1,3',
            'comment' => 'required|max:255',
            'price' => 'required|numeric'
        ]);

        if ($validator->fails()) {

            return back()->with('error', 'Введены некорректный данные');

        }

        $order_id = $request->input('order_id');
        $usr_br_id = $request->input('branch_id');
        $quant = $request->input('quantity');
        $quant_type = $request->input('quantity_type');
        $comment = $request->input('comment');
        $price = $request->input('price');

        $order = Order::find($order_id);
        if ($order->status < 2) {
            $order->status = 2;
            $order->is_accepted = 1;
        }
        if ($request->is_private) {
            $order->is_private = 1;
        } else {
            $founder = $order->user;
            $city_id = $founder->city_id;
            $receivers = Branch::where('city_id', $city_id)->get()->pluck('user_id')->unique();
            $this->createAlarm('order', $order, 'Был размещен новый заказ', 'Был размещен новый заказ', $receivers);

        }
        $order->save();

        $resp = $order->addResponse($usr_br_id, $quant, $quant_type, $comment, $price);
        if ($resp instanceof ApiError) {
            return back()->with('error', 'Ошибка! ' . $resp->json());
        }

        unset($resp);

        $receivers = [$order->user_id];
        $this->createAlarm('order', $order, 'Поступило предложение на заказ', 'Поступило предложение на заказ', $receivers);

        return redirect('/moderation')->with('success', 'Предложение сделано');
    }

    /**
     * Список не-модерированных отзывов
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function reviewsToModerate(Request $request)
    {
        $reviews = Review::where('is_accepted', '=', 2)->with('user', 'user.city', 'branch')->get();

        return $reviews;
    }

    /**
     * Разрешить отзыв
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptReview(Request $request)
    {
        $review = Review::findOrFail($request->review_id);
        $review->is_accepted = 1;
        $review->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Запретить отзыв
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function rejectReview(Request $request)
    {
        $review = Review::findOrFail($request->review_id);
        $review->is_accepted = 0;
        $review->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Список немодерированных изображений
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function imagesToModerate(Request $request)
    {
        $images = Image::where('is_accepted', '=', 2)->orderBy('created_at', 'desc')->with('user', 'branches')->get();
        return $images;
    }

    /**
     * Разрешить изображение
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptImage(Request $request)
    {
        $image = Image::findOrFail($request->image_id);
        $image->is_accepted = 1;
        $image->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Отклонить изображение
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function rejectImage(Request $request)
    {
        $image = Image::findOrFail($request->image_id);
        $image->is_accepted = 0;
        $image->save();
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     *Страница всех заявок
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getBidsPage()
    {
        return view('bids');
    }

    /**
     * Вернуть список заявок
     *
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getBids(Request $request)
    {
        if ($request->searchQuery && $request->searchQuery != '') {
            $query = $request->searchQuery;
            $bids = Bid::where('description', 'like', "%$query%")->orWhereHas('user', function ($user) use ($query) {
                $user->where('name', 'like', "%$query%")->orWhere('surname', 'like', "%$query%");
            })->orWhereHas('product', function ($product) use ($query) {
                $product->where('name', 'like', "%$query%");
            })->orWhereHas('services', function ($service) use ($query) {
                $service->where('name', 'like', "%$query%");
            });
            return $bids->with('user', 'services', 'product')->get();
        } else {
            return Bid::with('user', 'services', 'product')->simplePaginate(10);
        }
    }

    /**
     * Страница одной заявки
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBid($id)
    {
        $bid = Bid::with('user', 'services', 'product')->findOrFail($id);
        $products = Product::all();
        $services = Service::all();
        return view('bid', compact('bid', 'products', 'services'));
    }

    /**
     * Сохранить заявку
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editBid(Request $request)
    {
        $bid = Bid::findOrFail($request->id);
        $bid->description = $request->description;
        $bid->terms = Carbon::parse($request->terms)->format('Y-m-d H:i:s');
        $bid->status = $request->status;
        $product = Product::find($request->product_id);
        if ($product) {
            $bid->product_id = $product->id;
        }
        $service = Service::find($request->service_id);
        if ($service) {
            $bid->services()->detach();
            $bid->services()->attach($service->id);
        }

        $bid->save();

        return redirect('/bids');
    }


    /**
     *Страница всех заявок
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getOrdersPage()
    {
        return view('orders');
    }

    /**
     * Вернуть список заявок
     *
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getOrders(Request $request)
    {
        if ($request->searchQuery && $request->searchQuery != '') {
            $query = $request->searchQuery;
            $orders = Order::whereIn('status', ['2', '3', '4', '5', '6'])->where(function ($q) use ($query) {
                $q->whereHas('user', function ($user) use ($query) {
                    $user->where('name', 'like', "%$query%")->orWhere('surname', 'like', "%$query%");
                })->orWhereHas('products', function ($product) use ($query) {
                    $product->where('name', 'like', "%$query%");
                })->orWhereHas('relatedServices', function ($service) use ($query) {
                    $service->where('name', 'like', "%$query%");
                });
            });
            return $orders->with('user', 'products', 'relatedServices', 'delivery')->simplePaginate(10);
        } else {
            return Order::with('user', 'products', 'relatedServices', 'delivery')->simplePaginate(10);
        }
    }

    /**
     * Страница одной заявки
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getOrder($id)
    {
        $order = Order::with('user', 'products', 'relatedServices', 'delivery')->findOrFail($id);
        $products = Product::all();
        $relatedServices = RelatedService::all();
        $deliveries = Delivery::all();

        return view('order', compact('order', 'products', 'relatedServices', 'deliveries'));
    }

    public function newOrder()
    {
        return view('orderNew');
    }

    public function editOrder(Request $request)
    {
        $order = Order::findOrFail($request->id);
        $order->terms = Carbon::parse($request->terms)->format('Y-m-d H:i:s');
        $order->delivery_id = $request->delivery_id;
        $order->status = $request->status;
        $order->save();

        $toSync = [];
        if (count($request->products) > 0) {
            foreach ($request->products as $key => $product_id) {
                $toSync[$product_id] = ['quantity' => $request->quantities[$key]];
            }
            $order->products()->sync($toSync);
        } else {
            $order->products()->detach();
        }
        return redirect('/orders');
    }

    public function getNewsPage()
    {
        $news_arr = News::with('images')->orderBy('created_at', 'desc')->get();
        return view('news', compact('news_arr'));
    }

    public function getNews($id)
    {
        $news = News::findOrFail($id);
        return view('editNews', compact('news'));
    }

    public function createNewsPage()
    {
        return view('createNews');
    }

    public function createNews(Request $request)
    {
        $news = new News;

        $news->name = $request->name;
        $news->description = $request->description;
        $news->save();


        if (count($request->images) > 0) {
            $ids = [];
            foreach ($request->images as $file) {

                $validator_image = Validator::make($request->all(), [
                    'logo' => 'image|mimes:jpg,jpeg,png',
                ]);

                if ($validator_image->fails()) {

                    $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                    return $err->json();

                }

                $path = $file->storePublicly('public');

                $image = new Image;

                $image->href = Storage::url($path);
                //$image->owner_id = $branch->user->id;
                $image->is_accepted = 1;
                $image->save();
                $ids[] = $image->id;
            }
            $news->images()->attach($ids);
        }
        /* Раскидываем уведомления */
        $receivers = User::get(['id']);
        // $this->createAlarm('news', $news, $news->name, $news->description, $receivers);

        return redirect('/news');
    }

    public function editNews(Request $request)
    {
        $news = News::findOrFail($request->news_id);

        $news->name = $request->name;
        $news->description = $request->description;
        $news->save();

        if (count($request->to_delete_images) > 0) {
            $news->images()->detach($request->to_delete_images);
        }
        if (count($request->images) > 0) {
            $ids = [];
            foreach ($request->images as $file) {

                $validator_image = Validator::make($request->all(), [
                    'logo' => 'image|mimes:jpg,jpeg,png',
                ]);

                if ($validator_image->fails()) {

                    $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                    return $err->json();

                }

                $path = $file->storePublicly('public');

                $image = new Image;

                $image->href = Storage::url($path);
                //$image->owner_id = $branch->user->id;
                $image->is_accepted = 1;
                $image->save();
                $ids[] = $image->id;
            }
            $news->images()->attach($ids);
        }
        return back();
    }

    public function deleteNews(Request $request)
    {
        $news = News::findOrFail($request->news_id);
        $image_ids = $news->images()->get()->pluck('id');
        $news->images()->detach();
        // todo удалять изображения, чтобы не засорять память.
        $news->delete();
        return back();
    }

    public function getSalesPage()
    {
        $sales = Sale::with('images')->orderBy('created_at', 'desc')->get();
        return view('sales', compact('sales'));
    }

    public function getSale($id)
    {
        $sale = Sale::findOrFail($id);
        return view('editSale', compact('sale'));
    }

    public function editSale(Request $request)
    {
        $sale = Sale::findOrFail($request->sale_id);
        $sale->name = $request->name;
        $sale->description = $request->description;
        $sale->start_date = Carbon::parse($request->start_date)->format('Y-m-d H:i:s');
        $sale->end_date = Carbon::parse($request->end_date)->format('Y-m-d H:i:s');
        $sale->user_type = $request->user_type ? $request->user_type : 1;
        $sale->save();

        if (count($request->to_delete_images) > 0) {
            $sale->images()->detach($request->to_delete_images);
        }
        if (count($request->images) > 0) {
            $ids = [];
            foreach ($request->images as $file) {

                $validator_image = Validator::make($request->all(), [
                    'logo' => 'image|mimes:jpg,jpeg,png',
                ]);

                if ($validator_image->fails()) {

                    $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                    return $err->json();

                }

                $path = $file->storePublicly('public');

                $image = new Image;

                $image->href = Storage::url($path);
                //$image->owner_id = $branch->user->id;
                $image->is_accepted = 1;
                $image->save();
                $ids[] = $image->id;
            }
            $sale->images()->attach($ids);
        }
        return back();
    }

    public function createSalePage()
    {
        return view('createSale');
    }

    public function createSale(Request $request)
    {
        /* Сохраняем акцию */
        $sale = new Sale;

        $sale->name = $request->name;
        $sale->start_date = Carbon::parse($request->start_date)->format('Y-m-d H:i:s');
        $sale->end_date = Carbon::parse($request->end_date)->format('Y-m-d H:i:s');
        $sale->description = $request->description;
        $sale->user_type = $request->user_type ? $request->user_type : 1;
        $sale->user_id = $request->user_id;
        $sale->save();

        try {
            $sale->cities()->attach($request->city_id);
        } catch (\Exception $e) {

        }

        /* Сохраняем изображения */
        if (count($request->images) > 0) {
            $ids = [];
            foreach ($request->images as $file) {

                $validator_image = Validator::make($request->all(), [
                    'logo' => 'image|mimes:jpg,jpeg,png',
                ]);

                if ($validator_image->fails()) {

                    $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                    return $err->json();

                }

                $path = $file->storePublicly('public');

                $image = new Image;

                $image->href = Storage::url($path);
                //$image->owner_id = $branch->user->id;
                $image->is_accepted = 1;
                $image->save();
                $ids[] = $image->id;
            }
            $sale->images()->attach($ids);
        }

        /* Раскидываем уведомления */
        $receivers = [$sale->user_id];
        // $this->createAlarm('sales', $sale, $sale->name, $sale->description, $receivers);

        return redirect('/sales');
    }

    public function deleteSale(Request $request)
    {
        $sale = Sale::findOrFail($request->sale_id);
        $sale->images()->detach();
        $sale->cities()->detach();
        $sale->delete();
        return back();
    }


    public function getCities(Request $request)
    {
        return City::where('name', 'like', "%$request->q%")->get();
    }

    public function getUsers(Request $request)
    {
        $query = $request->q;
        $users = User::where('name', 'like', "%$query%")->orWhere('surname', 'like', "%$query%")->orWhere('email', 'like', "%$query%")->orWhereHas('phoneSalt', function ($q) use ($query) {
            $q->where('phone', 'like', "%$query%");
        })->get();
        return $users;
    }
}
