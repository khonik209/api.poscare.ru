<?php

namespace App\Http\Controllers\Admin;

use App\Classes\ApiError;
use App\Models\Branch;
use App\Models\Service;
use App\Models\User;
use App\Models\UserOpt;
use App\Reward;
use App\Setting;
use App\Traits\GeoTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller {

	use GeoTrait;

	/**
	 * Главная страница админки
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
		return view( 'home' );
	}

	/**
	 * Страница всех пользователей
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getUsers() {
		return view( 'users' );
	}

	/**
	 * Страница пользователя
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getUser( $id ) {
		$user                = User::with( 'phoneSalt', 'branchesWithCity', 'userOpt', 'userOpt.logo', 'city', 'services', 'rewards' )->findOrFail( $id );
		$user_rewards_force  = $user->rewards()->wherePivot( 'force', 1 )->get();
		$user->force_rewards = $user_rewards_force;
		$rewards             = Reward::all();
		return view( 'user', compact( 'user', 'rewards' ) );
	}

	/**
	 * Страница создания пользователей
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function newUser() {
		return view( 'newUser' );
	}

	/**
	 * Создание нового пользователя
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function saveNewUser( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'email' => 'required|string|email|max:255|unique:users',
			'name' => 'required',
			'surname' => 'required'
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			return back()->with( 'errors', $val_err );

		}
		$user            = new User;
		$user->email     = $request->email;
		$user->password  = bcrypt( $request->password );
		$user->name      = $request->name;
		$user->surname   = $request->surname;
		$user->user_type = $request->user_type;
		$user->orgform   = $request->orgform;
		$user->role      = $request->role;
		$user->save();

		if ( $request->orgform == 2 ) {
			$resp = $this->getApiAddressCoords( $request->address );

			if ( $resp instanceof ApiError ) {
				return $resp->json();
			}

			$userOpt                  = new UserOpt;
			$userOpt->user_id         = $user->id;
			$userOpt->short_name      = $request->short_name;
			$userOpt->full_name       = $request->full_name;
			$userOpt->inn             = $request->inn;
			$userOpt->description     = $request->description;
			$userOpt->open_hours_from = $request->open_hours_from;
			$userOpt->open_hours_to   = $request->open_hours_to;
			$userOpt->site            = $request->site;
			$userOpt->address         = $request->address;
			$userOpt->address_lat     = $resp['lat'];
			$userOpt->address_lon     = $resp['lon'];
			$userOpt->save();

		}

		return redirect( 'user' );
	}

	/**
	 * Страница создание филиала
	 *
	 * @param $user_id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function userBranchCreate( $user_id ) {
		$user = User::findOrFail( $user_id );

		return view( 'newBranch', compact( 'user' ) );
	}


	/**
	 * Страница редактирования филиала
	 *
	 * @param $branch_id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function userBranchEdit( $branch_id ) {

		$branch = Branch::with( 'city', 'images', 'services' )->findOrFail( $branch_id );

		return view( 'editBranch', compact( 'branch' ) );
	}

	public function getcities() {
		return City::orderBy( 'name' )->get( [ 'id', 'name' ] );
	}
}
