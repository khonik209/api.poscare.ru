<?php

namespace App\Http\Controllers\Admin;

use App\Classes\ApiError;
use App\Models\Product;
use App\Models\Tender;
use App\Models\TenderResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TenderController extends Controller {
	public function list() {
		$tenders = Tender::orderBy( 'created_at', 'desc' )->get();

		return view( 'tenders.list', compact( 'tenders' ) );
	}

	public function create() {
		$products = Product::all();

		return view( 'tenders.create', compact( 'products' ) );
	}

	public function store( Request $request ) {
		$tender              = new Tender;
		$tender->terms       = $request->terms;
		$tender->status      = $request->status;
		$tender->is_accepted = $request->is_accepted;
		$tender->description = $request->description;
		$tender->is_private  = $request->is_private;
		$tender->user_id     = Auth::id();
		$tender->save();

		$products = $request->products;
		$tender->products()->detach();
		if ( $products ) {
			foreach ( $products as $key => $product ) {
				if ( ! $product['quantity'] ) {
					continue;
				}
				$tender->products()->attach( $key, [
					'quantity' => $product['quantity'],
					'price' => isset( $product['price'] ) ? $product['price'] : null
				] );
			}
		}


		return redirect( '/tenders/' . $tender->id );
	}

	public function show( $id ) {
		$tender = Tender::findOrFail( $id );

		return view( 'tenders.show', compact( 'tender' ) );
	}

	public function edit( $id ) {
		$tender = Tender::findOrFail( $id );

		$products = Product::all();

		$tenderProducts = $tender->products()->get();

		return view( 'tenders.edit', compact( 'tender', 'products', 'tenderProducts' ) );
	}

	public function update( Request $request ) {
		$tender              = Tender::findOrFail( $request->tender_id );
		$tender->terms       = $request->terms;
		$tender->status      = $request->status;
		$tender->is_accepted = $request->is_accepted;
		$tender->description = $request->description;
		$tender->is_private  = $request->is_private;
		$tender->save();

		$products = $request->products;
		$tender->products()->detach();
		if ( $products ) {
			foreach ( $products as $key => $product ) {
				if ( ! $product['quantity'] ) {
					continue;
				}
				$tender->products()->attach( $key, [
					'quantity' => $product['quantity'],
					'price' => isset( $product['price'] ) ? $product['price'] : null
				] );
			}
		}


		return redirect( '/tenders/' . $tender->id );
	}

	public function delete( Request $request ) {
		$tender = Tender::findOrFail( $request->tender_id );
		$tender->delete();

		return redirect( '/tenders/' );
	}

	public function deleteResponse( Request $request ) {

		$response = TenderResponse::findOrFail( $request->tender_response_id );
		$tender   = $response->tender;
		$response->delete();

		return redirect( '/tenders/' . $tender->id );
	}

	public function tendersToModerate( Request $request ) {
		$tenders = Tender::where( 'status', 0 )->with( 'user', 'products', 'user.city' )->get();
		return $tenders;
	}

	public function acceptTender( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'tender_id' => 'required|integer',
		] );
		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		$tender = Tender::findOrFail( $request->tender_id );
		if ( $tender->status < 1 ) {
			$tender->status      = 1;
			$tender->is_accepted = 1;
			$tender->save();
		}

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function rejectTender( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'tender_id' => 'required|integer',
		] );
		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		$tender = Tender::findOrFail( $request->tender_id );
		if ( $tender->status < 1 ) {
			$tender->status      = 1;
			$tender->is_accepted = 2;
			$tender->save();
		}

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}
}
