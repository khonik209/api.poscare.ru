<?php

namespace App\Http\Controllers\Tenders;

use App\Classes\ApiError;
use App\Models\Product;
use App\Models\Tender;
use App\Traits\AlarmTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TenderController extends Controller {
	use AlarmTrait;
	/**
	 * Список тендеров
	 *
	 * Статусы:
	 *
	 *  is_mine = 0
	 *  null - все, кроме модерации
	 *  0 - модерация
	 *  1 - (didIAnswer = 0) - Новый
	 *  1 - (didIAnswer = 1) - Участник
	 *  2 - Исполнитель
	 *  3 - Закрыт
	 *  4 - Архив
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function list( Request $request ) {
		$status   = $request->status;
		$answered = $request->didIAnswer;
		$is_mine  = $request->is_mine;
		$is_sell  = $request->is_sale;

		$user       = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$myBranches = $user->branches->pluck( 'id' );

		if ( $is_mine ) {
			$tenders = Tender::where( 'user_id', $user->id )->orderBy( 'created_at', 'desc' )->where( 'is_private', '=', 0 );
		} else {
			if ( $status === '0' ) {
				$status = null;
			}
			$tenders = Tender::where( 'user_id', '!=', $user->id )->where( 'is_sell', '=', $is_sell )->orderBy( 'created_at', 'desc' )->where( 'is_private', '=', 0 );

		}

		if ( $status === '0' ) {
			$tenders = $tenders->where( 'is_accepted', 0 );
		} elseif ( $status == 1 ) {
			$tenders = $tenders->where( 'is_accepted', 1 );
			if ( $answered ) {
				$tenders = $tenders->whereHas( 'tenderResponses', function ( $q ) use ( $myBranches ) {
					$q->whereIn( 'branch_id', $myBranches );
				} );
			} else {
				$tenders = $tenders->has( 'tenderResponses', '<', 1 )->orWhereHas( 'tenderResponses', function ( $q ) use ( $myBranches ) {
					$q->whereNotIn( 'branch_id', $myBranches );
				} );
			}
		} elseif ( $status == 2 ) {
			$tenders = $tenders->whereIn( 'branch_id', $myBranches )->where( 'is_accepted', 1 );
		} elseif ( ! isset( $status ) ) {
			if ( ! $is_mine ) {
				$tenders = $tenders->where( 'is_accepted', 1 );
			}
		} elseif ( $status == 4 ) {
			$tenders = $tenders->where( 'status', $status );
		} else {
			$tenders = $tenders->where( 'status', $status )->where( 'is_accepted', 1 );
		}
		$tenders = $tenders->with( 'products' )->get();
		$tenders = $tenders->map( function ( $v, $k ) use ( $user, $myBranches ) {
			$v->is_mine       = $v->user_id == $user->id;
			$v->product_count = $v->products->count();
			$commonBranches   = $v->tenderResponses->pluck( 'branch_id' )->intersect( $myBranches );
			$v->didIAnswer    = count( $commonBranches ) > 0 ? 1 : 0;
			$v->is_sale       = $v->is_sell;
			$v->products->map(function ($p,$i){
				if(!$p->pivot->price)
				{
					$p->pivot->price = 0;
				}
				return $p;
			});
			return $v;
		} );
		return response()->json( [

			'response' => $tenders,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function show( Request $request ) {
		$user      = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$validator = Validator::make( $request->all(), [
			'tender_id' => 'required|integer|exists:tenders,id',
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}

		$tender                = Tender::with( 'products' )->findOrFail( $request->tender_id );
		$tender->is_mine       = $tender->user_id == $user->id;
		$tender->product_count = $tender->products->count();

		$tender->is_sale = $tender->is_sell;
		$tender->founder = $tender->user;

		$tender->products->map( function ( $v, $k ) {
			if ( ! $v->pivot->price ) {
				$v->pivot->price = 0;
			}
			return $v;
		} );

		return response()->json( [

			'response' => $tender,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function create( Request $request ) {
		$user      = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$validator = Validator::make( $request->all(), [
			'terms' => 'required',
			'description' => 'required',
			'is_sale' => 'required|in:0,1',
			'delivery' => 'required|in:0,1',
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}

		if ( $user->user_type != 2 ) {
			$err = new ApiError( 404, null, 'You are not allowed to create tenders', 'user type is not "2"' );
			return $err->json();
		}

		$tender              = new Tender();
		$tender->user_id     = $user->id;
		$tender->terms       = Carbon::createFromTimestamp($request->terms)->format('Y-m-d H:i:s');
		$tender->status      = 0;
		$tender->is_accepted = 0;
		$tender->description = $request->description;
		$tender->is_private  = 0;
		$tender->is_sell     = $request->is_sale;
		$tender->delivery    = $request->delivery;
		$tender->save();


		//$testProducts = [ ['product_id'=>1,'quantity'=>2],['product_id'=>3,'quantity'=>1],['product_id'=>5,'quantity'=>3] ];
		//$testProductsJson = json_encode($testProducts);
		/* Присобачиваем товары к тендеру */
		$products = json_decode( $request->product_ids );
		if ( $products ) {
			foreach ( $products as $product ) {
				$tender->products()->attach( $product->product_id, [
					'quantity' => $product->quantity,
					'price' => isset( $product->price ) ? $product->price : null
				] );
			}
		}

		return response()->json( [

			'response' => $tender,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function edit( Request $request ) {
		$user      = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$validator = Validator::make( $request->all(), [
			'tender_id' => 'required|integer|exists:tenders,id',
			'terms' => 'required',
			'description' => 'required',
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}

		$tender = Tender::findOrFail( $request->tender_id );

		if ( $user->id != $tender->user_id ) {
			$err = new ApiError( 404, null, 'You are not allowed to edit tender', 'User is not a founder of tender' );
			return $err->json();
		}

		$tender->terms       = Carbon::createFromTimestamp($request->terms)->format('Y-m-d H:i:s');;
		$tender->description = $request->description;
		$tender->is_sell     = $request->is_sale;
		$tender->delivery    = $request->delivery;
		$tender->save();

		/* Присобачиваем товары к тендеру */
		$products = collect( $request->products );
		if ( $products ) {
			$tender->products()->detach();
			foreach ( $products as $product ) {
				$tender->products()->attach( $product->product_id, [
					'quantity' => $product->quantity,
					'price' => isset( $product->price ) ? $product->price : null
				] );
			}
		}

		return response()->json( [

			'response' => $tender,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function delete( Request $request ) {
		$user      = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$validator = Validator::make( $request->all(), [
			'tender_id' => 'required|integer|exists:tenders,id',
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}

		$tender = Tender::findOrFail( $request->tender_id );

		if ( $user->id != $tender->user_id ) {
			$err = new ApiError( 404, null, 'You are not allowed to delete tender', 'User is not a founder of tender' );
			return $err->json();
		}

		$tender->delete();

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function complete( Request $request ) {
		$user      = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$validator = Validator::make( $request->all(), [
			'tender_id' => 'required|integer|exists:tenders,id',
			'branch_id' => 'required|integer|exists:branches,id',
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}

		$tender = Tender::findOrFail( $request->tender_id );

		if ( $user->id != $tender->user_id ) {
			$err = new ApiError( 404, null, 'You are not allowed to delete tender', 'User is not a founder of tender' );
			return $err->json();
		}
		$tender->branch_id = $request->branch_id;
		$tender->status = 3;
		$tender->save();

		$receivers = [ $tender->user_id ];
		$this->createAlarm( 'tender', $tender, 'Тендер был завершен партнером', 'Тендер был завершен партнером', $receivers );

		if ( $tender->status == 3 ) {
			$this->createAlarm( 'tender', $tender, 'Вам необходимо оставить отзыв партнеру', 'Вам необходимо оставить отзыв партнеру', $receivers );
		}

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function productList( Request $request ) {
		$products = Product::orderBy( 'order', 'asc' )->with( 'images' )->get();

		return response()->json( [

			'response' => $products,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function cancel( Request $request ) {
		$user      = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$validator = Validator::make( $request->all(), [
			'tender_id' => 'required|integer|exists:tenders,id',
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}

		$tender = Tender::findOrFail( $request->tender_id );

		if ( $user->id != $tender->user_id ) {
			$err = new ApiError( 404, null, 'You are not allowed to delete tender', 'User is not a founder of tender' );
			return $err->json();
		}
		$tender->status = 4;
		$tender->save();

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function tenderProducts( Request $request ) {
		$user      = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$validator = Validator::make( $request->all(), [
			'tender_id' => 'required|integer|exists:tenders,id',
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}

		$tender = Tender::with( 'products', 'products.images' )->findOrFail( $request->tender_id );

		$products = $tender->products->map( function ( $v, $k ) {
			if ( ! $v->pivot->price ) {
				$v->pivot->price = 0;
			}
			return $v;
		} );

		return response()->json( [

			'response' => $products,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}
}
