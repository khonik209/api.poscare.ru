<?php

namespace App\Http\Controllers\Tenders;

use App\Classes\ApiError;
use App\Models\Tender;
use App\Models\TenderResponse;
use App\Traits\AlarmTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TenderResponseController extends Controller
{
	use AlarmTrait;

    public function list(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));
        $validator = Validator::make($request->all(), [
            'tender_id' => 'required|integer|exists:tenders,id',
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, NULL, NULL, $val_err->all());
            return $err->json();
        }

        $user_branch_ids = $user->branches->pluck('id')->toArray();

        $tender = Tender::findOrFail($request->tender_id);

        $tenderResponses = $tender->tenderResponses;
        $tenderResponses->map(function ($v, $k) use ($user_branch_ids) {
            $v->is_mine = in_array($v->branch_id, $user_branch_ids);
            $v->quantity_type = $v->time_key;
            $v->quantity = $v->time_value;
            $v->users_branches_id = $v->branch_id;
            return $v;
        });
        return response()->json([

            'response' => $tenderResponses,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function show(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));

        $validator = Validator::make($request->all(), [
            'tender_id' => 'required|integer|exists:tenders,id',
            'tender_response_id' => 'required|integer|exists:tender_responses,id',
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, NULL, NULL, $val_err->all());
            return $err->json();
        }

        $user_branch_ids = $user->branches->pluck('id')->toArray();

        $tenderResponse = TenderResponse::findOrFail($request->tender_response_id);

        $policy = false;
        if ($user->id == $tenderResponse->user_id) {
            $policy = true;
        }

        if (in_array($tenderResponse->branch_id, $user_branch_ids)) {
            $policy = true;
        }

        if (!$policy) {
            $err = new ApiError(404, NULL, 'You are not allowed to see response', 'user is not founder of tender or this response');
            return $err->json();
        }

        $tenderResponse->quantity_type = $tenderResponse->time_key;
        $tenderResponse->quantity = $tenderResponse->time_value;
        $tenderResponse->users_branches_id = $tenderResponse->branch_id;

        return response()->json([

            'response' => $tenderResponse,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function create(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));
        $validator = Validator::make($request->all(), [
            'price' => 'required',
            'comment' => 'required',
            'tender_id' => 'required|integer|exists:tenders,id',
            'users_branches_id' => 'required|integer|exists:branches,id',
            'quantity_type' => 'required',
            'quantity' => 'required',
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, NULL, NULL, $val_err->all());
            return $err->json();
        }

        if ($user->user_type != 2) {
            $err = new ApiError(404, NULL, 'You are not allowed to create response', 'user type is not "2"');
            return $err->json();
        }

        $user_branch_ids = $user->branches->pluck('id')->toArray();
        if (!in_array($request->branch_id, $user_branch_ids)) {
            $err = new ApiError(404, NULL, 'You are not allowed to create response', 'User is not a branch owner');
            return $err->json();
        }

        $tenderResponse = new TenderResponse();
        $tenderResponse->tender_id = $request->tender_id;
        $tenderResponse->branch_id = $request->users_branches_id;
        $tenderResponse->price = $request->price;
        $tenderResponse->time_key = $request->quantity_type;
        $tenderResponse->time_value = $request->quantity;
        $tenderResponse->comment = $request->comment;
        $tenderResponse->save();

	    $receivers = [$tenderResponse->bid->user_id];
	    $this->createAlarm('bid', $tenderResponse->bid, 'Поступило предложение на тендер', 'Поступило предложение на тендер', $receivers);

	    return response()->json([

            'response' => $tenderResponse,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function edit(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));
        $validator = Validator::make($request->all(), [
            'price' => 'required',
            'comment' => 'required',
            'tender_response_id' => 'required|integer|exists:tender_responses,id',
            'quantity_type' => 'required',
            'quantity' => 'required',
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, NULL, NULL, $val_err->all());
            return $err->json();
        }

        $tenderResponse = TenderResponse::findOrFail($request->tender_response_id);

        $user_branch_ids = $user->branches->pluck('id')->toArray();

        if (!in_array($tenderResponse->branch_id, $user_branch_ids)) {
            $err = new ApiError(404, NULL, 'You are not allowed to edit response', 'User is not a response owner');
            return $err->json();
        }

        $tenderResponse->price = $request->price;
        $tenderResponse->time_key = $request->quantity_type;
        $tenderResponse->time_value = $request->quantity;
        $tenderResponse->comment = $request->comment;
        $tenderResponse->save();

        return response()->json([

            'response' => $tenderResponse,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function delete(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));
        $validator = Validator::make($request->all(), [
            'tender_response_id' => 'required|integer|exists:tender_responses,id',
        ]);

        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, NULL, NULL, $val_err->all());
            return $err->json();
        }

        $tenderResponse = TenderResponse::findOrFail($request->tender_response_id);

        $user_branch_ids = $user->branches->pluck('id')->toArray();

        if (!in_array($tenderResponse->branch_id, $user_branch_ids)) {
            $err = new ApiError(404, NULL, 'You are not allowed to delete response', 'User is not a response owner');
            return $err->json();
        }

        $tenderResponse->delete();

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }
}
