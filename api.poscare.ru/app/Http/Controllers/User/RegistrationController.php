<?php

namespace App\Http\Controllers\User;

use App\Classes\ApiError;
use App\Models\Branch;
use App\Models\City;
use App\Models\Image;
use App\Models\PhoneSalt;
use App\Models\User;
use App\Models\Order;
use App\Models\UserOpt;
use App\Traits\GeoTrait;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RegistrationController extends Controller
{
    use GeoTrait;

    public function checkAuthToken(Request $request)
    {
        $auth_tok = Input::get('auth_token');

        $phone_salt = PhoneSalt::where('auth_token', $auth_tok)->where('is_accepted', 1)->first();

        if (!$phone_salt) {
            $err = new ApiError(341, NULL, "Требуется войти в приложение", "Токен не верный");
            return $err->json();
        }

        $user = $phone_salt->user;

        if (isset($user)) {

            $order = Order::where(['status' => 0, 'user_id' => $user->id])->first();

            $city = $user->city;

            $state = 1;
            $user_type = $user->user_type;
            $cart_count = $order ? $order->items->count() : 0;                  //todo сделать значение из бд
            $orders_id = $order ? $order->id : null;
            $is_service = $user->is_service;
            $is_shop = $user->is_shop;
            $city_id = $user->city_id;
            $city_name = $city->name;
            $city_lat = $city->center_lat;
            $city_lon = $city->center_lon;
            $city_utc_off = $city->utc_offset;
            $phone = $user->phoneSalt->phone;
            $orgform = $user->orgform;
            $show_head = $user->head_id && !$user->head_accepted ? 1 : 0;
        } else {
            $state = 0;
            $user_type = 0;
            $cart_count = 0;
            $orders_id = null;
            $is_service = 0;
            $is_shop = 0;
            $city_id = 0;
            $city_name = 0;
            $phone = 0;
            $city_lat = 0;
            $city_lon = 0;
            $city_utc_off = 0;
            $orgform = null;
            $show_head = 0;
        }

        return response()->json([

            'response' => [

                'state' => $state,
                'user_type' => $user_type,
                'cart_count' => $cart_count,
                'orders_id' => $orders_id,
                'is_service' => $is_service,
                'is_shop' => $is_shop,
                'city' => [
                    'id' => $city_id,
                    'name' => $city_name,
                    'center_lat' => $city_lat,
                    'center_lon' => $city_lon,
                    'utc_offset' => $city_utc_off,
                ],
                'phone' => $phone,
                'orgform' => $orgform,
                'show_head' => $show_head

            ]

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function regUser(Request $request)
    {

        $phone_id = PhoneSalt::where('auth_token', $request->input('auth_token'))
            ->where('is_accepted', 1)
            ->get(['id'])
            ->first()
            ->id;

        if (User::where('phone_id', $phone_id)->exists()) {

            $err = new ApiError(341, NULL,
                'Пользователь с таким телефоном уже существует',
                "Пользователь с таким телефоном уже существует");
            return $err->json();

        }
        if (User::where('email', $request->email)->exists()) {

            $err = new ApiError(341, NULL,
                'Пользователь с таким email уже существует',
                "Пользователь с таким email уже существует");
            return $err->json();

        }
        $required_params = [
            'name', 'email', 'password',
            'user_type', 'orgform', 'city_id'
        ];

        $not_valid_param = $this->checkRequiredParams($required_params);

        if (isset($not_valid_param)) {
            $err = new ApiError(305, $not_valid_param);
            return $err->json();
        }

        unset($required_params, $not_valid_param);

        $org_form = $request->input('orgform');

        if ($org_form === '2') {

            $required_params = [
                'short_name', 'full_name',
                'inn', 'description', 'open_hours_from',
                'open_hours_to', 'address', 'site'
            ];

            $not_valid_param = $this->checkRequiredParams($required_params);

            if (isset($not_valid_param)) {
                $err = new ApiError(345,
                    NULL,
                    'Заполните все поля',
                    'Не передан обязательный параметр для orgform = 2 ,' . $not_valid_param);
                return $err->json();
            }

            unset($required_params, $not_valid_param);

        }

        $user = new User;

        //Обязательные для всех параметры
        $user->name = $request->input('name');


        if ($org_form !== '2') {

            if ($request->has('birthday'))
                $user->birthday = $request->input('birthday');

            else {
                $err = new ApiError(345,
                    'birthday',
                    'Заполните все поля',
                    'Не передан обязательный параметр');
                return $err->json();
            }
        }


        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->user_type = $request->input('user_type');
        $user->orgform = $org_form;

        $city_id = $request->input('city_id');
        if (City::where('id', $city_id)->exists())
            $user->city_id = $city_id;

        else {
            $err = new ApiError(349,
                NULL,
                'Нет такого города',
                'Нет такого города city_id - ' . $city_id);
            return $err->json();
        }


        $user->phone_id = $phone_id;

        //Параметры не обязательные , но для всех
        if ($request->exists('surname'))
            $user->surname = $request->input('surname');
        if ($request->exists('lat'))
            $user->lat = $request->input('lat');
        if ($request->exists('lon'))
            $user->lon = $request->input('lon');

        //Параметры для user_type = 2
        if ($request->input('user_type') === '2') {

            if ($request->exists('is_service'))
                $user->is_service = $request->input('is_service');

            if ($request->exists('is_shop'))
                $user->is_shop = $request->input('is_shop');

        }

        $user->save();

        $user->myRating('register');

        //Проверка при user_type = 2
        if ($user->user_type == 2) {
            if (!isset($user->is_service) && !isset($user->is_shop)) {

                $err = new ApiError(343,
                    NULL,
                    'Одно из значений - продажа товара/оказание услуги должно быть выбрано',
                    'Не передан обязательный параметр для user_type = 2');
                return $err->json();

            } elseif ($user->is_service != '1' && $user->is_shop != '1') {

                $err = new ApiError(344,
                    NULL,
                    'Одно из значений - продажа товара/оказание услуги должно быть выбрано',
                    'Один из обязательных параметров для user_type = 2 должен быть = 1');
                return $err->json();

            }
	        $resp = $this->getApiAddressCoords($request->address);

	        $branch = new Branch;

	        $branch->type = 1; // todo
	        $branch->user_id = $user->id;
	        $branch->name = $request->input('branch_name'); // todo
	        $branch->open_hours_from = $request->input('open_hours_from') . ':00';
	        $branch->open_hours_to = $request->input('open_hours_to') . ':00';
	        $branch->address = $request->input('address');
	        $branch->city_id = $request->input('city_id');
	        $branch->address_lat = $resp['lat'];
	        $branch->address_lon = $resp['lon'];
	        $branch->email = $request->email;
	        $branch->phone = $request->phone;
	        $branch->description = $request->description;
	        $branch->save();

	        $user->myRating('new_branch');
        }

        if ($request->hasFile('image_file')) {

            $file = $request->file('image_file');

            $validator_image = Validator::make($request->all(), [
                'image_file' => 'image|mimes:jpg,jpeg,png',
            ]);

            if ($validator_image->fails()) {

                $err = new ApiError(341,
                    NULL,
                    "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png",
                    "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                return $err->json();

            }

            $path = $file->storePublicly('public');

            $image = new Image;

            $image->href = Storage::url($path);
            $image->user_id = $user->id;
            $image->is_accepted = 2;

            $image->save();
            $user->myRating('new_image');
        }

        $user_opt = NULL;

        if ($org_form == 2) {

            $user_opt = new UserOpt;
            $user_opt->short_name = $request->input('short_name');
            $user_opt->full_name = $request->input('full_name');
            $user_opt->inn = $request->input('inn');
            $user_opt->description = $request->input('description');
            $user_opt->open_hours_from = $request->input('open_hours_from') . ":00";
            $user_opt->open_hours_to = $request->input('open_hours_to') . ":00";
            $user_opt->site = $request->input('site');
            if (isset($image)) {
                $user_opt->image_id = $image->id;
            }
            //Получение координат из адреса
            $addr = '' . $request->input('address');
            $user_opt->address = $addr;

            $resp = $this->getApiAddressCoords($addr);

            if ($resp instanceof ApiError)
                return $resp->json();

            $user_opt->address_lat = $resp['lat'];
            $user_opt->address_lon = $resp['lon'];
            $user_opt->user_id = $user->id;
            $user_opt->save();

            $user->myRating('options');
        }

        if (isset($image)) {
            return response()->json([

                'response' => [
                    'image_id' => $image->id,
                    'href' => $image->href,
                ]

            ], 200, [], JSON_UNESCAPED_UNICODE);
        } else {
            return response()->json([

                'response' => 1

            ], 200, [], JSON_UNESCAPED_UNICODE);
        }
    }

}
