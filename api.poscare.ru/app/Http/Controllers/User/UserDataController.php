<?php

namespace App\Http\Controllers\User;

use App\Classes\ApiError;
use App\Models\Alarm;
use App\Models\City;
use App\Models\Image;
use App\Reward;
use App\Traits\GeoTrait;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\PhoneSalt;
use App\Models\User;
use App\Models\UserOpt;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class UserDataController extends Controller
{
    use GeoTrait;

    public function getInfo(Request $request)
    {
        $user = $this->getCurrentUser($request->input('auth_token'));

        $phone_salt = $user->phoneSalt;

        $user_phone = $phone_salt->phone;
        $app_type = $phone_salt->app_type;


        $name = $user->name;
        $surname = $user->surname ?? 0;
        $birthday = $user->birthday;
        $email = $user->email ?? 0;
        $user_type = $user->user_type;
        $orgform = $user->orgform;
        $city_id = $user->city_id;
        $lat = $user->lat ?? 0;
        $lon = $user->lat ?? 0;
        $is_service = $user->is_service ?? 0;
        $is_shop = $user->is_shop ?? 0;

        $userOpt = $user->userOpt;
        $logo_id = $userOpt ? $userOpt->image_id : 0;
        if ($userOpt && $userOpt->logo) {
            $logo_href = $userOpt->logo->href;
        } else {
            $logo_href = '/storage_1/3XzuBTO3seg5BChpZgf0ISf6JshIqkdFPuTwlzda.png';
        }
        $reponse = [
            'name' => $name,
            'surname' => $surname,
            'birthday' => $birthday,
            'email' => $email,
            'phone' => $user_phone,
            'user_type' => $user_type,
            'orgform' => $orgform,
            'app_type' => $app_type,
            'city_id' => $city_id,
            'city_name' => $user->city->name,
            'lat' => $lat,
            'lon' => $lon,
            'is_service' => $is_service,
            'is_shop' => $is_shop,
            'image_id' => $logo_id,
            'href' => $logo_href,
        ];

        if ($orgform == '2') {

            if (isset($userOpt)) {

                $reponse['short_name'] = $userOpt->short_name;
                $reponse['full_name'] = $userOpt->full_name;
                $reponse['inn'] = $userOpt->inn;
                $reponse['description'] = $userOpt->description;
                $reponse['open_hours_from'] = Carbon::parse($userOpt->open_hours_from)->format('H:i');
                $reponse['open_hours_to'] = Carbon::parse($userOpt->open_hours_to)->format('H:i');
                $reponse['address'] = $userOpt->address;
                $reponse['address_lat'] = $userOpt->address_lat;
                $reponse['address_lon'] = $userOpt->address_lon;
                $reponse['site'] = $userOpt->site;

            }

        }

        return response()->json([

            'response' => $reponse,

        ], 200, [], JSON_UNESCAPED_UNICODE);


    }

    public function changetype(Request $request)
    {
        $user = $this->getCurrentUserModel($request->input('auth_token'));

        $params = $request->except('auth_token');

        $want2be = [];
        foreach ($params as $param => $value) {
            $want2be[$param] = $value;
        }

        $user->want2be = (count($want2be) == 0) ? null : json_encode($want2be); // todo: переделать через читатель и преобразователь
        $user->save();

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function editInfo(Request $request)
    {
        $user = $this->getCurrentUserModel($request->input('auth_token'));

        $validator = Validator::make($request->all(), [
            'name' => 'string',
            'surname' => 'string',
            'birthday' => 'integer',
            'email' => 'email',
            'city_id' => 'integer|exists:cities,id',
            'lat' => 'numeric',
            'lon' => 'numeric',
            'is_service' => 'boolean',
            'is_shop' => 'boolean',
            'short_name' => 'string',
            'full_name' => 'string',
            'inn' => 'numeric',
            'description' => 'string',
            'open_hours_from' => 'date_format:H:i',
            'open_hours_to' => 'date_format:H:i',
            'address' => 'string',
            'address_lat' => 'numeric',
            'address_lon' => 'numeric',
            'site' => 'string'
        ]);
        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }

        if ($request->hasFile('image_file')) {

            $file = $request->file('image_file');

            $validator_image = Validator::make($request->all(), [
                'image_file' => 'image|mimes:jpg,jpeg,png',
            ]);

            if ($validator_image->fails()) {

                $err = new ApiError(341, null, "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png", "Неверный формат файла.Доступные форматы изображений - jpg,jpeg,png");
                return $err->json();

            }

            $path = $file->storePublicly('public');

            $image = new Image;

            $image->href = Storage::url($path);
            $image->user_id = $user->id;
            $image->is_accepted = 2;

            $image->save();
            $user->myRating('new_image');
        }
        if ($request->name) {
            $user->name = $request->input('name');
        }
        if ($user->orgform != 2) {

            if ($request->has('birthday')) {
                $user->birthday = $request->input('birthday');
            } else {
                $err = new ApiError(345, 'birthday', 'Заполните все поля', 'Не передан обязательный параметр');
                return $err->json();
            }
        }

        if ($request->email) {
            $user->email = $request->input('email');
        }
        if ($request->user_type) {
            $user->user_type = $request->input('user_type');
        }
        if ($request->city_id) {
            $city_id = $request->input('city_id');
            $city = City::where('id', $city_id)->first();
            if ($city) {
                $user->city_id = $city_id;
            } else {
                $err = new ApiError(349, null, 'Нет такого города', 'Нет такого города city_id - ' . $city_id);
                return $err->json();
            }
        }
        //Параметры не обязательные , но для всех
        if ($request->exists('surname')) {
            $user->surname = $request->input('surname');
        }
        if ($request->exists('lat')) {
            $user->lat = $request->input('lat');
        }
        if ($request->exists('lon')) {
            $user->lon = $request->input('lon');
        }


        //Параметры для user_type = 2
        if ($user->user_type == 2) {

            if ($request->exists('is_service')) {
                $user->is_service = $request->input('is_service');
            }

            if ($request->exists('is_shop')) {
                $user->is_shop = $request->input('is_shop');
            }

        }

        //Проверка при user_type = 2
        if ($user->user_type === '2') {
            if (!isset($user->is_service) && !isset($user->is_shop)) {

                $err = new ApiError(343, null, 'Одно из значений - продажа товара/оказание услуги должно быть выбрано', 'Не передан обязательный параметр для user_type = 2');
                return $err->json();

            } elseif ($user->is_service !== '1' && $user->is_shop !== '1') {

                $err = new ApiError(344, null, 'Одно из значений - продажа товара/оказание услуги должно быть выбрано', 'Один из обязательных параметров для user_type = 2 должен быть = 1');
                return $err->json();

            }
        }
        if ($user->user_type == 2) {
            $user_opt = $user->userOpt;
            if (!$user_opt) {
                $user_opt = new UserOpt;
                $user_opt->user_id = $user->id;
            }
            if (isset($image)) {
                $user_opt->image_id = $image->id;
            }

            if ($request->short_name) {
                $user_opt->short_name = $request->input('short_name');
            }
            if ($request->full_name) {
                $user_opt->full_name = $request->input('full_name');
            }
            if ($request->inn) {
                $user_opt->inn = $request->input('inn');
            }
            if ($request->description) {
                $user_opt->description = $request->input('description');
            }
            if ($request->open_hours_from) {
                $user_opt->open_hours_from = $request->input('open_hours_from') . ":00";
            }
            if ($request->open_hours_to) {
                $user_opt->open_hours_to = $request->input('open_hours_to') . ":00";
            }
            if ($request->site) {
                $user_opt->site = $request->input('site');
            }
            //Получение координат из адреса
            if ($request->address) {
                $addr = '' . $request->input('address');
                $user_opt->address = $addr;

                $resp = $this->getApiAddressCoords($addr);

                if ($resp instanceof ApiError) {
                    return $resp->json();
                }

                $user_opt->address_lat = $resp['lat'];
                $user_opt->address_lon = $resp['lon'];
            } elseif (isset($city)) {
                $resp = $this->getApiAddressCoords($city->name);
                if ($resp instanceof ApiError) {
                    return $resp->json();
                }

                $user_opt->address_lat = $resp['lat'];
                $user_opt->address_lon = $resp['lon'];
            } else {
                /**/
            }
            // $user_opt->address_lat = $request->input('lat');
            //$user_opt->address_lon = $request->input('lon');


            $user_opt->save();
            $user->myRating('options');
        }
        $user->save();

        if (isset($image)) {
            return response()->json([

                'response' => [
                    'image_id' => $image->id,
                    'href' => $image->href,
                ]

            ], 200, [], JSON_UNESCAPED_UNICODE);
        } else {
            return response()->json([

                'response' => 1

            ], 200, [], JSON_UNESCAPED_UNICODE);
        }

    }

    public function getRewards(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }
        $user = User::find($request->user_id);
        if (!$user) {
            $err = new ApiError(404, null, null, 'User not found');
            return $err->json();
        }

        $rewards = $user->rewards;

        return response()->json([

            'response' => $rewards

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }
}
