<?php

namespace App\Http\Controllers\User;

use App\Classes\ApiError;
use App\Models\PhoneSalt;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use \Zelenin\SmsRu\Api as SMSApi;
use \Zelenin\SmsRu\Auth\ApiIdAuth as SMSApiIdAuth;
use \Zelenin\SmsRu\Entity\Sms;

class HeadController extends Controller
{
    public function userList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'auth_token' => 'required',
        ]);
        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }
        $user = $this->getCurrentUser($request->input('auth_token'));

        if ($user->head_id && $user->head_accepted) {
            $bossId = $user->head_id;
        } else {
            $bossId = $user->id;
        }

        $filter = $request->filter;

        $users = User::where('head_id', $bossId)->orWhere('id', $bossId)->with('phoneSalt')->get();

        $users = $users->map(function ($v, $k) use ($bossId) {
            $v->is_head = $v->id == $bossId;
            $v->head_accepted_date = $v->head_accepted_date;
            return $v;
        })->filter(function ($v, $k) use ($filter, $bossId) {
            if ($filter == 'wait') {
                return $v->head_accepted === 0 && $v->id != $bossId;
            } elseif ($filter == "grouped") {
                return $v->head_accepted === 1 || $v->id == $bossId;
            } else {
                return true;
            }
        });

        return response()->json([

            'response' => [
                'users' => $users
            ]

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function deleteUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer',
            'auth_token' => 'required'
        ]);
        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }
        $boss = $this->getCurrentUser($request->input('auth_token'));
        $user = User::findOrFail($request->user_id);

        if ($user->head_id != $boss->id) {
            $err = new ApiError(500, null, 'У вас недостаточно прав для удаления сотрудника', 'You have not allowed to do this');
            return $err->json();
        }

        $user->head_accepted = 0;
        $user->head_id = null;
        $user->save();

        return response()->json([

            'response' => 1

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function newUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'auth_token' => 'required'
        ]);
        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }
        $boss = $this->getCurrentUser($request->input('auth_token'));
        if ($boss->head_id) {
            /* Подчиненный не может быть боссом */
            $err = new ApiError(299, null, 'You can`t add a user', 'User is already in team');
            return $err->json();
        }
        /* check phone */
        $checkPhone = PhoneSalt::where('phone', $request->phone)->first();
        if ($checkPhone) {
            $phoneSalt = $checkPhone;
        } else {
            $phoneSalt = new PhoneSalt;
            $phoneSalt->phone = $request->phone;
            $phoneSalt->auth_token = $request->auth_token;
        }
        $phoneSalt->is_accepted = 1;
        $phoneSalt->save();
        /* check user */
        $checkUser = $phoneSalt->user;
        if ($checkUser) {
            $user = $checkUser;
            if ($user->head_id) {
                $err = new ApiError(299, null, 'You can`t add a user', 'User is already in team');
                return $err->json();
            }
            $checkTeam = User::where('head_id', $user->id)->where('head_accepted', 1)->first();
            if ($checkTeam) {
                $err = new ApiError(299, null, 'You can`t add a user', 'User is already in team');
                return $err->json();
            }
            $user->head_accepted = 0;
        } else {
            $password = str_random(8);
            $user = new User;
            $user->phone_id = $phoneSalt->id;
            $user->email = $boss->email;
            $user->password = bcrypt($password);
            $user->head_accepted = 1;
        }
        $user->head_id = $boss->id;

        $user->save();

        return response()->json([

            'response' => [
                'new_user_id' => $user->id,
                'password' => isset($password) ? $password : null,
                'phone' => $phoneSalt->phone,
            ]

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function acceptHead(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }
        $user = User::find($request->user_id);
        $user->head_accept = 1;
        $user->save();
        return response()->json([

            'response' => 1

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function denyHead(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            $val_err = $validator->errors();
            $err = new ApiError(299, null, null, $val_err->all());
            return $err->json();
        }
        $user = User::find($request->user_id);
        $user->head_id = null;
        $user->save();
        return response()->json([

            'response' => 1

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    protected function changePhone($phone)
    {

        $phone = str_replace("+7 (", "7", $phone);
        $phone = str_replace(") ", "", $phone);
        $phone = str_replace("-", "", $phone);

        return $phone;

    }
}
