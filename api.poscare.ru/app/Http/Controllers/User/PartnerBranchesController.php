<?php

namespace App\Http\Controllers\User;

use App\Classes\ApiError;
use App\Models\Branch;
use App\Models\BranchImage;
use App\Models\Image;
use App\Models\Service;
use App\Reward;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\PartnerSupply;
use App\Traits\GeoTrait;

class PartnerBranchesController extends Controller {
	use GeoTrait;

	private $default_logo_href = '/storage_1/3XzuBTO3seg5BChpZgf0ISf6JshIqkdFPuTwlzda.png';

	public function newBranch( Request $request ) {

		$validator = Validator::make( $request->all(), [
			'type' => 'required|integer|between:1,2',
			'name' => 'required',
			'open_hours_from' => [
				'required',
				'regex:/^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/si',
			],
			'open_hours_to' => [
				'required',
				'regex:/^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/si',
			],
			'address' => 'required',
			'city_id' => 'required|integer|exists:cities,id',
			// 'address_lat' => [
			//     'required','regex:^-?\d{2}\.\d{6}$^',
			// ],
			// 'address_lon' => [
			//     'required','regex:^-?\d{2}\.\d{6}$^',
			//     ]
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}
		$resp = $this->getApiAddressCoords( $request->address );

		if ( $resp instanceof ApiError ) {
			return $resp->json();
		}

		$user = $this->getCurrentUser( $request->input( 'auth_token' ) );


		if ( $user->user_type !== 2 ) {
			$err = new ApiError( 308 );
			return $err->json();
		}

		$br_type = + $request->input( 'type' );

		/*if(gettype($br_type) != "integer" || ($br_type !== 1 && $br_type !== 0)){
			$err = new ApiError(307,'type');
			return $err->json();
		}*/

		if ( $br_type === 1 && + $user->is_service === 0 ) {

			$err = new ApiError( 341, null, 'Нужно отметить ,что вы занимаетесь сервисом', "партнёр не может заниматься сервисом" );
			return $err->json();

		}

		if ( $br_type === 2 && + $user->is_service === 0 ) {

			$err = new ApiError( 342, null, 'Нужно отметить ,что вы занимаетесь продажами', "партнёр не может заниматься продажей" );
			return $err->json();

		}

		$branch = new Branch;

		$branch->type            = $br_type;
		$branch->user_id         = $user->id;
		$branch->name            = $request->input( 'name' );
		$branch->open_hours_from = $request->input( 'open_hours_from' ) . ':00';
		$branch->open_hours_to   = $request->input( 'open_hours_to' ) . ':00';
		$branch->address         = $request->input( 'address' );
		$branch->city_id         = $request->input( 'city_id' );
		$branch->address_lat     = $resp['lat'];
		$branch->address_lon     = $resp['lon'];
		$branch->email           = $request->email;
		$branch->phone           = $request->phone;
		$branch->description     = $request->description;
		$branch->save();

		$user->myRating( 'new_branch' );

		return response()->json( [

			'response' => [ 'branch_id' => $branch->id ],

		], 200, [], JSON_UNESCAPED_UNICODE );

	}

	public function addBranchPhotos( Request $request ) {

		$validator = Validator::make( $request->all(), [
			'branch_id' => 'required|exists:branches,id',
			'branch_images' => 'required|array',
			'branch_images.*' => 'required|integer|exists:images,id',
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		//Проверка принадлежности филиала пользователю
		$user = $this->getCurrentUser( $request->input( 'auth_token' ) );

		$user_id = $user->id;

		$br_id = $request->input( 'branch_id' );

		$branch = Branch::where( 'user_id', $user_id )->where( 'id', $br_id )->first();

		if ( ! $branch ) {
			$err = new ApiError( 308 );
			return $err->json();
		}

		/*
		 * Проверка картинок на принадлежность пользоваетелю
		 * и прохождения модерации
		*/

		$images_ids = $request->input( 'branch_images' );

		$images = Image::whereIn( 'id', $images_ids )->get();

		foreach ( $images as $image ) {

			if ( $image->user_id !== $user_id ) {
				$err = new ApiError( 342, null, 'У вас нет прав на одно из изображений', 'Изображение id = ' . $image->id . ' не принадлежит пользователю id = ' . $user_id );
				return $err->json();
			}

		}

		try {

			$branch->images()->sync( $images->pluck( 'id' ) );

		} catch ( QueryException $ex ) {
			$err = new ApiError( 310 );
			return $err->json();
		}

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );

	}

	public function deleteBranchPhoto( Request $request ) {

		$validator = Validator::make( $request->all(), [
			'image_id' => 'required|integer|exists:images,id',
			'branch_id' => 'required|integer|exists:branches,id',
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		$image_id = $request->input( 'image_id' );
		$br_id    = $request->input( 'branch_id' );

		//Проверка прав

		//На СЦ
		$user = $this->getCurrentUserModel( $request->input( 'auth_token' ) );

		$brchs_bld = $user->branches()->find( $br_id );

		if ( ! $brchs_bld->exists() ) {
			$err = new ApiError( 308, null, 'Вы не можете редактировать данный филиал' );
			return $err->json();
		}

		//На фотографию
		$is_img_owner = $user->images()->where( 'images.id', $image_id )->exists();

		if ( ! $is_img_owner ) {

			$err = new ApiError( 308, null, 'Вы не владелец данной картинки' );
			return $err->json();

		}

		$br_img_bound = BranchImage::where( 'branch_id', $br_id )->where( 'image_id', $image_id )->first();

		$is_deleted = $br_img_bound->delete();

		if ( ! $is_deleted ) {
			$err = new ApiError( 310 );
			return $err->json();
		}

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );

	}

	public function editBranch( Request $request ) {

		$validator = Validator::make( $request->all(), [
			'branch_id' => 'required|integer|exists:branches,id',

			'name' => 'required_without_all:open_hours_from,
            open_hours_to,city_id,address,address_lat,address_lon',

			'open_hours_from' => [
				'required_without_all:name,
            open_hours_to,city_id,address,address_lat,address_lon',
				'regex:/^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/si',
			],

			'open_hours_to' => [
				'required_without_all:name,
            open_hours_from,city_id,address,address_lat,address_lon',
				'regex:/^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/si',
			],

			'city_id' => 'required_without_all:open_hours_from,
            open_hours_to,name,address,address_lat,address_lon|
            integer|exists:cities,id',

			'address' => 'required_without_all:open_hours_from,
            open_hours_to,name,city_id,address_lat,address_lon',

			'address_lat' => [
				'required_without_all:open_hours_from,
            open_hours_to,city_id,address,name,address_lon',
				'regex:^-?\d{2}\.\d{6}$^',
			],

			'address_lon' => [
				'required_without_all:open_hours_from,
            open_hours_to,city_id,address,name,address_lat',
				'regex:^-?\d{2}\.\d{6}$^',
			],
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		$brnch_id = $request->input( 'branch_id' );

		//Проверка прав
		$user = $this->getCurrentUserModel( $request->input( 'auth_token' ) );

		$brnch_bld = $user->branches()->where( 'id', $brnch_id );

		if ( ! $brnch_bld->exists() ) {

			$err = new ApiError( 308 );

			return $err->json();

		}

		$branch = $brnch_bld->first();

		if ( $request->has( 'name' ) ) {
			$branch->name = $request->input( 'name' );
		}

		if ( $request->has( 'open_hours_from' ) ) {
			$branch->open_hours_from = $request->input( 'open_hours_from' );
		}

		if ( $request->has( 'open_hours_to' ) ) {
			$branch->open_hours_to = $request->input( 'open_hours_to' );
		}

		if ( $request->has( 'city_id' ) ) {
			$branch->city_id = $request->input( 'city_id' );
		}

		if ( $request->has( 'address' ) ) {
			$branch->address = $request->input( 'address' );
			$addr            = '' . $request->input( 'address' );
			$resp            = $this->getApiAddressCoords( $addr );
			if ( $resp instanceof ApiError ) {
				return $resp->json();
			}

			$branch->address_lat = $resp['lat'];
			$branch->address_lon = $resp['lon'];
		}

		if ( $request->has( 'phone' ) ) {
			$branch->phone = $request->input( 'phone' );
		}

		if ( $request->has( 'email' ) ) {
			$branch->email = $request->input( 'email' );
		}

		if ( $request->has( 'description' ) ) {
			$branch->description = $request->description;
		}
		$is_saved = $branch->save();

		if ( ! $is_saved ) {
			$err = new ApiError( 310 );
			return $err->json();
		}

		return response()->json( [

			'response' => [ 'branch_id' => $branch->id ],

		], 200, [], JSON_UNESCAPED_UNICODE );

	}

	public function deleteBranch( Request $request ) {
		$user = $this->getCurrentUserModel( $request->input( 'auth_token' ) );

		//Проверяем ,является ли покупателем

		if ( $user->user_type !== 2 ) {
			$err = new ApiError( 308 );
			return $err->json();
		}

		$validator = Validator::make( $request->all(), [
			'branch_id' => [
				'required',
				'integer',
				Rule::exists( 'branches', 'id' )->where( function ( $query ) use ( $user ) {
					$query->where( 'user_id', $user->id );
				} ),
			],
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		Branch::find( $request->branch_id )->delete();

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function getBranchesList( Request $request ) {
		$user = $this->getCurrentUserModel( $request->input( 'auth_token' ) );

		$branches = $user->branches()->with( [ 'reviews', 'user.userOpt' ] )->get()->map( function ( $branch, $key ) {
			return [
				'id' => $branch->id,
				'name' => $branch->name,
				'address' => $branch->address,
				'address_lon' => $branch->address_lon,
				'address_lat' => $branch->address_lat,
				'open_hours_from' => $branch->open_hours_from ? date( 'H:i', strtotime( $branch->open_hours_from ) ) : null,
				'open_hours_to' => $branch->open_hours_to ? date( 'H:i', strtotime( $branch->open_hours_to ) ) : null,
				'reviews_count' => $branch->reviews()->where( 'is_accepted', 1 )->count(),
				'reviews_rating' => round( $branch->reviews()->where( 'is_accepted', 1 )->avg( 'stars' ) ),
				'logo_id' => $branch->user->userOpt ? $branch->user->userOpt->image_id : 0,
				'logo_href' => $branch->user->userOpt ? $branch->user->userOpt->logo->href : $this->default_logo_href,
				'description' => $branch->description
			];
		} );
		// $logos = $user->userOpt

		return response()->json( [

			'response' => $branches,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function getServicesList( Request $request ) {
		$user = $this->getCurrentUserModel( $request->input( 'auth_token' ) );

		$services = $user->services->map( function ( $v, $k ) {
			return [
				'id' => $v->id,
				'name' => $v->name
			];
		} )->values();

		return response()->json( [

			'response' => $services,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function getBranch( Request $request ) {
		$user      = $this->getCurrentUserModel( $request->input( 'auth_token' ) );
		$validator = Validator::make( $request->all(), [
			'user_branch_id' => [
				'required',
				'integer',
				Rule::exists( 'branches', 'id' )->where( function ( $query ) use ( $user ) {
					$query->where( 'user_id', $user->id );
				} ),
			],
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		$branch       = Branch::where( 'id', $request->user_branch_id )->with( [ 'user.userOpt', 'reviews' ] )->first();
		$rewardsCount = $user->rewards->count();
		$response     = [
			'id' => $branch->id,
			'name' => $branch->name,
			'address' => $branch->address,
			'address_lon' => $branch->address_lon,
			'address_lat' => $branch->address_lat,
			'open_hours_from' => $branch->open_hours_from ? date( 'H:i', strtotime( $branch->open_hours_from ) ) : null,
			'open_hours_to' => $branch->open_hours_to ? date( 'H:i', strtotime( $branch->open_hours_to ) ) : null,
			'reviews_count' => $branch->reviews()->where( 'is_accepted', 1 )->count(),
			'reviews_rating' => round( $branch->reviews()->where( 'is_accepted', 1 )->avg( 'stars' ) ),
			'logo_id' => $branch->user->userOpt ? $branch->user->userOpt->image_id : 0,
			'logo_href' => $branch->user->userOpt ? $branch->user->userOpt->logo->href : $this->default_logo_href,
			'phone' => $branch->phone,
			'email' => $branch->email,
			'city_name' => $branch->city->name,
			'city_id' => $branch->city->id,
			'description' => $branch->description,
			'rewards_count' => $rewardsCount,
			'user_branch_user_id' => $branch->user_id
		];

		$usr_br_imgs = $branch->images->sortByDesc( 'updated_at' )->toArray();
		$images_arr  = [];

		foreach ( $usr_br_imgs as $usr_br_img ) {
			$image_arr = [];

			$image_arr['id']   = $usr_br_img['id'];
			$image_arr['href'] = $usr_br_img['href'];

			array_push( $images_arr, $image_arr );

		}

		//ЮР лицо
		if ( $user->orgform === 2 ) {
			$usr_opt                         = $branch->user->userOpt()->first();
			$response['company_description'] = $usr_opt->description;
		} //Физ лицо
		elseif ( $user->orgform === 1 ) {
			$response['company_description'] = 'Нет описания';
		}

		$response['user_branch_images'] = $images_arr;
		$response['awards']             = 3;

		return response()->json( [

			'response' => $response,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function getBranchContacts( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'branch_id' => [ 'required', 'integer', 'exists:branches,id' ],
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}
		$branch = Branch::findOrFail( $request->branch_id );

		if ( ! $branch->email ) {
			$branch->email = $branch->user->email;
		}

		if ( ! $branch->phone ) {
			$user = $branch->user;
			if ( $user ) {
				$phone = $user->phone;
				if ( $phone ) {
					$number = $user->number;
					if ( $number ) {
						$branch->phone = $number;
					}
				}
			}

		}

		//   $branch->save();

		//  $response = $branch->pluck(['id', 'address', 'phone', 'email']);

		$response = [
			'id' => $branch->id,
			'address' => $branch->address,
			'email' => $branch->email,
			'phone' => $branch->phone
		];

		return response()->json( [

			'response' => $response,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}
}
