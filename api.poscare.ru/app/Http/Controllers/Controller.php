<?php

namespace App\Http\Controllers;

use App\Classes\ApiError;
use app\Jobs\SendPushNotification;
use App\Models\Alarm;
use App\Models\Bid;
use App\Models\Order;
use App\Models\PhoneSalt;
use App\Models\User;
use Edujugon\PushNotification\Facades\PushNotification;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use ApnsPHP_Push;
use ApnsPHP_Message;
use ApnsPHP_Abstract;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function checkRequiredParams($req_params)
    {

        foreach ($req_params as $req_param) {

            if (!Input::has($req_param) || !Input::filled($req_param))
                return $req_param;
        }

        return NULL;
    }

    protected function getCurrentUser($auth_token)
    {
        $phone = PhoneSalt::where('auth_token', $auth_token)
            ->where('is_accepted', 1)
            ->first();
        if (!isset($phone)) {
            $err = new ApiError(341, null, "Требуется войти в приложение", "Пользователь не найден");
            return $err->json();
        }
        $user = $phone->user;
        if (!isset($user)) {
            $err = new ApiError(341, null, "Требуется войти в приложение", "Пользователь не найден");
            return $err->json();
        }
        if ($user->head_id && $user->head_accepted) {
            $checkBoss = User::find($user->head_id);
            if ($checkBoss) {
                return $checkBoss;
            }
        }
        return $user;
    }

    public function getCurrentUserModel($auth_token)
    {
        $user = PhoneSalt::where('auth_token', $auth_token)
            ->where('is_accepted', 1)
            ->first()
            ->user()
            ->first();
        if ($user->head_id && $user->head_accepted) {
            $checkBoss = User::find($user->head_id);
            if ($checkBoss) {
                return $checkBoss;
            }
        }
        return $user;
    }

    public function timeToApiResponse($time)
    {

        $time_params = explode(':', $time);

        return $time_params[0] . ':' . $time_params[1];


    }

    /**
     * @param $alarm
     * @param $receivers
     * @throws \ApnsPHP_Exception
     */
    protected function sendPush($alarm, $receivers)
    {

        // Instantiate a new ApnsPHP_Push object
        $push = new ApnsPHP_Push(\ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, __DIR__ . '/ck.pem');

// Set the Provider Certificate passphrase
        $push->setProviderCertificatePassphrase(config('pushnotification.apn.passPhrase'));

// Set the Root Certificate Autority to verify the Apple remote peer
        $push->setRootCertificationAuthority(__DIR__ . '/entrust_root_certification_authority.pem');

// Connect to the Apple Push Notification Service
        $push->connect();

        foreach ($receivers as $receiver) {
            try {
                $token = User::find($receiver)->phoneSalt->device_token;
                // Instantiate a new Message with a single recipient
                $message = new ApnsPHP_Message($token);

// Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
// over a ApnsPHP_Message object retrieved with the getErrors() message.
                $message->setCustomIdentifier("Message-Badge-3");

// Set badge icon to "3"
                $message->setBadge(3);

// Set a simple welcome text
                $message->setText($alarm->title);

// Play the default sound
                $message->setSound();

// Set a custom property
                //           $message->setCustomProperty('acme2', array('bang', 'whiz'));

// Set another custom property
                //    $message->setCustomProperty('acme3', array('bing', 'bong'));

// Set the expiry value to 30 seconds
                $message->setExpiry(30);

// Add the message to the message queue
                $push->add($message);

// Send all messages in the message queue
                $push->send();
            } catch (\Exception $e) {
                // Логирование Ошибки
                Log::error('Уведомление пользователю не отправилось: ' . $e);
            }

        }
// Disconnect from the Apple Push Notification Service
        $push->disconnect();

// Examine the error message container
        $aErrorQueue = $push->getErrors();
        if (!empty($aErrorQueue)) {
            var_dump($aErrorQueue);
        }
    }

    public function activities()
    {
    	$bids = Bid::where('status',2)->count();
    	$orders = Order::where('status',3)->count();
    	return response()->json([
    		'response'=>[
    			'bids_work_count'=>$bids,
			    'orders_work_count'=>$orders
		    ]
	    ]);
    }
}
