<?php

namespace App\Http\Controllers\Bids;

use App\Classes\ApiError;
use App\Models\Alarm;
use App\Models\Bid;
use App\Models\BidResponse;
use App\Models\BidsChoose;
use App\Models\Branch;
use App\Models\PhoneSalt;
use App\Models\Product;
use App\Models\Service;
use App\Models\Review;
use App\Models\User;
use App\Traits\AlarmTrait;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BidsController extends Controller {

	use AlarmTrait;

	public function addBid( Request $request ) {

		$required_params = [ 'terms', 'service_id' ];

		$not_valid_param = $this->checkRequiredParams( $required_params );

		if ( isset( $not_valid_param ) ) {
			$err = new ApiError( 305, $not_valid_param );
			return $err->json();
		}

		$service_id = $request->input( 'service_id' );

		if ( ! Service::where( 'id', $service_id )->exists() ) {
			$err = new ApiError( 341, null, 'Выберите услугу', 'Нет услуги c id - ' . $service_id );
			return $err->json();
		}

		$bid = new Bid;

		if ( $request->exists( 'product_id' ) && $request->filled( 'product_id' ) ) {

			$prod_id = $request->input( 'product_id' );

			if ( ! Product::where( 'id', $prod_id )->exists() ) {
				$err = new ApiError( 342, null, 'Товар отсутсвует', 'Нет товара с id - ' . $prod_id );
				return $err->json();
			}

			$bid->product_id = $prod_id;
		}

		$user = $this->getCurrentUser( $request->input( 'auth_token' ) );


		$terms = $request->input( 'terms' );

		$bid->description = $request->input( 'description' );
		$bid->terms       = date( "Y-m-d H:i:s", $terms );
		$bid->user_id     = $user->id;

		$bids_choose = new BidsChoose;

		$bids_choose->service_id = $service_id;

		DB::transaction( function () use ( $bid, $bids_choose ) {

			$bid->save();
			$bids_choose->bid_id = $bid->id;
			$bids_choose->save();


		} );

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );


	}

	public function getBidsServices( Request $request ) {

		$required_params = [ 'bids_id' ];

		$not_valid_param = $this->checkRequiredParams( $required_params );

		if ( isset( $not_valid_param ) ) {
			$err = new ApiError( 305, $not_valid_param );
			return $err->json();
		}

		$bid_id = $request->input( 'bids_id' );

		$bids_bld = Bid::where( 'id', $bid_id );

		if ( ! $bids_bld->exists() ) {
			$err = new ApiError( 342, null, null, 'Заявка не найдена' );
			return $err->json();
		}

		$bid = $bids_bld->first();

		$services_bld = $bid->services();

		if ( $request->exists( 'type' ) && $request->filled( 'type' ) ) {

			$type = $request->input( 'type' );

			$services_bld = $services_bld->where( 'type', $type );

		}

		$services = $services_bld->orderBy( 'services.order', 'asc' )->get( [
			'services.order',
			'services.id',
			'services.name',
			'services.description',
			'services.type',
			'services.verification'
		] )->all();

		foreach ( $services as $service ) {
			unset( $service['pivot'] );
		}


		return response()->json( [

			'response' => $services,

		], 200, [], JSON_UNESCAPED_UNICODE );

	}

	public function cancelBid( Request $request ) {

		$required_params = [ 'bid_id' ];

		$not_valid_param = $this->checkRequiredParams( $required_params );

		if ( isset( $not_valid_param ) ) {
			$err = new ApiError( 305, $not_valid_param );
			return $err->json();
		}

		$bid_id = $request->input( 'bid_id' );

        $user = $this->getCurrentUser($request->input('auth_token'));
		$myBranches = $user->branches->pluck('id');
        $bid = Bid::where('id', $bid_id)->where(function ($q)use ($user,$myBranches){
        	$q->where('user_id', $user->id)->orWhereHas('bidResponses',function ($query)use ($myBranches){
        	    $query->whereIn('branch_ud',$myBranches)->where('status',1);
	        });
        })->first();

        if (!$bid) {
            $err = new ApiError(341,
                NULL,
                'Заявка не найдена',
                'нет такой заявки id - ' . $bid_id);
            return $err->json();
        }

        $bid->status = 5;
        $bid->save();

        // Если в отказ уходит исполнитель, пишем, че и как
        if($request->bid_response)
        {
        	$response = BidResponse::find($request->bid_response);
        	$response->comment = $request->comment;
        	$response->save();
        }

        return response()->json([

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );

	}

	public function editBid( Request $request ) {
		$required_params = [ 'bid_id' ];

		$not_valid_param = $this->checkRequiredParams( $required_params );

		if ( isset( $not_valid_param ) ) {
			$err = new ApiError( 305, $not_valid_param );
			return $err->json();
		}

		$user_id = $this->getCurrentUser( $request->input( 'auth_token' ) )->id;

		$bid_id = $request->input( 'bid_id' );

		$bid_bld = Bid::where( 'id', $bid_id );

		if ( ! $bid_bld->exists() ) {

			$err = new ApiError( 341, null, 'Заявка не найдена', 'нет такой заявки' );
			return $err->json();

		}

		$bid_bld = Bid::where( 'id', $bid_id )->where( 'user_id', $user_id );

		if ( ! $bid_bld->exists() ) {

			$err = new ApiError( 308 );
			return $err->json();

		}

		$bid = $bid_bld->first();

		unset( $bid_bld );

		if ( $request->has( 'terms' ) && $request->filled( 'terms' ) ) {

			$bid->terms = date( "Y-m-d H:i:s", $request->input( 'terms' ) );

		} elseif ( $request->has( 'status' ) && $request->filled( 'status' ) ) {

			if ( $bid->is_accepted !== 1 ) {

				$err = new ApiError( 344, null, 'Ваша заявка пока не прошла модерацию', 'Заявка не прошла модерацию, статус не может быть изменён' );
				return $err->json();

			}

			$bid->status = $request->input( 'status' );

		} elseif ( $request->has( 'description' ) && $request->filled( 'description' ) ) {

			$bid->description = $request->input( 'description' );

		} elseif ( $request->has( 'products_id' ) && $request->filled( 'products_id' ) ) {

			$prod_id = $request->input( 'products_id' );

			if ( ! Product::where( 'id', $prod_id )->exists() ) {
				$err = new ApiError( 343, null, 'Возникла ошибка ,мы просим прощения =)', 'Нет такого товара - ' . $prod_id );
				return $err->json();
			}

			$bid->product_id = $prod_id;

		} else {
			$err = new ApiError( 342, null, 'Возникла ошибка ,мы просим прощения =)', 'Один из необязательных параметров должен быть передан' );
			return $err->json();
		}

		$bid->save();

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );


	}

	public function moderateBid( Request $request ) {

		$validator = Validator::make( $request->all(), [
			'bid_id' => 'required|integer|exists:bids,id',
			'is_accepted' => 'required|integer|between:0,1',
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		$bid_bld = Bid::where( 'id', $request->input( 'bid_id' ) )->where( 'is_accepted', 2 );

		if ( ! $bid_bld->exists() ) {
			$err = new ApiError( 341, null, null, 'Заявка не найдена' );

			return $err->json();
		}

		$bid = $bid_bld->first();

		$bid->is_accepted = $request->input( 'is_accepted' );

		$bid->save();

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );

	}

	public function refreshBid( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'bid_id' => 'required|integer|exists:bids,id',
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		//Проверка прав
		$user = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$bid  = $user->bids()->where( 'id', $request->input( 'bid_id' ) )->where( function ( $query ) {
			$query->where( 'status', 3 )->orWhere( 'status', 5 )->orWhere( 'status', 4 );
		} )->first();

		if ( ! $bid ) {
			$err = new ApiError( 308 );
			return $err->json();
		}

		$bid->bidResponses()->delete(); // Удаляем отклики

		$bid->status = 1;
		$bid->save();

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function repeatBid( Request $request ) {

		$validator = Validator::make( $request->all(), [
			'bid_id' => 'required|integer|exists:bids,id',
			'terms' => 'required|integer',
		] );

		if ( $validator->fails() ) {

			$val_err = $validator->errors();

			$err = new ApiError( 299, null, null, $val_err->all() );

			return $err->json();

		}

		//Проверка прав
		$user = $this->getCurrentUser( $request->input( 'auth_token' ) );

		if ( $user->user_type !== 1 ) {
			$err = new ApiError( 308, null, "Только покупатель может выполнить это действие" );
			return $err->json();
		}

		$bid_bld = $user->bids()->where( 'id', $request->input( 'bid_id' ) )->where( function ( $query ) {
			$query->where( 'status', 3 )->orWhere( 'status', 5 )->orWhere( 'status', 4 );
		} );

		if ( ! $bid_bld->exists() ) {
			$err = new ApiError( 308 );
			return $err->json();
		}

		$bid = $bid_bld->first();

		if ( $bid->is_accepted === 0 && ( $bid->status === 4 || $bid->status === 3 ) ) {
			$err = new ApiError( 343, null, 'Заявка не принята модератором', 'Заявка не принята модератором' );
			return $err->json();
		}

		$newBid = new Bid;

		$terms = $request->input( 'terms' );

		if ( $terms <= time() ) {
			$err = new ApiError( 341, null, 'срок заявки должен быть позже текущего времени', 'срок заявки должен быть позже текущего времени' );
			return $err->json();
		}

		$newBid->description = $bid->description;
		$newBid->user_id     = $bid->user_id;


		$newBid->is_accepted = $bid->is_accepted === 1 ? 1 : 2;

		if ( isset( $bid->product ) ) {
			$newBid->product_id = $bid->product->id;
		}

		$newBid->terms = date( "Y-m-d H:i:s", $terms );

		$service_id = $bid->services()->first()->id;


		if ( $bid->status === 4 ) {

			$newBid->status = 2;

			$chosen_resp_bld = $bid->bidResponses()->where( 'status', 1 );

			if ( ! $chosen_resp_bld->exists() ) {
				$err = new ApiError( 342, null, 'Не выбран исполнитель', 'Не выбран исполнитель' );
				return $err->json();
			}

			$chosen_resp = $chosen_resp_bld->first();

			$newResp = new BidResponse;

			$newResp->branch_id     = $chosen_resp->branch_id;
			$newResp->price         = $chosen_resp->price;
			$newResp->quantity      = $chosen_resp->quantity;
			$newResp->quantity_type = $chosen_resp->quantity_type;
			$newResp->comment       = $chosen_resp->comment;
			$newResp->status        = 1;

		} elseif ( $bid->status === 3 ) {
			$newBid->status = 1;

			$newResp = false;
		} elseif ( $bid->status === 5 ) {

			$newBid->status = $bid->is_accepted === 1 ? 1 : 0;

			$newResp = false;

		}

		try {
			DB::transaction( function () use ( $newBid, $newResp, $service_id ) {

				$newBid->save();

				if ( $newResp !== false ) {
					$newResp->bid_id = $newBid->id;
					$newResp->save();
				}

				$newBid->services()->attach( $service_id );

			} );

		} catch ( QueryException $ex ) {
			$err = new ApiError( 310 );
			return $err->json();
		}

		$founder   = $bid->user;
		$city_id   = $founder->city_id;
		$receivers = Branch::where( 'city_id', $city_id )->get()->pluck( 'user_id' )->unique();
		$this->createAlarm( 'bid', $newBid, 'Была размещена повторная заявка', 'Была размещена повторная заявка', $receivers );

		return response()->json( [

			'response' => 1,

		], 200, [], JSON_UNESCAPED_UNICODE );

	}

	public function getPartnerBidsList( Request $request ) {
		$user = $this->getCurrentUser( $request->input( 'auth_token' ) );

		if ( $user->user_type != 2 ) {
			$err = new ApiError( 308, null, 'Данный метод недоступен для покупателя', null );
			return $err->json();
		}
		$validator = Validator::make( $request->all(), [
			'bids_status' => Rule::in( [ 1, 2, 3, 4, 5, 0, '' ] ),
			'didIAnswer' => 'boolean'
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}
		$mycities   = $user->branches->keyBy( 'city_id' )->keys();
		$mybranches = $user->branches->keyBy( 'id' )->keys();
		if ( $request->bids_status == 1 && ! $request->didIAnswer ) {
			$newBids  = Bid::where( 'is_private', '=', 0 )->where( 'is_accepted', '=', 1 )->where( 'status', '=', 1 )->whereHas( 'user', function ( $query ) use ( $mycities ) {
				$query->whereIn( 'city_id', $mycities );
			} );
			$response = $newBids->with( [ 'bidResponses.branch', 'services' ] )->get( [
				'id',
				'terms',
				'created_at',
				'updated_at',
				'status',
				'is_accepted',
				'description',
				'user_id'
			] );
		} elseif ( $request->bids_status == 1 && $request->didIAnswer == 1 ) {
			$candidateBids = Bid::where( 'is_private', '=', 0 )->where( 'is_accepted', '=', 1 )->where( 'status', '=', 1 )->whereHas( 'bidResponses', function ( $query ) use ( $mybranches ) {
				$query->whereIn( 'branch_id', $mybranches );
			} );
			$response      = $candidateBids->with( [ 'services', 'bidResponses' ] )->get( [
				'id',
				'terms',
				'created_at',
				'updated_at',
				'status',
				'is_accepted',
				'description',
				'user_id'
			] );
		} elseif ( $request->bids_status == 2 ) {
			$inJobBids = Bid::where( 'is_private', '=', 0 )->where( 'is_accepted', '=', 1 )->where( 'status', '=', 2 )->whereHas( 'bidResponses', function ( $query ) use ( $mybranches ) {
				$query->whereIn( 'branch_id', $mybranches )->where( 'status', '=', 1 );
			} );

			$response = $inJobBids->with( [ 'services', 'bidResponses' ] )->get( [
				'id',
				'terms',
				'created_at',
				'updated_at',
				'status',
				'is_accepted',
				'description',
				'user_id'
			] );

		} elseif ( $request->bids_status == 3 ) {
			$doneBids = Bid::where( 'is_private', '=', 0 )->where( 'is_accepted', '=', 1 )->where( 'status', '=', 3 )->whereHas( 'bidResponses', function ( $query ) use ( $mybranches ) {
				$query->whereIn( 'branch_id', $mybranches )->where( 'status', '=', 1 );
			} );

			$response = $doneBids->with( [ 'services', 'bidResponses' ] )->get( [
				'id',
				'terms',
				'created_at',
				'updated_at',
				'status',
				'is_accepted',
				'description',
				'user_id'
			] );
		} elseif ( $request->bids_status == 4 ) {
			$freezedBids = Bid::where( 'is_accepted', '=', 1 )->where( 'status', '=', 4 )->whereHas( 'bidResponses', function ( $query ) use ( $mybranches ) {
				$query->whereIn( 'branch_id', $mybranches )->where( 'status', '=', 1 );
			} );

			$response = $freezedBids->with( [ 'services', 'bidResponses' ] )->get( [
				'id',
				'terms',
				'created_at',
				'updated_at',
				'status',
				'is_accepted',
				'description',
				'user_id'
			] );
		} elseif ( $request->bids_status == 5 ) {
			$archiveBids = Bid::where( 'is_private', '=', 0 )->where( 'is_accepted', '=', 1 )->where( 'status', '=', 5 )->whereHas( 'bidResponses', function ( $query ) use ( $mybranches ) {
				$query->whereIn( 'branch_id', $mybranches );
			} );

			$canceledBids = Bid::where( 'is_private', '=', 0 )->where( 'is_accepted', '=', 1 )->whereHas( 'bidResponses', function ( $query ) use ( $mybranches ) {
				$query->whereIn( 'branch_id', $mybranches )->where( 'status', 2 );
			} );

			$response_1 = $archiveBids->with( [ 'services', 'bidResponses' ] )->get( [
				'id',
				'terms',
				'created_at',
				'updated_at',
				'status',
				'is_accepted',
				'description',
				'user_id'
			] );

			$response_2 = $canceledBids->with( [ 'services', 'bidResponses' ] )->get( [
				'id',
				'terms',
				'created_at',
				'updated_at',
				'status',
				'is_accepted',
				'description',
				'user_id'
			] );

			$response = $response_1->merge( $response_2 );

		} else {
			$response_1 = Bid::where( 'is_private', '=', 0 )->where( 'is_accepted', '=', 1 )->where( function ( $query ) use ( $mybranches, $mycities ) {
				$query->whereHas( 'bidResponses', function ( $query ) use ( $mybranches ) {
					$query->whereIn( 'branch_id', $mybranches );
				} );
			} )->with( [ 'services', 'bidResponses' ] )->get( [
				'id',
				'terms',
				'created_at',
				'updated_at',
				'status',
				'is_accepted',
				'description',
				'user_id'
			] );
			$response_2 = Bid::where( 'is_private', '=', 0 )->where( 'status', '=', 1 )->where( 'is_accepted', '=', 1 )->whereHas( 'user', function ( $query ) use ( $mycities ) {
				$query->whereIn( 'city_id', $mycities );
			} )->with( [ 'services', 'bidResponses' ] )->get( [
				'id',
				'terms',
				'created_at',
				'updated_at',
				'status',
				'is_accepted',
				'description',
				'user_id'
			] );


			$response = $response_1->merge( $response_2 );
		}
		$response = $response->sortByDesc( 'created_at' )->toArray();
		foreach ( $response as $i => &$bid ) {
			$bid_responses     = $bid['bid_responses'];
			$bid['didIAnswer'] = 0;
			$bid['bid_responses_count'] = count($bid['bid_responses']);
			foreach ( $bid_responses as $bid_response ) {
				if ( in_array( $bid_response['branch_id'], $mybranches->toArray() ) ) {
					$bid['didIAnswer'] = 1; // todo: Отрефакторить. слишком много циклов
					if ( $bid_response['status'] == 2 ) { // А если я отвечал и мне отказали
						if ( ! $request->bids_status || $request->bids_status == 5 ) {
							$bid['status'] = 5; // Он для меня архивный
						} else {
							unset( $response[ $i ] );
							continue;
						}
					}
				}

			}
			if ( $request->has( 'didIAnswer' ) ) {
				if ( $bid['didIAnswer'] != $request->didIAnswer ) {
					unset( $response[ $i ] );
					continue;
				}
			}
			try {
				$bid['service_id']   = $bid['services'][0]['id'];
				$bid['service_name'] = $bid['services'][0]['name'];
			} catch ( \Exception $e ) {

			}
			unset( $bid['bid_responses'] );
			unset( $bid['services'] );
		}

		return response()->json( [

			'response' => array_values( $response ),

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function getPartnerBid( Request $request ) {
		$user      = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$validator = Validator::make( $request->all(), [
			'bid_id' => 'required|integer|exists:bids,id',
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}
		$mybranches   = $user->branches->keyBy( 'id' )->keys();
		$bid          = Bid::find( $request->bid_id );
		$bidResponses = $bid->bidResponses->whereIn( 'branch_id', $mybranches );

		if ( $bid->status == 0 || ( $bid->status > 1 && $bidResponses->count() == 0 ) ) {
			$err = new ApiError( 308, null, 'Вы не имеете доступа к этой заявку', null );
			return $err->json();
		}

		$response = $bid->toArray(); // Возвращаем bid
		/* Но меняем ряд параметров */
		if ( $bidResponses->count() > 0 && $bidResponses->first()->status == 2 ) {

			$response['status'] = 5;
		}

		$response['didIAnswer'] = ( $bidResponses->count() > 0 ) ? 1 : 0;
		if ( $bidResponses->count() > 0 ) {
			$response['chosen_response_id'] = $bidResponses->first()->id;
		}
		$response['created_at']    = $bid->created_at;
		$response['updated_at']    = $bid->updated_at;
		$response['terms']         = $bid->terms;
		$response['is_review']     = ( $bidResponses->count() > 0 && Review::where( 'branch_id', $bidResponses->first()->id )->count() > 0 ) ? 1 : 0;
		$response['product_name']  = $bid->product_id ? $bid->product->name : null;
		$service                   = $bid->services()->first();
		$response['services_id']   = $service->id;
		$response['services_name'] = $service->name;
		$response['bid_responses_count'] = $bid->bidResponses->count();
		if ( $bid->user ) {
			$founder             = [
				'name' => $bid->user->name . ' ' . $bid->user->surname,
				'phone' => $bid->user->phoneSalt ? $bid->user->phoneSalt->phone : '',
				'email' => $bid->user->email,
				'city' => $bid->user->city ? $bid->user->city->name : '',
			];
			$response['founder'] = $founder;
		}
		return response()->json( [

			'response' => $response,

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function bidComplete( Request $request ) {
		$user      = $this->getCurrentUser( $request->input( 'auth_token' ) );
		$validator = Validator::make( $request->all(), [
			'bid_id' => [
				'required',
				'integer',
				Rule::exists( 'bids', 'id' )->where( function ( $query ) use ( $user ) {
					$query->where( [ 'user_id' => $user->id, 'is_buyer_complete' => 0, 'status' => 2 ] );
				} ),
			],
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}
		$bid                    = Bid::where( 'id', $request->bid_id )->first();
		$bid->is_buyer_complete = 1;
		if ( $bid->is_partner_complete == 1 ) {
			$bid->status = 3;
		}
		$bid->save();

		$bidResponse = $bid->bidResponses->where( 'status', '=', 1 )->first();

		if ( ! $bidResponse ) {
			return response()->json( [

				'response' => [ 'bid_status' => $bid->status, 'warning' => "Уведомление не создалось. BidResponse не найден." ],

			], 200, [], JSON_UNESCAPED_UNICODE );
		}

		$receivers = [ $bidResponse->branch->user->id ];
		$this->createAlarm( 'bid', $bid, 'Заявка была завершена покупателем', 'Заявка была завершена покупателем', $receivers );

		if ( $bid->status == 3 ) {
			$receivers = [ $bid->user_id ];
			$this->createAlarm( 'bid', $bid, 'Вам необходимо оставить отзыв партнеру', 'Вам необходимо оставить отзыв партнеру', $receivers );
		}

		return response()->json( [

			'response' => [ 'bid_status' => $bid->status ],

		], 200, [], JSON_UNESCAPED_UNICODE );
	}

	public function bidPartnerComplete( Request $request ) {
		$user = $this->getCurrentUser( $request->input( 'auth_token' ) );

		$validator = Validator::make( $request->all(), [
			'bid_id' => [
				'bail',
				'required',
				'integer',
				Rule::exists( 'bids', 'id' )->where( function ( $query ) {
					$query->where( [ 'is_partner_complete' => 0, 'status' => 2 ] );
				} ),
			],
		] );

		if ( $validator->fails() ) {
			$val_err = $validator->errors();
			$err     = new ApiError( 299, null, null, $val_err->all() );
			return $err->json();
		}

		$mybranches = $user->branches()->get()->keyBy( 'id' )->keys()->toArray();

		$bidresponses = BidResponse::where( [ 'bid_id' => $request->bid_id, 'status' => 1 ] )->whereIn( 'branch_id', $mybranches )->get();

		if ( $bidresponses->isEmpty() ) {
			$err = new ApiError( 308, null, null, [ 'error' => 'Вы не являетесь исполнителем по этой заявке' ] );
			return $err->json();
		}

		$bid                      = Bid::where( 'id', $request->bid_id )->first();
		$bid->is_partner_complete = 1;
		if ( $bid->is_buyer_complete == 1 ) {
			$bid->status = 3;
		}
		$bid->save();

		/* Раскидываем уведомления */
		$receivers = [ $bid->user_id ];
		$this->createAlarm( 'bid', $bid, 'Заявка была завершена партнером', 'Заявка была завершена партнером', $receivers );
		if ( $bid->status == 3 ) {
			$this->createAlarm( 'bid', $bid, 'Вам необходимо оставить отзыв партнеру', 'Вам необходимо оставить отзыв партнеру', $receivers );
		}

		return response()->json( [

			'response' => [ 'bid_status' => $bid->status ],

		], 200, [], JSON_UNESCAPED_UNICODE );
	}
}
