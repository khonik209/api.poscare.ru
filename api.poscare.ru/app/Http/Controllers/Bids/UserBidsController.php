<?php

namespace App\Http\Controllers\Bids;

use App\Classes\ApiError;
use App\Models\Bid;
use App\Models\BidReview;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserBidsController extends Controller
{

    public function getUserBidsList(Request $request)
    {

        $user = $this->getCurrentUserModel($request->input('auth_token'));

        $bids_bld = $user->bids();

        if ($request->has('bids_status') && $request->bids_status != '') {

            $bids_bld = $bids_bld->where('status', $request->bids_status);

        }

        $bids = $bids_bld->orderBy('created_at','desc')->get();


        foreach ($bids as &$bid) {

            $service = $bid->services->first();
	        $bid->bid_responses_count = $bid->bidResponses->count();
            $bid->service_id = isset($service) ? $service->id : NULL;
            $bid->service_name = isset($service) ? $service->name : NULL;
            $bid->last_refresh = [
            	'hours'=>Carbon::parse($bid->last_refresh)->hour,
            	'minutes'=>Carbon::parse($bid->last_refresh)->minute,
            ];
            unset($bid->services);
        }

        unset($bid);

        return response()->json([

            'response' => $bids ,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function getBid(Request $request)
    {

        $required_params = ['bid_id'];

        $not_valid_param = $this->checkRequiredParams($required_params);

        if (isset($not_valid_param)) {
            $err = new ApiError(305, $not_valid_param);
            return $err->json();
        }


        $bid_id = $request->input('bid_id');

        if (gettype(+$bid_id) !== "integer") {
            $err = new ApiError(307, 'bid_id');
            return $err->json();
        }

        $bid = Bid::where('id', $bid_id)->first();

        if (!$bid) {
            $err = new ApiError(341,
                NULL,
                'Заявка не найдена',
                'нет такой заявки');
            return $err->json();
        }

        /* Прошли валидацию, начинаем бизнес логику */

        $services_arr = $bid->services->first()->toArray();

        $product_bld = $bid->product;

        $bids_arr = $bid->toArray();

        $bids_arr['services_id'] = $services_arr['id'];
        $bids_arr['services_name'] = $services_arr['name'];
        $bids_arr['product_name'] = isset($product_bld) ? $product_bld->name : NULL;
        $bid_respone = $bid->bidResponses->where('status', 1)->first();
	    $bids_arr['bid_responses_count'] = $bid->bidResponses->count();
        if ($bid_respone) {
            $bids_arr['chosen_response_id'] = $bid_respone->id;
        }
	    $bids_arr['last_refresh'] = [
		    'hours'=>Carbon::parse($bid->last_refresh)->hour,
		    'minutes'=>Carbon::parse($bid->last_refresh)->minute,
	    ];
        return response()->json([

            'response' => $bids_arr,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

}
