<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use App\Classes\ApiError;
use App\PartnerSupply;
use App\Models\Service;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Traits\OrderTrait;

class PartnerController extends Controller
{
    public function addServices(Request $request)
    {

        $user = $this->getCurrentUserModel($request->input('auth_token'));
        $request->service_id = json_decode($request->service_id, true);

        $validator = Validator::make(['service_id' => $request->service_id], [
            'service_id' => 'array',
        ]);
        $validator->after(function ($validator) use ($request) {
            $services = Service::whereIn('id', $request->service_id)->get()->pluck('id');
            $service_id = collect($request->service_id);
            if ($services->count() != $service_id->count()) {
                $diff = $service_id->diff($services);
                $diff->map(function ($item, $key) use ($validator) {
                    $validator->errors()->add('service_id', 'Недопустимое значение - ' . $item);
                });
            }
        });
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());

            return $err->json();

        }
        if ($request->type == 0 || $request->type == 1) {
            $services_to_delete = $user->services->where('type', $request->type)->pluck('id');
            $user->services()->detach($services_to_delete);
        } else {
            $services_to_delete = $user->services->pluck('id');
            $user->services()->detach($services_to_delete);
        }
        $user->services()->attach($request->service_id);
        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }


    /**
     * Возвращает категории партнера
     * @return \Illuminate\Http\JsonResponse
     */
    public function partnerProductCategories(Request $request)
    {

        $categories_arr = ProductCategory::get(['id', 'name',]);

        $response = [
            [
                'id' => "",
                'name' => "Все товары",
            ]
        ];

        $total_count = 0;
        $user = $this->getCurrentUserModel($request->input('auth_token'));

        $products = $user->products;
        foreach ($categories_arr as $category) {
            $prod_count = $products->where('category_id', '=', $category->id)->count();

            $response[] = [
                'id' => $category->id,
                'name' => $category->name,
                'quantity' => $prod_count,
            ];

            $total_count += $prod_count;

        }

        $response[0]['quantity'] = $total_count;

        return response()->json([

            'response' => $response,

        ], 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Возвращает товары партнера
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function partnerProductList(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'category_id' => 'integer|nullable',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());

            return $err->json();

        }

        $user = $this->getCurrentUserModel($request->input('auth_token'));

        $products = $user->products->sortBy('order');

        if (isset($request->category_id) && $request->category_id != '') {
            $products = $products->where('category_id', '=', $request->category_id);
        }

        $products_arr = [];
        foreach ($products as $product) {
            $products_in_arr = [];

            //Получаем изображения
            $img_arr = [];

            $imgs = $product->images;

            if (isset($imgs)) {

                foreach ($imgs as $img) {

                    $img_in_arr = [];

                    $img_in_arr['img_id'] = $img->id;

                    $img_in_arr['img_href'] = $img->href;

                    array_push($img_arr, $img_in_arr);

                }

                $products_in_arr['images'] = $img_arr;

            }

            $products_in_arr['name'] = $product->name;

            $products_in_arr['id'] = $product->id;

            $products_in_arr['description'] = $product->description;

            $products_in_arr['category_id'] = $product->category_id;

            $products_in_arr['average_price'] = $product->average_price;

            $products_in_arr['quantity_in_unformed_order'] = 0;

            $count = 0;

            $products_in_arr['related_services'] = $product->relatedServices;

            //$products_in_arr['quantity_in_unformed_order'] = array_key_exists($product->id, $selected_products) ? $selected_products[$product->id]['quantity'] : 0;

            $products_in_arr['related_services_count'] = $count;
            array_push($products_arr, $products_in_arr);

        }
        return response()->json([

            'response' => $products_arr,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function addPartnerProduct(Request $request)
    {
        $user = $this->getCurrentUserModel($request->input('auth_token'));
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());

            return $err->json();

        }
        try {
            $user->products()->detach($request->product_id);
            $user->products()->attach($request->product_id);
        } catch (\Exception $e) {
            Log::error('Ошибка при добавлении товара партнеру: ' . $e);
        }

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function deletePartnerProduct(Request $request)
    {
        $user = $this->getCurrentUserModel($request->input('auth_token'));
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer',
        ]);
        if ($validator->fails()) {

            $val_err = $validator->errors();

            $err = new ApiError(299,
                NULL,
                NULL,
                $val_err->all());

            return $err->json();

        }

        try {
            $user->products()->detach($request->product_id);
        } catch (\Exception $e) {
            Log::error('Ошибка при удалении товара у партнера: ' . $e);
        }

        return response()->json([

            'response' => 1,

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }

}
