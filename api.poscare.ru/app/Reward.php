<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
	protected $hidden = [
		'created_at', 'updated_at'
	];

	public function users()
	{
		return $this->belongsToMany('App\Models\User', 'reward_user', 'reward_id', 'user_id')->withPivot('force');
	}

}
