<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $table = 'bids';

    protected $guarded = ['id'];

    public function services()
    {
        return $this->belongsToMany('App\Models\Service',
            'bid_service',
            'bid_id',
            'service_id');
    }

    public function user()
    {

        return $this->belongsTo('App\Models\User');

    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }

    public function bidResponses()
    {
        return $this->hasMany('App\Models\BidResponse','bid_id','id');
    }

    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->timestamp;
    }
    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->timestamp;
    }

    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getTermsAttribute($value) {
        return Carbon::parse($value)->timestamp;
    }
}
