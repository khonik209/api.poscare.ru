<?php

namespace App\Models;

use App\Classes\ApiError;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

class Order extends Model
{

    protected $guarded = ['id'];

  //  protected $table = 'orders';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function orderResponses()
    {
        return $this->hasMany('App\Models\OrderResponse');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product',
            'order_product')
            ->withPivot('quantity');
    }

    public function relatedServices()
    {
        return $this->hasMany('App\Models\OrderService');
    }

    public function items()
    {
        return $this->hasMany('App\Models\OrderItem', 'order_id');
    }

    public function paymentMethod()
    {
        return $this->hasOne('App\Models\PaymentMethod', 'id', 'payment_id');
    }

    public function delivery()
    {
        return $this->hasOne('App\Models\Delivery', 'id', 'delivery_id');
    }

    /*public function conditions()
    {
        return $this->hasMany('App\Models\OrderCondition', 'orders_id');
    }*/

    public function addResponse($usr_br_id, $quant, $quant_type, $comment, $price)
    {

        if($this->status !== 2)
            return new ApiError(341,
                NULL,
                "Нельзя ответить на заказ",
                "Нельзя ответить на заказ, статус - " . $this->status);

        $order_id = $this->id;

        $order_resp = new OrderResponse;

        $order_resp->branch_id = $usr_br_id;
        $order_resp->price = $price;
        $order_resp->quantity = $quant;
        $order_resp->quantity_type = $quant_type;
        $order_resp->comment = $comment;
        $order_resp->status = 0;
        $order_resp->order_id = $order_id;

        try {
            $order_resp->save();
        } catch (QueryException $ex) {
            return new ApiError(310);
        }

        return true;

    }

    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->timestamp;
    }
    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->timestamp;
    }

    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getTermsAttribute($value) {
        return Carbon::parse($value)->timestamp;
    }

}
