<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BranchImage extends Model
{
    //protected $table = 'users_branches_img';

    protected $guarded = ['id'];

    public function logo_href()
    {
        return $this->hasOne('App\Models\Image', 'id', 'image_id')->select('id', 'href');
    }
}
