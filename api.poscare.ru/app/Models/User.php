<?php

namespace App\Models;

use App\Classes\ApiError;
use App\RatingEvent;
use App\Reward;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Null_;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $guarded = ['id'];

    protected $table = 'users';

    public function phoneSalt()
    {
        return $this->belongsTo('App\Models\PhoneSalt', 'phone_id');
    }

    public function userOpt()
    {
        return $this->hasOne('App\Models\UserOpt', 'user_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }

    public function bids()
    {
        return $this->hasMany('App\Models\Bid', 'user_id');
    }

    public function branches()
    {
        return $this->hasMany('App\Models\Branch', 'user_id');
    }

    public function branchesWithCity()
    {
        return $this->hasMany('App\Models\Branch', 'user_id')->with('city', 'images');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'user_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Review', 'user_id');
    }

    public function tenders()
    {
        return $this->hasMany('App\Models\Tender');
    }

    public function isOwner(Model $model, $model_id, array $params = [])
    {

        $bild = $model::where('user_id', $this->id)->where('id', $model_id);

        if (isset($params)) {
            foreach ($params as $param) {

                if ($param['operand'] === null) {
                    $bild = $bild->where($param['name'], $param['value']);
                } else {
                    $bild = $bild->where($param['name'], $param['operand'], $param['value']);
                }
            }
        }

        return $bild->exists();


    }

    public function sales()
    {
        return $this->hasMany('App\Models\Sale', 'user_id');
    }

    public function alarms()
    {
        return $this->belongsToMany('App\Models\Alarm')->withPivot('status')->withTimestamps();
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'user_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_user', 'user_id', 'product_id');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service', 'service_user', 'user_id', 'service_id')->withPivot('verified');
    }

    public function rewards()
    {
        return $this->belongsToMany('App\Reward', 'reward_user')->withPivot('force');
    }

    public function myRating($event = 'none', $num = 0)
    {
        if ($event != 'none') {
            $checkEvent = RatingEvent::where('event', $event)->first();
            if ($checkEvent) {
                $rating = $checkEvent->score;

                $this->rating = $this->rating + $rating;
                $this->save();
            }
        }
        if ($num) {
            $this->rating = $num;
            $this->save();
        }
        $rewards = Reward::where('price', '<=', $this->rating)->get()->pluck('id');
        $user_reward = $this->rewards()->wherePivot('force', '=', 0)->get()->pluck('id');
        $this->rewards()->detach($user_reward);
        $this->rewards()->attach($rewards);
        return $this->rating;
    }

    public function getBirthdayAttribute($value)
    {
        return Carbon::parse($value)->timestamp;
    }

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = Carbon::createFromTimestamp($value)->format('Y-m-d H:i:s');
    }

    public function getCreatedAtAttribute($value)
    {
        if(!$value)
        {
            return Carbon::parse('2018-07-24 16:00')->timestamp;
        }
        return Carbon::parse($value)->timestamp;
    }

    public function getHeadAcceptedDateAttribute($value=null)
    {
        return Carbon::now()->timestamp;
        if(!$value)
        {

        }
        return Carbon::parse($value)->timestamp;
    }
}
