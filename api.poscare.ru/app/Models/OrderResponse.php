<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderResponse extends Model
{

    protected $guarded = ['id'];

    protected $table = 'order_responses';

    public $timestamps = false;
    
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    /*public function reviews()
    {
        return $this->hasMany('App\Models\Review')->where('is_accepted', 1);
    }*/
    
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
}
