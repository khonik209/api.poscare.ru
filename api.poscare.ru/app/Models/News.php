<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    //protected $table = 'news';

    protected $guarded = ['id'];

    public function images()
    {
        return $this->belongsToMany('App\Models\Image',
            'image_news');
    }

    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->timestamp;
    }
    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->timestamp;
    }

}
