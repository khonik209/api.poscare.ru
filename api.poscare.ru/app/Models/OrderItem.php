<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{

    protected $guarded = ['id'];

    protected $table = 'order_product';

    public $timestamps = false;

    public function relatedServices()
    {
        return $this->belongsToMany('App\Models\RelatedService',
            'order_related_service',
            'order_product_id',
            'related_service_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
    
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

}
