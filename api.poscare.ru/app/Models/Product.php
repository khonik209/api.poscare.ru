<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
	//  protected $table = 'products';

	protected $guarded = [ 'id' ];

	public function bids() {
		return $this->hasMany( 'App\Models\Bid' );
	}

	public function characters() {
		return $this->belongsToMany( 'App\Models\Character', 'character_product' )->withPivot( 'quantity' );
	}

	public function relatedServices() {
		return $this->belongsToMany( 'App\Models\RelatedService', 'product_related_service' )->orderBy( 'related_services.order', 'asc' );
	}

	public function category() {
		return $this->belongsTo( 'App\Models\ProductCategory' );
	}

	public function images() {
		return $this->belongsToMany( 'App\Models\Image', 'image_product' );
	}

	public function orders() {
		return $this->belongsToMany( 'App\Models\Order', 'order_product' );
	}

	public function users() {
		return $this->belongsToMany( 'App\Models\User', 'product_user' );
	}

	public function tenders() {
		return $this->belongsToMany( 'App\Models\Tender', 'product_tender' )->withPivot( 'quantity' )->withPivot('price')->withTimestamps();
	}
}
