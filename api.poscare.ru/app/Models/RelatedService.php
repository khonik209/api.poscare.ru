<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelatedService extends Model {

	//protected $table = 'related_services';

	protected $guarded = [ 'id' ];

	protected $hidden = array( 'pivot' );

	public function products() {
		return $this->belongsToMany( 'App\Models\Product', 'product_related_service', 'related_service_id', 'product_id' );
	}

	public function ordersItems() {
		return $this->belongsToMany( 'App\Models\OrderItem', 'order_related_service', 'related_service_id', 'order_product_id' )->withPivot('variant');
	}

	public function getVariantsAttribute( $value ) {
		if ( ! $value ) {
			return null;
		}
		return json_decode( $value );
	}

	public function setVariantsAttribute( $value ) {
		if ( ! $value ) {
			$this->attributes['variants'] = null;
		}
		$this->attributes['variants'] = json_encode( $value );
	}
}
