<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';

    protected $guarded = ['id'];

    public function bids()
    {
        return $this->belongsToMany('App\Models\Bid',
            'bid_service',
            'service_id',
            'bid_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User',
            'service_user',
            'service_id',
            'user_id')
	        ->withPivot('verified');
    }

	public function branches()
	{
		return $this->belongsToMany('App\Models\Branch', 'branch_service')->withPivot('price')->withPivot('quantity')->withPivot('quantity_type')->withTimestamps();
	}
}
