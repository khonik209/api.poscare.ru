<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tender extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function tenderResponses()
    {
        return $this->hasMany('App\Models\TenderResponse');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product',
            'product_tender')->withPivot('quantity')->withPivot('price')->withTimestamps();
    }

    public function getTermsAttribute($value)
    {
        if (!$value) {
            return null;
        }

        return Carbon::parse($value)->timestamp;
    }

    /*public function setTermsAttribute($value)
    {
        if (!$value) {
            $this->attributes['terms'] = null;
        }
        $this->attributes['terms'] = Carbon::createFromTimestamp($value)->format('Y-m-d H:i:s');
    }*/

    public function getCreatedAtAttribute($value)
    {
        if (!$value) {
            return null;
        }

        return Carbon::parse($value)->timestamp;
    }

    public function getUpdatedAtAttribute($value)
    {
        if (!$value) {
            return null;
        }

        return Carbon::parse($value)->timestamp;
    }

    /**
     * Список тендеров
     *
     * Статусы:
     *
     *  is_mine = 0
     *  null - все, кроме модерации
     *  0 - модерация
     *  1 - (didIAnswer = 0) - Новый
     *  1 - (didIAnswer = 1) - Участник
     *  2 - Исполнитель
     *  3 - Закрыт
     *  4 - Архив
     *
     */

    public function getGetStatusAttribute()
    {
        $status = $this->status;
        if ($status == 1) {
            return "Новый";
        } elseif ($status == 2) {
            return "Выбран исполнитель";
        } elseif ($status == 3) {
            return "Закрыт";
        } elseif ($status == 4) {
            return "В архиве";
        } else {
            return "Модерация";
        }
    }

    public function getGetModerationAttribute()
    {
        $is_accepted = $this->is_accepted;
        if ($is_accepted == 1) {
            return "Одобрено";
        } elseif ($is_accepted == 2) {
            return "Отклонено";
        } else {
            return "Новое";
        }
    }

    public function getGetPrivateAttribute()
    {
        $is_private = $this->is_private;
        return $is_private?"Приватный":"Общий";
    }

}
