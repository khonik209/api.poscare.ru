<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //  protected $table = 'images';

    protected $guarded = ['id'];

    public function userOpt()
    {
        return $this->hasOne('App\Models\UserOpt', 'image_id');
    }

    public function branches()
    {
        return $this->belongsToMany('App\Models\Branch',
            'branch_image'
        //       'image_id',
        //     'users_branches_id'
        );
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function sales()
    {
        return $this->belongsToMany('App\Models\Sale',
            'image_sale'
           // 'image_id',
           // 'sale_id'
        );
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product',
            'image_product'
           // 'image_id',
           // 'product_id'
        );
    }

    public function news()
    {
        return $this->belongsToMany('App\Models\News',
            'image_news'
           );
    }

}
