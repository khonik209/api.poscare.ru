<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BidsChoose extends Model
{
    protected $table = 'bid_service';

    protected $guarded = ['id'];
}
