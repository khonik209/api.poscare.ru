<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCharacter extends Model
{
    protected $table = 'character_product';

    protected $guarded = ['id'];
}
