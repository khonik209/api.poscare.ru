<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $guarded = ['id'];

    // protected $table = 'users_branches';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function bidResponses()
    {
        return $this->hasMany('App\Models\BidResponse');
    }

    public function orderResponses()
    {
        return $this->hasMany('App\Models\OrderResponse');
    }

    public function images()
    {
        return $this->belongsToMany('App\Models\Image', 'branch_image');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Review');
    }

    public function image()
    {
        return $this->hasMany('App\Models\BranchImage', 'branch_id', 'id')->select('id', 'image_id', 'branch_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service', 'branch_service')->withPivot('price')->withPivot('quantity')->withPivot('quantity_type')->withTimestamps();
    }

    public function tenders()
    {
        return $this->hasMany('App\Models\Tender');
    }

    public function tenderResponses()
    {
        return $this->hasMany('App\Models\TenderResponse');
    }
}
