<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenderResponse extends Model
{
    public function tender()
    {
        return $this->belongsTo('App\Models\Tender');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }
    public function getHumanTimeKeyAttribute()
    {
        if($this->time_key==1)
        {
            return "ч.";
        } elseif($this->time_key==2)
        {
            return "дн.";
        } elseif($this->time_key==3)
        {
            return "мес.";
        } else {
            return "мин.";
        }
    }
}
