<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Alarm extends Model
{
    //protected $table = 'alarms';

    //protected $guarded = ['id'];

    public function users()
    {

        return $this->belongsToMany('App\Models\User')->withPivot('status')->withTimestamps();

    }
    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->timestamp;
    }
    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->timestamp;
    }
}
