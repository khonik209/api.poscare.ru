<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{

    //  protected $table = 'sales';

    protected $guarded = ['id'];

    public function images()
    {
        return $this->belongsToMany('App\Models\Image',
            'image_sale');
    }

    public function cities()
    {
        return $this->belongsToMany('App\Models\City',
            'city_sale',
            'sale_id',
            'city_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->timestamp;
    }

    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->timestamp;
    }

    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getStartDateAttribute($value)
    {
        return Carbon::parse($value)->timestamp;
    }

    /**
     * Always capitalize the first name when we retrieve it
     */
    public function getEndDateAttribute($value)
    {
        return Carbon::parse($value)->timestamp;
    }
}
