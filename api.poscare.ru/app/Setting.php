<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public function getBotSettingsAttribute($value)
    {
        return json_decode($value);
    }

    public function setBotSettingsAttribute($value)
    {
        if ($value != null) {
            $this->attributes['bot_settings'] = json_encode($value);
        } else {
            $this->attributes['bot_settings'] = null;
        }
    }
}
