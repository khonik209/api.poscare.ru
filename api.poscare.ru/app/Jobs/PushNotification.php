<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class PushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $alarm;
    protected $receivers;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($alarm, $receivers)
    {
        $this->receivers = $receivers;
        $this->alarm = $alarm;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Instantiate a new ApnsPHP_Push object
        $push = new \ApnsPHP_Push(\ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, __DIR__ . '/ck.pem');

// Set the Provider Certificate passphrase
        $push->setProviderCertificatePassphrase(config('pushnotification.apn.passPhrase'));

// Set the Root Certificate Autority to verify the Apple remote peer
        $push->setRootCertificationAuthority(__DIR__ . '/entrust_root_certification_authority.pem');

// Connect to the Apple Push Notification Service
        $push->connect();

        foreach ($this->receivers as $receiver) {
            try {
                $user = User::find($receiver);
                $token = $user->phoneSalt->device_token;
                // Instantiate a new Message with a single recipient
                $message = new \ApnsPHP_Message($token);

// Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
// over a ApnsPHP_Message object retrieved with the getErrors() message.
                $message->setCustomIdentifier("Message-Badge-3");

// Set badge icon to "3"
                $newAlarmsCount = $user->alarms()->wherePivot('status', 'new')->count();
                $message->setBadge($newAlarmsCount);

// Set a simple welcome text
                $message->setText($this->alarm->title);

// Play the default sound
                $message->setSound();

// Set a custom property
                //           $message->setCustomProperty('acme2', array('bang', 'whiz'));

// Set another custom property
                //    $message->setCustomProperty('acme3', array('bing', 'bong'));

// Set the expiry value to 30 seconds
                $message->setExpiry(30);

// Add the message to the message queue
                $push->add($message);

// Send all messages in the message queue
                $push->send();
            } catch (\Exception $e) {
                // Логирование Ошибки
                Log::error('Уведомление пользователю не отправилось: ' . $e);
            }

        }
// Disconnect from the Apple Push Notification Service
        $push->disconnect();

// Examine the error message container
        $aErrorQueue = $push->getErrors();
        if (!empty($aErrorQueue)) {
            var_dump($aErrorQueue);
        }
    }
}
