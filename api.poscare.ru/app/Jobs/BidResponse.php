<?php

namespace App\Jobs;

use App\Models\Alarm;
use App\Models\Bid;
use App\Models\Branch;
use App\Models\User;
use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class BidResponse implements ShouldQueue {
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $bid;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct(Bid $bid ) {
		$this->bid = $bid;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle() {
		if ( $this->bid->status != 1 ) {
			exit;
		}
		$myUser          = User::find( 15 );
		$myBranches      = $myUser->branches->pluck( 'id' );
		$checkMyResponse = $this->bid->bidResponses->whereIn( 'branch_id', $myBranches )->count();
		if ( $checkMyResponse > 0 ) {
			exit;
		}

		$settings = Setting::first();
		if ( $settings ) {
			$botSettings = $settings->bot_settings;
		}

		$branch = Branch::find(27);
		/* Ищем подхожящих филиал */
		$city_id = $this->bid->user->city_id;
		$checkBranch  = Branch::where( 'user_id', '=', $myUser->id )->where( 'city_id', $city_id )->first();
		if ( $checkBranch ) {
			$branch = $checkBranch;
		}

		$quantity = 2;
		$service = $this->bid->services->first();
		$branch_service = $branch->services()->find($service->id);
		$checkQ = $branch_service?$branch_service->pivot->quantity:null;
		if($checkQ)
		{
			$quantity = $checkQ;
		}
		$checkQType = $branch_service?$branch_service->pivot->quantity_type:null;
		if($checkQType)
		{
			$quantity_type = $checkQType;
		}
		$price = $this->bid->services->sum('average_price');
		$checkPrice = $branch_service?$branch_service->pivot->price:null;
		if($checkPrice)
		{
			$price = $checkPrice;
		}
		$bid_response = new \App\Models\BidResponse();

		$bid_response->branch_id     = $branch->id;
		$bid_response->price         = $price;
		$bid_response->bid_id        = $this->bid->id;
		$bid_response->quantity      = $quantity;
		$bid_response->quantity_type = $quantity_type;
		$bid_response->comment       = isset( $botSettings ) ? $botSettings->bid_answer : 'Выполним Вашу заявку быстро и качественно';
		$bid_response->status        = 0;

		$bid_response->save();

		/* Раскидываем уведомления */
		$receivers        = [ $this->bid->user_id ];
		$alarm            = new Alarm();
		$alarm->object    = 'bid';
		$alarm->object_id = $this->bid->id;
		$alarm->title     = 'Поступило предложение на заявку';
		$alarm->text      = 'Поступило предложение на заявку';
		$alarm->save();

		$alarm->users()->attach( $receivers );

		$this->dispatch( new PushNotification( $alarm, $receivers ) );
	}
}
