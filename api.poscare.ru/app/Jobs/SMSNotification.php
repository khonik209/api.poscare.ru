<?php

namespace App\Jobs;

use App\Classes\ApiError;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use \Zelenin\SmsRu\Api as SMSApi;
use \Zelenin\SmsRu\Auth\ApiIdAuth as SMSApiIdAuth;
use \Zelenin\SmsRu\Entity\Sms;

class SMSNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $alarm;
    protected $receivers;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($alarm, $receivers)
    {
        $this->receivers = $receivers;
        $this->alarm = $alarm;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->receivers as $receiver) {
            try {
                $user = User::find($receiver);
                $phone = $user->phoneSalt->phone;

                $client = new SMSApi(new SMSApiIdAuth(env('SMS_API_ID', false)));

                $sms = new Sms($phone, $this->alarm->title);

                $response = $client->smsSend($sms);

                if ($response->code !== '100') {
                    $err = new ApiError(315, null, null, $response->getDescription());
                    Log::error('Error while sms notification: ' . $err);
                }

            } catch (\Exception $e) {
                Log::error('Error while sms notification: ' . $e);
            }
        }

    }
}
