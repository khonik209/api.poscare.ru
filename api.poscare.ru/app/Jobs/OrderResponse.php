<?php

namespace App\Jobs;

use App\Models\Alarm;
use App\Models\Branch;
use App\Models\Order;
use App\Models\User;
use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class OrderResponse implements ShouldQueue {
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	protected $order;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct(Order $order ) {
		$this->order = $order;

	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle() {
		if ( $this->order->status != 2 ) {
			exit;
		}
		$myUser          = User::find( 15 );
		$myBranches      = $myUser->branches->pluck( 'id' );
		$checkMyResponse = $this->order->orderResponses->whereIn( 'branch_id', $myBranches )->count();
		if ( $checkMyResponse > 0 ) {
			exit;
		}

		$settings = Setting::first();
		if ( $settings ) {
			$botSettings = $settings->bot_settings;
		}

		$price = 0;
		foreach ( $this->order->products as $product ) {
			$related_sum = 0;
			$ord_itm     = $this->order->items()->where( 'product_id', $product->id )->first();
			if ( $ord_itm ) {
				$related_sum = $ord_itm->relatedServices()->sum( 'average_price' );
			}
			$thisPrice    = $product->average_price;
			$thisQuantity = $product->pivot->quantity;
			$price        = $price + $thisPrice * $thisQuantity + $related_sum;

		}

		$branch_id = 27;
		/* Ищем подходящий филиал */
		$city_id = $this->order->city_id;
		$branch  = Branch::where( 'user_id', $myUser->id )->where( 'city_id', $city_id )->first();
		if ( $branch ) {
			$branch_id = $branch->id;
		} else {
			$city_id = $this->order->user->city_id;
			$branch  = Branch::where( 'user_id', $myUser->id )->where( 'city_id', $city_id )->first();
			if ( $branch ) {
				$branch_id = $branch->id;
			}
		}


		$order_resp = new \App\Models\OrderResponse();

		$order_resp->branch_id     = $branch_id;
		$order_resp->price         = $price;
		$order_resp->quantity      = 2;
		$order_resp->quantity_type = 2;
		$order_resp->comment       = isset( $botSettings ) ? $botSettings->order_answer : 'Выполним Ваш заказ быстро и качественно';
		$order_resp->status        = 0;
		$order_resp->order_id      = $this->order->id;
		$order_resp->save();

		$receivers = [ $this->order->user_id ];
		/* Раскидываем уведомления */
		$alarm            = new Alarm();
		$alarm->object    = 'order';
		$alarm->object_id = $this->order->id;
		$alarm->title     = 'Поступило предложение на заказ';
		$alarm->text      = 'Поступило предложение на заказ';
		$alarm->save();

		$alarm->users()->attach( $receivers );

		$this->dispatch( new PushNotification( $alarm, $receivers ) );
	}
}
