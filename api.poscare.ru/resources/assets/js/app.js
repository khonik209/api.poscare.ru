/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./plugins/bootstrap');
import VueRouter from 'vue-router';

Vue.use(VueRouter);
import moment from 'moment'

moment.locale('ru');
Vue.prototype.moment = moment;
import Users from './components/GetUsers.vue';
import User from './components/GetUser.vue';
import BranchEdit from './components/BranchEdit.vue';
import BranchCreate from './components/BranchCreate.vue';

import Lists from './components/Lists.vue'
import ListCategories from './components/lists/ListCategories';
import ListProducts from './components/lists/ListProducts';
import ListRelatedServices from './components/lists/ListRelatedServices';
import ListServices from './components/lists/ListServices';
import ListCharacters from './components/lists/ListCharacters';
import ListDeliveries from './components/lists/ListDeliveries';
import ListPayments from './components/lists/ListPayments';
import ListRewards from './components/lists/ListRewards';

import Moderation from './components/Moderation';
import ModerateBids from './components/moderate/ModerateBids';
import ModerateOrders from './components/moderate/ModerateOrders';
import ModerateReviews from './components/moderate/ModerateReviews';
import ModerateImages from './components/moderate/ModerateImages';
import ModerateSales from './components/moderate/ModerateSales';
import ModerateNews from './components/moderate/ModerateNews';
import ModerateTenders from './components/moderate/ModerateTenders';

import Bids from './components/GetBids';
import Orders from './components/GetOrders';

import SearchUser from './components/SearchUser';

Vue.component('get-users', Users);
Vue.component('get-user', User);
Vue.component('branch-edit', BranchEdit);
Vue.component('branch-create', BranchCreate);

Vue.component('lists', Lists);
Vue.component('list-categories', ListCategories);
Vue.component('list-products', ListProducts);
Vue.component('list-related-services', ListRelatedServices);
Vue.component('list-services', ListServices);
Vue.component('list-characters', ListCharacters);
Vue.component('list-deliveries', ListDeliveries);
Vue.component('list-payments', ListPayments);
Vue.component('list-rewards', ListRewards);

Vue.component('moderation', Moderation);
Vue.component('moderate-bids', ModerateBids);
Vue.component('moderate-orders', ModerateOrders);
Vue.component('moderate-reviews', ModerateReviews);
Vue.component('moderate-images', ModerateImages);
Vue.component('moderate-sales', ModerateSales);
Vue.component('moderate-news', ModerateNews);
Vue.component('moderate-tenders', ModerateTenders);

Vue.component('get-bids', Bids);
Vue.component('get-orders', Orders);

Vue.component('search-user', SearchUser);

const routes = [
    {path: '/categories', component: ListCategories},
    {path: '/characters', component: ListCharacters},
    {path: '/related_services', component: ListRelatedServices},
    {path: '/products', component: ListProducts},
    {path: '/services/', component: ListServices},
    {path: '/delivery', component: ListDeliveries},
    {path: '/payment', component: ListPayments},
    {path: '/rewards', component: ListRewards},
];
const router = new VueRouter({
    root:'/lists',
    routes,
    history:true,
    hashbang:false,
    linkActiveClass:'active'
});
router.mode='html5';
var get_users = new Vue({
    el: '#get-users',
});
var get_user = new Vue({
    el: '#get-user',
});
var create_branch = new Vue({
    el: '#create-branch',
});
var edit_branch = new Vue({
    el: '#edit-branch',
});
var lists = new Vue({
    router,
    el: '#vue-lists',
});

var moderate = new Vue({
    el: '#vue-moderation',
});

var get_bids = new Vue({
    el: '#get-bids'
});

var get_orders = new Vue({
    el: '#get-orders'
});

var search_user = new Vue({
    el: '#search-user'
});