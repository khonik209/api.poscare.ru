@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/news">Новости</a></li>
                    <li class="active">Создать новость</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Создать новость</div>
                    <div class="panel-body">
                        <form action="/news/create" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Название</label>
                                <input name="name" id="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="description">Текст</label>
                                <textarea name="description" id="description"
                                          class="form-control"></textarea>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-3">Добавить изображение</label>
                                <div class="col-sm-8">
                                    <input type="file" multiple name="images[]">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <button type="submit" class="btn btn-primary">Сохранить данные</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection