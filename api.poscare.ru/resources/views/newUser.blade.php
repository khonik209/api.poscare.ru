@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <ol class="breadcrumb">
                <li><a href="/">Главная</a></li>
                <li><a href="/user">Пользователи</a></li>
                <li class="active">Новый пользователь</li>
            </ol>
            <div class="panel panel-default">
                <div class="panel-heading">Новый пользователь</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="/user/new" role="form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-3 control-label">Пароль</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="password" name="password" placeholder="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">Имя</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" name="name" id="name"/>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="surname">Фамилия</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" name="surname" id="surname" />
                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="user_type">Тип</label>
                            <div class="col-sm-8">
                                <select id="user_type" name="user_type" class="form-control">
                                    <option value="1">Покупатель</option>
                                    <option value="2">Партнер</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="orgform">Форма</label>
                            <div class="col-sm-8">
                                <select id="orgform" name="orgform" class="form-control">
                                    <option value="1">Физ. лицо</option>
                                    <option value="2">Юр. лицо</option>
                                </select>
                            </div>
                        </div>

                        <div class="js-u-form hidden">
                            <h4>О компании</h4>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="short_name">Сокращенное название</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="short_name" name="short_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="full_name">Полное название</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="full_name" name="full_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inn">ИНН</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="inn" name="inn"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="description">Описание</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="description" name="description"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="address">Адрес</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="address" id="address"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="open_hours_from">Время работы с</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="open_hours_from" id="open_hours_from"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="open_hours_to">Время работы до</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="open_hours_to" id="open_hours_to"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="site">URL</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="site" name="site"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="role">Роль</label>
                            <div class="col-sm-8">
                                <select id="role" name="role" class="form-control">
                                    <option value="user">Пользователь</option>
                                    <option value="manager">Менеджер</option>
                                    <option value="admin">Администратор</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Сохранить данные</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    @push('scripts')
        <script>
            $('#orgform').change(function () {
               if($(this).val()==='2')
               {
                   $('.js-u-form').removeClass('hidden')
               } else {
                   $('.js-u-form').addClass('hidden')
               }
            });
        </script>
    @endpush
@endsection
