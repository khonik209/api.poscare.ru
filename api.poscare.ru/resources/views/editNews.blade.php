@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/news">Новости</a></li>
                    <li class="active">{{$news->name}}</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">{{$news->name}}</div>
                    <div class="panel-body">
                        <form action="/news/edit" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="news_id" value="{{$news->id}}">
                            <div class="form-group">
                                <label for="name">Название</label>
                                <input name="name" id="name" class="form-control" value="{{$news->name}}">
                            </div>
                            <div class="form-group">
                                <label for="description">Текст</label>
                                <textarea name="description" id="description"
                                          class="form-control">{{$news->description}}</textarea>
                            </div>
                            <hr>
                            @if($news->images->count()>0)
                                @foreach($news->images as $image)
                                    <div class="form-group">
                                        <label>
                                            <img src="{{$image->href}}" class=" col-sm-3 img">
                                            <input type="checkbox" value="{{$image->id}}" name="to_delete_images[]">
                                            Удалить
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                            <div class="form-group">
                                <label class="col-sm-3">Добавить изображение</label>
                                <div class="col-sm-8">
                                    <input type="file" multiple name="images[]">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <button type="submit" class="btn btn-primary">Сохранить данные</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection