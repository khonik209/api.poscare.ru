@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/user">Пользователи</a></li>
                    <li><a href="/user/get/{{$branch->user->id}}">{{$branch->user->name.' '.$branch->user->surname}}</a></li>
                    <li class="active">{{$branch->name}}</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">{{$branch->name}}</div>

                    <div class="panel-body" id="edit-branch">
                        <branch-edit :branch="{{$branch}}"></branch-edit>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
