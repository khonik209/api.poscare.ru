@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/moderation">Модерация</a></li>
                    <li class="active">Новое предложение</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Новое предложение</div>

                    <div class="panel-body" id="create-branch">
                        <form class="form-horizontal" method="post" action="/moderation/order/offer" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_id" value="{{$order->id}}">
                            <div class="form-group">
                                <label for="comment" class="col-sm-3 control-label">Комментарий</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="comment" name="comment"
                                              placeholder="comment"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="quantity" class="col-sm-3 control-label">Срок</label>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" id="quantity" name="quantity"
                                           placeholder="Срок">
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control" id="quantity_type" name="quantity_type">
                                        <option value="1">часов</option>
                                        <option value="2">дней</option>
                                        <option value="3">месяцев</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="price">Цена</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="price" id="price"/>
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="branch">Филиал</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="branch" name="branch_id">
                                        @foreach($user->branches as $branch)
                                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="col-sm-offset-3 checkbox">
                                <label class="control-label" for="private">
                                    <input type="checkbox" name="is_private" value="1" id="private">
                                    Скрыть от других пользователей
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary">Сделать предложение</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
