@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Тендеры</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <a href="/tenders/{{$tender->id}}/edit" class="btn btn-default btn-xs pull-right">
                            Редактировать
                        </a>
                        Тендер {{$tender->id}}
                    </div>

                    <div class="panel-body" id="get-tenders">
                        <h4>Информация</h4>
                        {{$tender->description}}
                        <ul>
                            <li>
                                ID <strong>{{$tender->id}}</strong>
                            </li>
                            <li>
                                Создатель
                                <strong>
                                    @if($tender->user)
                                        <a href="{{url('/user/get/'.$tender->user->id)}}">{{$tender->user->name}}</a>
                                    @else
                                        Пользователь не определён
                                    @endif
                                </strong>

                            </li>
                            <li>
                                Срок:
                                <strong>{{\Carbon\Carbon::createFromTimestamp($tender->terms)->format('d.m.Y H:i')}}</strong>
                            </li>
                            <li>
                                Статус: <strong>{{$tender->get_status}}</strong>
                            </li>
                            <li>
                                Модерация: <strong>{{$tender->get_moderation}}</strong>
                            </li>
                            <li>
                                Приват: <strong>{{$tender->get_private}}</strong>
                            </li>
                            <li>
                                Победитель:
                                <strong>{{$tender->branch?$tender->branch->user->name:'Не определен'}}</strong>
                            </li>
                        </ul>
                        <hr>
                        <h4>Товары</h4>
                        @if($tender->products->count()>0)
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tbody>
                                    @foreach($tender->products()->get() as $product)
                                        <tr>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->pivot->quantity}} шт.</td>
                                            <td>{{$product->pivot->price}} руб.</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p><strong>К тендеру не прикреплены товары</strong></p>
                        @endif
                        <hr>
                        <h4>Ответы</h4>
                        @if($tender->tenderResponses->count()>0)
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>Создатель</td>
                                        <td>Цена</td>
                                        <td>Время</td>
                                        <td>Комментарий</td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tender->tenderResponses as $response)
                                        <tr>
                                            <td>
                                                {{$response->id}}
                                            </td>
                                            <td>
                                                {{$response->branch->user->name}}
                                            </td>
                                            <td>
                                                {{$response->price}}
                                            </td>
                                            <td>
                                                {{$response->time_value}} {{$response->human_time_key}}
                                            </td>
                                            <td>
                                                {{$response->comment}}
                                            </td>
                                            <td>
                                                <form action="/tenders/response/delete" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="tender_response_id"
                                                           value="{{$response->id}}">
                                                    <button class="btn btn-xs btn-danger" type="submit">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="alert alert-info">Ответы еще не созданы</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
