@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Тендеры</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Создание тендера
                    </div>

                    <div class="panel-body" id="get-tenders">
                        <form class="form-horizontal" action="{{url('tenders/store')}}" method="post">
                            {{csrf_field()}}
                            <h4>Общая информация</h4>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="terms">Срок</label>
                                <div class="col-sm-8">
                                    <input type="datetime-local" class="form-control" name="terms"
                                           value="{{old('terms')}}" id="terms">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="description">Описание</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="description"
                                              name="description">{{old('description')}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="status">Статус</label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control" id="status">
                                        <option value="0" @if(!old('status')) selected @endif>Модерация</option>
                                        <option value="1" @if(old('status')==1) selected @endif>Новый</option>
                                        <option value="2" @if(old('status')==2) selected @endif>Выбран исполнитель
                                        </option>
                                        <option value="3" @if(old('status')==3) selected @endif>Закрыт</option>
                                        <option value="4" @if(old('status')==4) selected @endif>В архиве</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="is_accepted">Модерация</label>
                                <div class="col-sm-8">
                                    <select name="is_accepted" class="form-control" id="is_accepted">
                                        <option value="0" @if(!old('is_accepted')) selected @endif>Новый</option>
                                        <option value="1" @if(old('is_accepted')==1) selected @endif>Одобрен</option>
                                        <option value="2" @if(old('is_accepted')==2) selected @endif>Отклонен</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="is_private">Приватность</label>
                                <div class="col-sm-8">
                                    <select name="is_private" class="form-control" id="is_private">
                                        <option value="0" @if(!old('is_private')) selected @endif>Общий</option>
                                        <option value="1" @if(old('is_private')==1) selected @endif>Приватный</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <h4>Товары:</h4>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-condensed table-hover">
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>
                                                {{$product->name}}
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input placeholder="Кол-во" type="number" class="form-control"
                                                           name="products[{{$product->id}}][quantity]"
                                                           value="{{old("products[$product->id][quantity]")}}">
                                                    <span class="input-group-addon">шт.</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input placeholder="Цена" type="number" class="form-control"
                                                           name="products[{{$product->id}}][price]"
                                                           value="{{old("products[$product->id][price]")}}">
                                                    <span class="input-group-addon">руб.</span>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button class="btn btn-primary btn-block" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
