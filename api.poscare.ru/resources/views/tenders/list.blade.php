@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Тендеры</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Тендеры
                        <a class="btn btn-info btn-xs pull-right" href="{{url('/tenders/create')}}">Создать тендер</a>
                    </div>

                    <div class="panel-body" id="get-tenders">
                        @if($tenders->count()>0)
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>Создатель</td>
                                        <td>Срок</td>
                                        <td>Статус</td>
                                        <td>Модерация</td>
                                        <td>Описание</td>
                                        <td>Приват</td>
                                        <td>Победитель</td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tenders as $tender)
                                        <tr>
                                            <td>
                                                <a href="/tenders/{{$tender->id}}">
                                                    {{$tender->id}}
                                                </a>
                                            </td>
                                            <td>
                                                {{$tender->user->name}}
                                            </td>
                                            <td>
                                                <a href="/tenders/{{$tender->id}}">
                                                    {{\Carbon\Carbon::createFromTimestamp($tender->terms)->format('d.m.Y H:i')}}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="/tenders/{{$tender->id}}">
                                                    {{$tender->get_status}}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="/tenders/{{$tender->id}}">
                                                    {{$tender->get_moderation}}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="/tenders/{{$tender->id}}">
                                                    {{$tender->description}}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="/tenders/{{$tender->id}}">
                                                    {{$tender->get_private}}
                                                </a>
                                            </td>
                                            <td>
                                                {{$tender->branch?$tender->branch->user->name:'Не определен'}}
                                            </td>
                                            <td>
                                                <form action="/tenders/delete" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="tender_id" value="{{$tender->id}}">
                                                    <button class="btn btn-xs btn-danger" type="submit">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="alert alert-info">Тендеры еще не созданы</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
