@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <ol class="breadcrumb">
                <li><a href="/">Главная</a></li>
                <li  class="active">Заказы</li>
            </ol>
            <div class="panel panel-default">
                <div class="panel-heading">Заказы</div>

                <div class="panel-body" id="get-orders">
                    <get-orders></get-orders>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
