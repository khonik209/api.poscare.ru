@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/bids">Заявки</a></li>
                    <li class="active">{{$bid->services->first()->name}}</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{$bid->services->first()->name}}
                        <a class="pull-right btn btn-primary btn-xs" href="/moderation/bid/offer/{{$bid->id}}"> Сделать предложение </a>
                    </div>

                    <div class="panel-body" id="get-user">
                        <form action="/bids/getOne/edit" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$bid->id}}">
                            <div class="form-group">
                                <label for="product" class="control-label">Товар</label>
                                <select name="product_id" id="product" class="form-control">
                                    @foreach($products as $product)
                                        <option value="{{$product->id}}"
                                                @if($product->id==$bid->product_id) selected @endif>{{$product->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="service" class="control-label">Услуга</label>
                                <select name="service_id" id="service" class="form-control">
                                    @foreach($services as $service)
                                        <option value="{{$service->id}}"
                                                @if($bid->services()->find($service->id)) selected @endif>{{$service->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description" class="control-label">Описание</label>
                                <input name="description" id="description" class="form-control"
                                       value="{{$bid->description}}">
                            </div>
                            <div class="form-group">
                                <label for="terms" class="control-label">Срок</label>
                                <input name="terms" id="terms" class="form-control"
                                       value="{{\Carbon\Carbon::createFromTimestamp($bid->terms)->format('Y-m-d H:i')}}">
                            </div>
                            <div class="form-group">
                                <label for="status" class="control-label">Статус</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="0" @if($bid->status==0) selected @endif>Модерация</option>
                                    <option value="1" @if($bid->status==1) selected @endif>Новая</option>
                                    <option value="2" @if($bid->status==2) selected @endif>В работе</option>
                                    <option value="3" @if($bid->status==3) selected @endif>Выполнена</option>
                                    <option value="4" @if($bid->status==4) selected @endif>Заявка закреплена</option>
                                    <option value="5" @if($bid->status==5) selected @endif>В архиве</option>
                                </select>
                            </div>
                            <button class="btn btn-primary">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
