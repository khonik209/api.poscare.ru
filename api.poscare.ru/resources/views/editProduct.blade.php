@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/lists">Справочник</a></li>
                    <li class="active">{{$product->name}}</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">{{$product->name}}</div>

                    <div class="panel-body" id="create-branch">
                        <form class="form-horizontal" method="post" action="/lists/product/edit" role="form"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$product->id}}">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Название</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="name"
                                           value="{{$product->name}}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label">Описание</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="description" name="description"
                                              placeholder="description">{{$product->description}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="average_price">Среднаяя цена</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="average_price" id="average_price"
                                           value="{{$product->average_price}}"/>
                                    @if ($errors->has('average_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('average_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="category">Категория</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="category" id="category">
                                        @foreach($categories as $category)
                                            <option @if($product->category_id == $category->id) selected
                                                    @endif value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="characters">Характеристики</label>
                                <div class="col-sm-8">
                                    <table>
                                        @foreach($characters as $key=>$character)
                                            <tr>

                                                <td>
                                                    <label>
                                                        <input type="checkbox" name="characters[]"
                                                               @if($product->characters()->find($character->id)) checked
                                                               @endif
                                                               id="character{{$key}}" class="char-box"
                                                               value="{{$character->id}}">
                                                        {{$character->name}}
                                                    </label>
                                                </td>

                                                <td>
                                                    <label class="sr-only"
                                                           for="character{{$key}}"> {{$character->name}}</label>
                                                    <input type="number" class="form-control char-inp"
                                                           id="value{{$key}}"
                                                           @if(!$product->characters()->find($character->id)) disabled
                                                           @endif
                                                           name="values[]"
                                                           value="{{$product->characters()->find($character->id)->pivot->quantity or null}}"
                                                           placeholder="Введите число">
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="services">Сопутствующие услуги</label>
                                <div class="col-sm-8">
                                    @foreach($relatedServices as $service)
                                        <div class="form-inline">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="services[]"
                                                           @if($product->relatedServices()->find($service->id)) checked
                                                           @endif
                                                           value="{{$service->id}}"> {{$service->name}}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="related_products">Добавить из товаров</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="related_products" name="related_products[]" multiple>
                                        @foreach($products as $related_product)
                                            <option value="{{$related_product->id}}"
                                                    @if($product->relatedServices->where('product_id',$related_product->id)->first()) selected @endif>{{$related_product->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr>
                            @if($product->images->count()>0)
                                @foreach($product->images as $image)
                                    <div class="form-group">
                                        <label>
                                            <img src="{{$image->href}}" class=" col-sm-3 img">
                                            <input type="checkbox" value="{{$image->id}}" name="to_delete_images[]">
                                            Удалить
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Добавить изображение</label>
                                <div class="col-sm-8">
                                    <input type="file" multiple name="images[]">
                                </div>
                            </div>
                            <hr>
                            <div class="col-sm-offset-3">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="is_actual" value="1" @if($product->is_actual) checked @endif>
                                        Актуален к продаже
                                    </label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Сохранить данные</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @push('scripts')
            <script>
                $('.char-box').change(function () {
                    $('.char-box').each(function (index, element) {
                        if ($(this).prop("checked")) {
                            $('#value' + index).prop('disabled', false)
                        } else {
                            $('#value' + index).prop('disabled', true)
                        }
                    })
                });

            </script>
        @endpush
    </div>
@endsection
