@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/orders">Заказы</a></li>
                    <li class="active">Новый заказ</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Создать заказ</div>

                    <div class="panel-body" id="get-user">
                        <form action="/orders/new" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="" class="control-label">

                                </label>
                                <input class="form-control" name="" id="">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
