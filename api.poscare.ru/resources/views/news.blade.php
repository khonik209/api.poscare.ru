@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Новости</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Новости
                        <a href="/news/create" class="pull-right btn btn-primary btn-xs">Добавить новость</a>
                    </div>

                    <div class="panel-body" id="get-orders">
                        @foreach($news_arr as $news)
                            <div class="media">
                                <a class="pull-left" href="/news/get/{{$news->id}}">
                                    <img class="media-object" src="{{$news->images->first()->href or '#'}}" alt="..."
                                         style=" max-width: 100px;">
                                </a>
                                <div class="media-body">
                                    <p>ID: <strong>{{$news->id}}</strong></p>
                                    <p>{{\Carbon\Carbon::createFromTimestamp($news->created_at)->format('d.m.Y H:i')}}</p>
                                    <h4 class="media-heading">
                                        <a href="/news/get/{{$news->id}}">
                                            {{$news->name}}
                                        </a>
                                    </h4>
                                    {{$news->description}}
                                </div>
                                <form action="/news/delete" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="news_id" value="{{$news->id}}">
                                    <button class="pull-right btn btn-danger btn-xs">Удалить</button>
                                </form>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
