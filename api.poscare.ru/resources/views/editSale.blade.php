@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/sales">Акции</a></li>
                    <li class="active">{{$sale->name}}</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">{{$sale->name}}</div>
                    <div class="panel-body">
                        <form action="/sales/edit" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="sale_id" value="{{$sale->id}}">
                            <div class="form-group">
                                <label for="name">Название</label>
                                <input name="name" id="name" class="form-control" value="{{$sale->name}}">
                            </div>
                            <div class="form-group">
                                <label for="description">Текст</label>
                                <textarea name="description" id="description"
                                          class="form-control">{{$sale->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="start_date">Начало акции</label>
                                <input name="start_date" id="start_date" value="{{\Carbon\Carbon::createFromTimestamp($sale->start_date)->format('Y-m-d H:i')}}"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="end_date">Конец акции</label>
                                <input name="end_date" id="end_date" value="{{\Carbon\Carbon::createFromTimestamp($sale->end_date)->format('Y-m-d H:i')}}"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="user_type">Тип пользователя</label>
                                <select class="form-control" id="user_type" name="user_type">
                                    <option @if($sale->user_type==1) selected @endif value="1">Покупатель</option>
                                    <option @if($sale->user_type==2) selected @endif value="2">Партнер</option>
                                </select>
                            </div>
                            <hr>
                            @if($sale->images->count()>0)
                                @foreach($sale->images as $image)
                                    <div class="form-group">
                                        <label>
                                            <img src="{{$image->href}}" class=" col-sm-3 img">
                                            <input type="checkbox" value="{{$image->id}}" name="to_delete_images[]">
                                            Удалить
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                            <div class="form-group">
                                <label class="col-sm-3">Добавить изображение</label>
                                <div class="col-sm-8">
                                    <input type="file" multiple name="images[]">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <button type="submit" class="btn btn-primary">Сохранить данные</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection