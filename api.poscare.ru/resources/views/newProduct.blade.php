@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/lists">Справочник</a></li>
                    <li class="active">Новый товар</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Новый товар</div>

                    <div class="panel-body" id="create-branch">
                        <form class="form-horizontal" method="post" action="/lists/product/create" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Название</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="name">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label">Описание</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="description" name="description"
                                              placeholder="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="average_price">Среднаяя цена</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="average_price" id="average_price"/>
                                    @if ($errors->has('average_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('average_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="category">Категория</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="category" id="category">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Добавить изображение</label>
                                <div class="col-sm-8">
                                    <input type="file" multiple name="images[]">
                                </div>
                            </div>
                            <div class="col-sm-offset-3">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="is_actual" value="1">
                                        Актуален к продаже
                                    </label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Сохранить данные</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
