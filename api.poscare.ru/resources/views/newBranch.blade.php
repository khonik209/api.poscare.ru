@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/user">Пользователи</a></li>
                    <li><a href="/user/get/{{$user->id}}">{{$user->name.' '.$user->surname}}</a></li>
                    <li class="active">Новый филиал</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Новый филиал</div>

                    <div class="panel-body" id="create-branch">
                        <branch-create :user_id="{{$user->id}}"></branch-create>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
