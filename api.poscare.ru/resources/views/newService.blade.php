@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/lists">Справочник</a></li>
                    <li class="active">Новый товар</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Новая услуга</div>

                    <div class="panel-body" id="create-branch">
                        <form class="form-horizontal" method="post" action="/lists/service/create" role="form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Название</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="name">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label">Описание</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="description" name="description"
                                              placeholder="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="average_price" class="col-sm-3 control-label">Средняя цена</label>
                                <div class="col-sm-8">
                                    <input class="form-control" id="average_price" name="average_price" placeholder="Average price">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="terms" class="col-sm-3 control-label">Время выполнения</label>
                                <div class="col-sm-8">
                                    <input max="99" min="0" class="form-control" id="quantity" name="quantity" type="number">
                                    <select class="form-control" id="quantity_type" name="quantity_type">
                                        <option value="1">часов</option>
                                        <option value="2">дней</option>
                                        <option value="3">месяцев</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="type" class="col-sm-3 control-label">Тип</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="type" name="type">
                                        <option value="0">Услуга</option>
                                        <option value="1">Сервис</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Сохранить данные</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
