@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/lists">Справочник</a></li>
                    <li class="active">Новая награда</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Новая награда</div>

                    <div class="panel-body" id="create-branch">
                        <form class="form-horizontal" method="post" action="/lists/reward/create" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="text" class="col-sm-3 control-label">Текст</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="text" name="text" placeholder="text">
                                    @if ($errors->has('text'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="price" class="col-sm-3 control-label">Количество баллов</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="price" name="price" placeholder="price">
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image" class="col-sm-3 control-label">Изображение</label>
                                <div class="col-sm-8">
                                    <input type="file" id="image" name="image" >
                                    @if ($errors->has('file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Сохранить данные</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
