@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/user">Пользователи</a></li>
                    <li class="active">{{$user->name.' '.$user->surname}}</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">{{$user->name.' '.$user->surname}}</div>

                    <div class="panel-body" id="get-user">
                        <get-user :user="{{$user}}" :rewards="{{$rewards}}"></get-user>

                        <form action="/user/delete" method="post" class="form-inline">
                            {{csrf_field()}}
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <button class="btn btn-danger pull-right" type="submit">Удалить пользователя</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
