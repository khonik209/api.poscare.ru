@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Настройки</div>

                    <div class="panel-body" id="settings">
                        <h4>Настройки бота</h4>
                        <div class="alert alert-info">
                            Подсказка
                            <ul>
                                <li>Текст ответа заявки</li>
                                <li>Текст ответа заказа</li>
                                <li>Время срабатывания</li>
                                <li>Среднее время поставки (Кол-во и Час, День, Месяц)</li>
                            </ul>
                        </div>
                        <form role="form" action="{{route('settings.store')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="settings" value="bot">
                            <div class="form-group">
                                <label for="bid_answer" class="col-sm-3">Ответ на заявку</label>
                                <div class="col-sm-9">
                                    <input type="text" name="bid_answer" id="bid_answer" class="form-control"
                                           value="{{$settings&&$settings->bot_settings?$settings->bot_settings->bid_answer:''}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="order_answer" class="col-sm-3">Ответ на заказ</label>
                                <div class="col-sm-9">
                                    <input type="text" name="order_answer" id="order_answer" class="form-control"
                                           value="{{$settings&&$settings->bot_settings?$settings->bot_settings->order_answer:''}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bot_timeout" class="col-sm-3">Время срабатывания</label>
                                <div class="col-sm-9">
                                    <input type="time" name="bot_timeout" id="bot_timeout" class="form-control"
                                           value="{{$settings&&$settings->bot_settings?gmdate("i:s", $settings->bot_settings->bot_timeout):''}}">
                                </div>
                            </div>
                            <hr>
                            <h4>Rating Event</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    @foreach($ratings as $key=>$rating)
                                        <tr>
                                            <td>{{$rating->description}}</td>
                                            <td>{{$rating->event}}</td>
                                            <td>
                                                <input class="form-control" type="number"
                                                       name="ratings[{{$rating->id}}]"
                                                       value="{{$rating->score}}">
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <button class="btn btn-primary">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
