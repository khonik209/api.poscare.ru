@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Модерация</div>

                    <div class="panel-body" id="vue-moderation">
                        @include('common.error')
                        @include('common.customError')
                        @include('common.info')
                        @include('common.success')
                       <moderation></moderation>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
