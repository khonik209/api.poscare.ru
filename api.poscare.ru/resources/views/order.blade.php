@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/orders">Заказы</a></li>
                    <li class="active">{{$order->id}}</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{$order->id}}
                        <a class="pull-right btn btn-primary btn-xs" href="/moderation/order/offer/{{$order->id}}"> Сделать предложение </a>
                    </div>

                    <div class="panel-body" id="get-user">
                        <form action="/orders/getOne/edit" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$order->id}}">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                Товар
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <table>
                                                    @foreach($products as $key=>$product)
                                                        <tr>
                                                            <td>

                                                                <input id="ch-box{{$key}}" name="products[]"
                                                                       type="checkbox"
                                                                       class="char-box"
                                                                       value="{{$product->id}}"
                                                                       @if($order->products()->find($product->id)) checked @endif>
                                                                <label for="ch-box{{$key}}"> {{$product->name}}</label>
                                                            </td>
                                                            <td>
                                                                <input type="number" name="quantities[]"
                                                                       id="value{{$key}}"
                                                                       class="form-control"
                                                                       @if(!$order->products()->find($product->id)) disabled
                                                                       @endif
                                                                       value="{{$order->products()->find($product->id)->pivot->quantity or null}}">
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="delivery" class="control-label">Доставка</label>
                                <select name="delivery_id" id="delivery" class="form-control">
                                    @foreach($deliveries as $delivery)
                                        <option value="{{$delivery->id}}"
                                                @if($order->delivery_id == $delivery->id)
                                                selected @endif>{{$delivery->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="terms" class="control-label">Срок</label>
                                <input name="terms" id="terms" class="form-control"
                                       value="{{\Carbon\Carbon::createFromTimestamp($order->terms)->format('Y-m-d H:i')}}">
                            </div>
                            <div class="form-group">
                                <label for="status" class="control-label">Статус</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="0" @if($order->status==0) selected @endif>Корзина не оформлена</option>
                                    <option value="1" @if($order->status==1) selected @endif>Модерация</option>
                                    <option value="2" @if($order->status==2) selected @endif>Новая</option>
                                    <option value="3" @if($order->status==3) selected @endif>Ожидает оплаты</option>
                                    <option value="4" @if($order->status==4) selected @endif>На доставке</option>
                                    <option value="5" @if($order->status==5) selected @endif>Завершен</option>
                                    <option value="6" @if($order->status==6) selected @endif>В архиве</option>
                                </select>
                            </div>
                            <button class="btn btn-primary">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $('.char-box').change(function () {
                $('.char-box').each(function (index, element) {
                    if ($(this).prop("checked")) {
                        $('#value' + index).prop('disabled', false)
                    } else {
                        $('#value' + index).prop('disabled', true)
                    }
                })
            });

        </script>
    @endpush
@endsection
