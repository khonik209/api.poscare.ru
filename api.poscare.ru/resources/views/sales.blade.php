@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Акции</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Акции
                        <a href="/sales/create" class="pull-right btn btn-primary btn-xs">Добавить акцию</a>
                    </div>

                    <div class="panel-body" id="get-orders">
                        @foreach($sales as $sale)
                            <div class="media">
                                <a class="pull-left" href="/sales/get/{{$sale->id}}">
                                    <img class="media-object" src="{{$sale->images->first()->href or '#'}}" alt="..."
                                         style="    max-width: 100px;">
                                </a>
                                <div class="media-body">
                                    <p>ID: <strong>{{$sale->id}}</strong></p>
                                    @if($sale->user)
                                        <p>Пользователь:
                                            <strong>{{$sale->user->email or 'Не найден'}}</strong>
                                        </p>
                                    @endif
                                    @if($sale->cities()->count()>0)
                                        <p>
                                            Города: @foreach($sale->cities as $city) <span>{{$city->name}}
                                                ,</span> @endforeach
                                        </p>
                                    @endif
                                    <h4 class="media-heading">
                                        <a href="/sales/get/{{$sale->id}}">
                                            {{$sale->name}}
                                        </a>
                                    </h4>
                                    <p>
                                        {{\Carbon\Carbon::createFromTimestamp($sale->start_date)->format('d.m.Y H:i')}}
                                        - {{\Carbon\Carbon::createFromTimestamp($sale->end_date)->format('d.m.Y H:i')}}
                                    </p>
                                    <p>
                                        {{$sale->description}}
                                    </p>
                                </div>
                                <form action="/sales/delete" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="sale_id" value="{{$sale->id}}">
                                    <button class="pull-right btn btn-danger btn-xs">Удалить</button>
                                </form>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
